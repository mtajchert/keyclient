User-Agent: *
Allow: /
Disallow: /logowanie
Disallow: /rejestracja
Disallow: /regulamin
Disallow: /polityka-prywatnosci
Disallow: /szukaj
#Sitemap files
Sitemap: https://alcatras.pl/sitemap.xml