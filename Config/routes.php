<?php
Router::connect('/', array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'));
Router::connect('/nie-znaleziono-strony', array('plugin' => CLIENT, 'controller' => 'Errors', 'action' => 'error404'));
Router::connect('/regulamin', array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'terms'));
Router::connect('/polityka-prywatnosci', array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'privacy'));
Router::connect('/kontakt', array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'contact'));
Router::connect('/:slug', array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'view'), array('pass' => array('slug'), 'slug' => '([a-zA-Z0-9-])*\-s([0-9]+)'));
Router::connect('/tuning/:action/*', array('plugin' => CLIENT, 'controller' => 'Tuning'));
Router::connect('/tuning', array('plugin' => CLIENT, 'controller' => 'Tuning'));
Router::connect('/:slug/*', array('plugin' => CLIENT, 'controller' => 'Products', 'action' => 'view'), array('pass' => array('slug'), 'slug' => '([a-zA-Z0-9-])*\-p([0-9]+)'));
Router::connect('/:slug/*', array('plugin' => CLIENT, 'controller' => 'Categories', 'action' => 'view'), array('pass' => array('slug'), 'slug' => '([a-zA-Z0-9-])*\-c([0-9]+)'));
Router::connect('/logowanie/*', array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'login'));
Router::connect('/wyloguj', array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'logout'));
Router::connect('/rejestracja', array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'register'));
Router::connect('/moje_konto', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'account'));
Router::connect('/historia_zamowien', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'orders'));
Router::connect('/historia_zamowien/podglad/*', array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'view'));
Router::connect('/adresy', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'addresses'));
Router::connect('/adresy/dodaj_adres', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'create_address'));
Router::connect('/adresy/edytuj_adres/*', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'edit_address'));
Router::connect('/adresy/usun_adres/*', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'delete_address'));
Router::connect('/user/get_zones', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'get_zones'));
Router::connect('/ustawienia_konta', array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'settings'));
Router::connect('/resetowanie_hasla/*', array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'reset_password'));

Router::connect('/koszyk', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index'));
Router::connect('/zamowienie/konto', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'));
Router::connect('/zamowienie/adresy', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'));
Router::connect('/zamowienie/adresy/dodaj-adres/*', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new'));
Router::connect('/zamowienie/adresy/edytuj-adres/*', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_edit'));
Router::connect('/zamowienie/adresy/usun-adres/*', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_delete'));
Router::connect('/zamowienie/adresy/wybierz-adres/*', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_choose'));
Router::connect('/zamowienie/dostawa-i-platnosc', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'));
Router::connect('/zamowienie/potwierdzenie', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_confirm'));
Router::connect('/cart/:action/*', array('plugin' => CLIENT, 'controller' => 'Cart'));

Router::connect('/zamowienie/platnosc/*', array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'pay_for_order'));
Router::connect('/zamowienie/po_platnosci/*', array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'after_payment'));
Router::connect('/zamowienie/status/*', array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'payment_status'));

Router::connect('/szukaj/*', array('plugin' => CLIENT, 'controller' => 'Search', 'action' => 'index'));
Router::connect('/szukaj_wycieraczki/*', array('plugin' => CLIENT, 'controller' => 'Categories', 'action' => 'search_wipers_index'));
Router::connect('/search/:action/*', array('plugin' => CLIENT, 'controller' => 'Search'));

Router::connect('/categories/:action', array('plugin' => CLIENT, 'controller' => 'Categories'));
Router::connect('/categories/:action/*', array('plugin' => CLIENT, 'controller' => 'Categories'));
Router::connect('/baselinker', array('plugin' => CLIENT, 'controller' => 'BaseLinker', 'action' => 'index'));

Router::connect(
  '/:slug/*',
  array('plugin' => CLIENT, 'controller' => 'Redirects', 'action' => 'redirectUrl', 'product'),
  array('pass' => array('slug'), 'slug' => '([a-zA-Z0-9-])*\-p-([0-9]+).html')
);
Router::connect(
  '/:slug/*',
  array('plugin' => CLIENT, 'controller' => 'Redirects', 'action' => 'redirectUrl', 'category'),
  array('pass' => array('slug'), 'slug' => '([a-zA-Z0-9-])*\-c-([0-9]+).html')
);
Router::connect(
  '/:slug/*',
  array('plugin' => CLIENT, 'controller' => 'Redirects', 'action' => 'redirectUrl', 'other'),
  array('pass' => array('slug'), 'slug' => '(platnosci-i-dostawa-pm-3.html|certyfikat-prokonsumencki-pm-21.html|reklamacja-towaru-pm-17.html|odstapienie-od-umowy-pm-18.html|regulamin-pm-5.html|polityka-prywatnosci-pm-19.html|kontakt-f-1.html|dane-firmy-pm-11.html|jak-dojechac-pm-10.html|logowanie.html|rejestracja.html)')
);

Router::connect('/sitemap.xml', array('plugin' => CLIENT, 'controller' => 'Sitemap', 'action' => 'index'));