<?php
App::uses('ImageComponent', 'KeyAdmin.Controller/Component');

class WipersShell extends AppShell {
	
  public $uses = array('Category', 'KeyAdmin.ProductsCategory', 'KeyAdmin.Product');
  
  public $components = array('KeyAdmin.Image');
  
  public function main() {
    set_time_limit(0);
    $brands = $this->Category->find('list', array('conditions' => array('Category.parent_id' => 2)));
    foreach ($brands as $brandId => $brandName){
      $models = $this->Category->find('list', array('conditions' => array('Category.parent_id' => $brandId)));
      foreach ($models as $modelId => $modelName){
        $wipers = $this->Category->find('list', array('conditions' => array('Category.parent_id' => $modelId)));
        foreach ($wipers as $wiperId => $wiperName ){
          $products = $this->ProductsCategory->find('all', array('conditions' => array('ProductsCategory.category_id' => $wiperId)));
          $productIds = Hash::extract($products, '{n}.ProductsCategory.product_id');
          $this->ProductsCategory->deleteAll(array('ProductsCategory.category_id' => $wiperId), false);
          $this->out("{$brandName} > {$modelName} > {$wiperName}: USUNIETO POWIAZANIA.");
          if (!empty($productIds)){
            $this->Product->deleteAll(array('Product.id' => $productIds), false);
            $this->out("{$brandName} > {$modelName} > {$wiperName}: USUNIETO.");
          }
        }
      }
    }
  }
  
  
  public function categories_description(){
    $raw = file_get_contents(WWW_ROOT.'produkty.csv');
    if (empty($raw)) {
      throw new Exception('Data file is empty');
    }
    
    $data = array_map('str_getcsv', explode("\r\n", $raw));
    
    foreach ($data as $row){
      if (!empty($row[2])){
        $this->Category->read(null, $row[0]);
        $this->Category->set('name', $row[2]);
        $this->Category->set('description', $row[3]);
        $this->Category->set('seo_url', '');
        $this->Category->save();
        $this->out("Zauktualizowano kategorie: {$row[2]}");
      }
    }
  }
  
  
  public function products_description(){
    $raw = file_get_contents(WWW_ROOT.'produkty.csv');
    if (empty($raw)) {
      throw new Exception('Data file is empty');
    }
    
    $data = array_map('str_getcsv', explode("\r\n", $raw));
    
    foreach ($data as $row){
      if (!empty($row[2])){
        $this->Product->updateAll(array(
          'Product.name' => '"'.strval(str_replace('"', '', $row[3])).'"', 
          'Product.description' => '"'.str_replace('"', '', $row[5]).'"',
          'Product.short_description' => '"'.str_replace('"', '', $row[4]).'"',
          'Product.seo_url' => '"'.str_replace('"', '', $row[2]).'"',
        ), array('Product.id' => intval($row[0])) );
        $this->out("Zauktualizowano product: {$row[2]}");
      }
    }
  }
  
  public function getProductViewUrl($product, $option_id = null) {
    if (is_numeric($product)) {
      $this->Product = ClassRegistry::init('KeyAdmin.Product');
      $product = $this->Product->findById($product);
    }
    $seoUrl = (!empty($product['Product']['seo_url']))? $product['Product']['seo_url']: Inflector::slug($product['Product']['name'], '-');
    $slug = strtolower($seoUrl.'-p'.$product['Product']['id']);
  
    if ($option_id > 0) {
      return Router::url(array('plugin' => CLIENT, 'controller' => 'Products', 'action' => 'view', 'slug' => $slug, 'option_id' => $option_id), true);
    } else {
      return Router::url(array('plugin' => CLIENT, 'controller' => 'Products', 'action' => 'view', 'slug' => $slug), true);
    }
  }
  
  
  public function setShipmentForProducts(){
    set_time_limit(0);
    $brands = $this->Category->find('list', array('conditions' => array('Category.parent_id' => 854, 'Category.status' => 1)));    
    
    $this->loadModel('ProductsShipping');
    foreach ($brands as $brandId => $brandName){
      $this->ProductsCategory->contain(array('Product'));
      $products = $this->ProductsCategory->find('all', array(
          'conditions' => array('ProductsCategory.category_id' => $brandId)
      ));
      foreach ($products as $product){
        $toSave = array(
          array('product_id' => $product['Product']['id'], 'shipping_id' => 3),
          array('product_id' => $product['Product']['id'], 'shipping_id' => 4),
          array('product_id' => $product['Product']['id'], 'shipping_id' => 5)
        );
        $this->out("Zapisuje: {$product['Product']['name']}");
        try {
          $this->ProductsShipping->saveAll($toSave);
        } catch (Exception $e) {
          
        }
        
      }
    
      $childs = $this->Category->find('list', array('conditions' => array('Category.parent_id' => $brandId, 'Category.status' => 1)));
      foreach ($childs as $id => $name){
        $this->ProductsCategory->contain(array('Product'));
        $products2 = $this->ProductsCategory->find('all', array(
            'conditions' => array('ProductsCategory.category_id' => $id)
        ));
        foreach ($products2 as $product2){
          $toSave = array(
            array('product_id' => $product2['Product']['id'], 'shipping_id' => 3),
            array('product_id' => $product2['Product']['id'], 'shipping_id' => 4),
            array('product_id' => $product2['Product']['id'], 'shipping_id' => 5)
          );
          $this->out("Zapisuje: {$product2['Product']['name']}");
          try {
            $this->ProductsShipping->saveAll($toSave);
          } catch (Exception $e) {
          }
        }
      }
    }
  }
  
  public function generate_csv(){
    $file = fopen("categories.csv","w");
    /* 
    foreach ($list as $line)
    {
      fputcsv($file,explode(',',$line));
    }*/
    
    set_time_limit(0);
    $brands = $this->Category->find('list', array('conditions' => array('Category.parent_id' => 854, 'Category.status' => 1)));
    
    App::uses('AppHelper', 'View/Helper');
    $appHelper = new AppHelper(new View());
    //$this->set('canonical', $appHelper->getProductViewUrl($product));
    
    foreach ($brands as $brandId => $brandName){
      $this->ProductsCategory->contain(array('Product'));
      $products = $this->ProductsCategory->find('all', array(
        'conditions' => array('ProductsCategory.category_id' => $brandId)
      ));
      foreach ($products as $product){
        if ($product['Product']['status'] == '1'){
          fputcsv($file,array($product['Product']['id'], $brandName, $product['Product']['name']));
        }        
      }
      
      $childs = $this->Category->find('list', array('conditions' => array('Category.parent_id' => $brandId, 'Category.status' => 1)));
      foreach ($childs as $id => $name){
        $this->ProductsCategory->contain(array('Product'));
        $products = $this->ProductsCategory->find('all', array(
            'conditions' => array('ProductsCategory.category_id' => $id)
        ));
        foreach ($products as $product){
          if ($product['Product']['status'] == '1'){
            fputcsv($file,array($product['Product']['id'], $name, $product['Product']['name']));
          }
        }
      }
    }
    
    fclose($file);
  }
  
  
  public function remove_empty(){
    set_time_limit(0);
    $products = $this->ProductsCategory->find('list', array('fields' => array('id', 'product_id'), 'order' => array('id' => 'ASC')));
    foreach ($products as $id => $productId){
      if (!$this->Product->exists($productId)){
        $this->ProductsCategory->delete($id);
        $this->out('Usunięto ID: '.$id);
      }
    }
    
  }
  
  
  public function remove_old(){
    set_time_limit(0);
    $categories = $this->Category->find('list', array('conditions' => array('Category.id' => array(8227, 8229, 8230, 8231, 8232, 8233, 8234, 8262))));
    
    foreach ($categories as $categoryId => $categoryName){
      $products = $this->ProductsCategory->find('all', array('conditions' => array('ProductsCategory.category_id' => $categoryId)));
      if (!empty($products)){
        $productIds = Hash::extract($products, '{n}.ProductsCategory.product_id');
        $this->Product->deleteAll(array('Product.id' => $productIds), false);
        $this->Category->delete($categoryId);
        $this->out('Removed for: '.$categoryName);
      }      
    }
  }
}