<?php

App::uses('CommonController', CLIENT.'.Controller');

class AuthController extends CommonController {

  public $uses = array('KeyAdmin.User', 'KeyAdmin.Customer', 'KeyAdmin.Page');
  public $components = array('KeyAdmin.NotificationCenter', 'RequestHandler');
  
  public function __construct($request = null, $response = null) {
    $this->uses[] = CLIENT.'ResetPassword';
    $this->uses[] = CLIENT.'ChangePassword';
    parent::__construct($request, $response);
  }
  
  /**
   * (non-PHPdoc)
   * @see Controller::beforeFilter()
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('login', 'logout', 'register', 'reset_password');
  }

  /**
   * 
   */
  public function login() {
    $internal = isset($this->request->params['named']['internal']) ? $this->request->params['named']['internal'] : false;
    if ($internal) {
      $this->request->data['User'] = array(
        'email' => $this->request->params['named']['email'],
        'password' => $this->request->params['named']['password'],
      );
    }
    
    App::uses('User', 'KeyAdmin.Model');
    if (is_numeric($this->Auth->user('id')) && $this->Auth->user('id') > 0) {
      if (User::isAdmin($this->Auth->user('id'))) {
        if ($internal) {
          return false;
        }
        return $this->redirect(array('plugin' => 'KeyAdmin', 'controller' => 'KeyDashboard', 'action' => 'index', 'admin' => true));
      } else {
        if ($internal) {
          return true;
        }
        return $this->redirect($this->Auth->redirectUrl());
      }
    }
    
    if ($this->request->is('post')) {
      if ($this->Auth->login()) {
        App::uses('User', 'KeyAdmin.Model');
		$this->Customer->recursive = 1;
        $customer = $this->Customer->findById($this->Auth->user('id'));
		
        if (User::isAdmin($this->Auth->user('id')) || $customer['Customer']['guest_account']) {
          $this->Session->destroy();
          $this->Auth->logout();
          $this->request->data['User']['password'] = '';
        } else {
          
          if (!$customer['UserData']['status']) {
            if (!$internal) {
              $this->Session->setFlash(__('Konto nieaktywne'), 'flash-error', array(), 'auth_login');
            }
          } else {
            $this->Customer->id = $this->Auth->user('id');
            $this->Customer->save(array(
              'last_login' => date('Y-m-d H:i:s')
            ), false);

            if ($internal) {
              return true;
            }
            return $this->redirect($this->Auth->redirect());
          }
        }
      }
      
      if ($internal) {
        return false;
      }
      
      $this->request->data['User']['password'] = '';
      $this->Session->setFlash(__('Niepoprawny e-mail i/lub hasło'), 'flash-error', array(), 'auth_login');
    }
    
    $this->render('Auth/login');
  }
  
  /**
   *
   */
  public function logout() {
    $this->Session->destroy();
    $this->redirect($this->Auth->logout());
  }
  
  public function register() {
    $this->Customer->validator()->add('accept_rules', array(
      'rule1' => array(
        'rule' => 'naturalNumber',
        'allowEmpty' => false,
        'message' => 'Musisz zaakceptować regulamin i politykę prywatności'
      ),
    ));
    
    App::uses('User', 'KeyAdmin.Model');
    if (is_numeric($this->Auth->user('id')) && $this->Auth->user('id') > 0) {
      if (User::isAdmin($this->Auth->user('id'))) {
        return $this->redirect(array('plugin' => 'KeyAdmin', 'controller' => 'KeyDashboard', 'action' => 'index', 'admin' => true));
      } else {
        return $this->redirect($this->Auth->redirectUrl());
      }
    }
    
    if ($this->request->is('post')) {
      unset($this->request->data['rememberme']);
      unset($this->request->data['submit']);
      
      $this->request->data['UserData'] = array(
        'guest_account' => 0,
        'status' => 1
      );
      
      $group = $this->Customer->UserGroup->find('first', array(
        'conditions' => array(
          'admin' => 0
        )
      ));
      $this->request->data['Customer']['user_group_id'] = $group['UserGroup']['id'];
      
      $this->request->data['Customer']['password2'] = $this->request->data['Customer']['password'];
      
      if ($this->Customer->saveAll($this->request->data, array('noAddress' => true, 'noContactPhone' => true))) {
        $this->Customer->recursive = 2;
        $customer = $this->Customer->findById($this->Customer->getLastInsertID());
        $this->Auth->login($customer['Customer']);

        $this->NotificationCenter->send('account_registration', array($customer['Customer']['email'], trim($customer['Customer']['first_name'].' '.$customer['Customer']['last_name'])), $customer);

        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'account'));
      } else {
        $this->request->data['Customer']['password'] = '';
        $this->Session->setFlash(__('Wpisz poprawne dane'), 'flash-error', array(), 'auth_register');
      }
    }
    
    $this->render('Auth/register');
  }
  
  public function reset_password() {
    $id = $this->Auth->user('id');
    if ($id > 0) {
      throw new NotFoundException();
    }
    
    $email = isset($this->request->params['named']['e']) ? $this->request->params['named']['e'] : null;
    $hash = isset($this->request->params['named']['h']) ? $this->request->params['named']['h'] : null;
    
    $step = 1;
    if (!empty($email) && !empty($hash)) {
      $step = 2;
    }
    
    
    if ($step == 1) {
      if ($this->request->is(array('post', 'put'))) {
        if ($this->ResetPassword->save($this->request->data)) {
          $this->Customer->contain('UserData');
          $customer = $this->Customer->find('first', array(
            'conditions' => array(
              'Customer.email' => $this->request->data['ResetPassword']['email'],
              'UserData.status' => 1,
              'Customer.guest_account' => 0
            )
          ));

          $customer['Customer']['hash'] = md5(uniqid(mt_rand(), true).time());
          $this->Customer->updateAll(array('Customer.hash' => "'".$customer['Customer']['hash']."'"), array('Customer.id' => $customer['Customer']['id']));

          if (empty($customer)) {
            $this->Session->setFlash(__('Użytkownik o podanym adresie e-mail nie został zarejestrowany.'), 'flash-error');
          } else {
            if ($this->NotificationCenter->send('password_reset', array($customer['Customer']['email'], trim($customer['Customer']['first_name'].' '.$customer['Customer']['last_name'])), $customer)) {
              $this->Session->setFlash(__('Na Twój adres e-mail został wysłany link do formularza zmiany hasła.'), 'flash-success');
            } else {
              $this->Session->setFlash(__('Nie udało się wysłać e-maila z linkiem resetowania hasła. Spróbuj ponownie.'), 'flash-error');
            }
          }
        }
      }
    } elseif ($step == 2) {
      $this->Customer->contain('UserData');
      $customer = $this->Customer->find('first', array(
        'conditions' => array(
          'Customer.email' => $email,
          'UserData.status' => 1,
          'Customer.guest_account' => 0
        )
      ));
      if (empty($customer) || $customer['Customer']['hash'] != $hash) {
        $this->Session->setFlash(__('Nie odnaleziono podanego konta lub wpisany link jest niepawidłowy lub nieaktualny.'), 'flash-error');
      } elseif ($customer) {
        if ($this->request->is(array('post', 'put'))) {
          $this->request->data['ChangePassword']['user_id'] = $customer['Customer']['id'];
          if ($this->ChangePassword->save($this->request->data)) {
            $this->Session->setFlash(__('Hasło do Twojego konta zostało zmienione, możesz się zalogować.'), 'flash-success');
          }
        }
      }
      
      $this->request->data['ChangePassword']['password'] = $this->request->data['ChangePassword']['password2'] = '';
    }
    
    $pages = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'reset_password_step_'.$step
      )
    ));
    
    $this->set('pages', $pages);
    $this->set('step', $step);
    
    $this->render('Auth/reset_password');
  }

}
