<?php
App::uses('CommonController', CLIENT . '.Controller');

class BaseLinkerController extends CommonController {
    public $uses = array(
        'KeyAdmin.Order',
        'KeyAdmin.Product',
        'KeyAdmin.ProductsCategory',
        'KeyAdmin.Category'
    );
    private $baseLinkerPass = 'iso7o579p8k8xi8fmhwqrslw9bzlelrt';
    public $components = array(
        'CartManage'
    );



    /**
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index');
    }



    /**
     */
    public function index() {
        $data = $this->request->data;
        //file_put_contents(TMP . 'baselinker'. DS . date('Y-m-d-H-i-s').'_baselinker.txt', serialize($data));
        file_put_contents(TMP . 'baselinker'. DS . date('Y-m-d-H-i-s').'_baselinker_json.txt', json_encode($data));
        // $data = json_decode('{"previous_order_id":"1192","action":"OrderAdd","delivery_fullname":"Mateusz Cholewi\u0144ski","delivery_company":"","delivery_address":"ul.Sygna\u0142owa 50a","delivery_city":"Wroc\u0142aw","delivery_country":"Polska","delivery_country_code":"PL","delivery_postcode":"52-130","delivery_point_name":"WRO912","invoice_fullname":"Mateusz Cholewi\u0144ski","invoice_company":"","invoice_address":"ul.Sygna\u0142owa 50a","invoice_city":"Wroc\u0142aw","invoice_country":"Polska","invoice_country_code":"PL","invoice_postcode":"52-130","invoice_nip":"","phone":"607 233 444","email":"cholewinski.m@gmail.com","user_comments":"[Allegro: 6531413546] Prosz\u0119 o wys\u0142anie rozmiaru 37-38 [Punkt odbioru: Paczkomat WRO912]","delivery_method":"Odbi\u00f3r w punkcie po przedp\u0142acie - Allegro Paczkomaty InPost","want_invoice":"0","paid":"1","currency":"PLN","delivery_price":"8.60","baselinker_id":"10726418","delivery_method_id":"7","delivery_point_postcode":"50-559","products":"[{\"id\":\"124\",\"variant_id\":\"261\",\"name\":\"Kapcie baleriny w paski z kokardk\\u0105 - HIT!\",\"price\":\"35.90\",\"quantity\":\"1\",\"auction_id\":\"6531413546\"}]","payment_method":"PayU","payment_method_cod":"0","change_products_quantity":"1","bl_pass":"rfg98oi717gyra7wgwsgemd98cf75pz"}', true);
        
        
        // sprawdzanie poprawności hasła wymiany danych
        if (! isset($data['bl_pass'])) {
            $this->errorHandle("no_password", "Odwołanie do pliku bez podania hasła. Jest to poprawny komunikat jeśli plik integracyjny został otworzony w przeglądarce internetowej.");
        } elseif ($this->baseLinkerPass !== $data['bl_pass']) {
            $this->errorHandle("incorrect_password");
        }
        
        if (isset($data['orders_ids'])) {
            $data['orders_ids'] = explode(",", $data['orders_ids']);
        }
        if (isset($data['products_id'])) {
            $data['products_id'] = explode(",", $data['products_id']);
        }
        if (isset($data['fields'])) {
            $data['fields'] = explode(",", $data['fields']);
        }
        if (isset($data['products'])) {
            $data['products'] = json_decode(stripslashes($data['products']), true);
        }
        
        try {
            $action = 'shop_' . $data['action'];
            $response = $this->{$action}($data);
        } catch ( Exception $e ) {
            echo $e->getMessage();
            $this->errorHandle("unsupported_action", "No action: " . $_POST['action']);
        }
        
        echo json_encode($response);
        exit();
    }



    protected function shop_ProductsCategories($request) {
        $this->Category->contain([
            'ChildCategory' => [
                'fields' => [
                    'ChildCategory.id',
                    'ChildCategory.name'
                ]
            ],
            'ChildCategory.ChildCategory' => [
                'fields' => [
                    'ChildCategory.id',
                    'ChildCategory.name'
                ]
            ]
        ]);
        $categories = $this->Category->find('all', [
            'conditions' => [
                'Category.parent_id' => 0
            ],
            'fields' => [
                'Category.id',
                'Category.parent_id',
                'Category.name'
            ]
        ]);
        
        $return = [];
        foreach ( $categories as $category ) {
            $return[$category['Category']['id']] = $category['Category']['name'];
            if (! empty($category['ChildCategory'])) {
                foreach ( $category['ChildCategory'] as $childCat ) {
                    $return[$childCat['id']] = "{$category['Category']['name']}/{$childCat['name']}";
                    
                    $child2 = Hash::extract($childCat, '{n}');
                    if (! empty($child2)) {
                        foreach ( $child2 as $childCat2 ) {
                            $return[$childCat2['id']] = "{$category['Category']['name']}/{$childCat['name']}/{$childCat2['name']}";
                        }
                    }
                }
            }
        }
        asort($return);
        
        return $return;
    }



    /**
     * Funkcja zwraca listę produktów z bazy sklepu
     * Zwracane liczby (np ceny) powinny mieć format typu: 123456798.12 (kropka oddziela część całkowitą, 2 miejsca po przecinku)
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu zawierająca pola:
     *            category_id => id kategori (wartość 'all' jeśli wszystkie przedmioty)
     *            filter_limit => limit zwróconych kategorii w formacie SQLowym ("ilość pomijanych, ilość pobieranych")
     *            filter_sort => wartość po której ma być sortowana lista produktów. Możliwe wartości:
     *            "id [ASC|DESC]", "name [ASC|DESC]", "quantity [ASC|DESC]", "price [ASC|DESC]"
     *            filter_id => ograniczenie wyników do konkretnego id produktu
     *            filter_ean => ograniczenie wyników do konkretnego ean
     *            filter_sku => ograniczenie wyników do konkretnego sku (numeru magazynowego)
     *            filter_name => filtr nazw przedmiotów (fragment szukanej nazwy lub puste pole)
     *            filter_price_from => dolne ograniczenie ceny (nie wyświetlane produkty z niższą ceną)
     *            filter_price_to => górne ograniczenie ceny
     *            filter_quantity_from => dolne ograniczenie ilości produktów
     *            filter_quantity_to => górne ograniczenie ilości produktów
     *            filter_available => wyświetlanie tylko produktów oznaczonych jako dostępne (wartość 1) lub niedostępne (0) lub wszystkich (pusta wartość)
     * @return array $response tablica z listą produktów w formacie:
     *         id produktu =>
     *         'name' => nazwa produktu
     *         'quantity' => dostępna ilość
     *         'price' => cena w PLN
     */
    protected function shop_ProductsList($request) {
        $options = [];
        if ($request['category_id'] != 'all') {
            $options['conditions']['ProductsCategory.category_id'] = $request['category_id'];
        }
        if (! empty($request['filter_id'])) {
            $options['conditions']['ProductsCategory.product_id'] = $request['filter_id'];
        }
        if (! empty($request['filter_ean'])) {
            $options['conditions']['Product.ean'] = $request['filter_ean'];
        }
        if (! empty($request['filter_sku'])) {
            $options['conditions']['Product.pkwiu'] = $request['filter_sku'];
        }
        if (! empty($request['filter_name'])) {
            $options['conditions']['Product.name LIKE'] = "%{$request['filter_name']}%";
        }
        if (! empty($request['filter_price_from'])) {
            $options['conditions']['Product.price_tax >='] = $request['filter_price_from'];
        }
        if (! empty($request['filter_price_to'])) {
            $options['conditions']['Product.price_tax <='] = $request['filter_price_to'];
        }
        if (! empty($request['filter_quantity_from'])) {
            $options['conditions']['Product.store_quantity >='] = $request['filter_quantity_from'];
        }
        if (! empty($request['filter_quantity_to'])) {
            $options['conditions']['Product.store_quantity <='] = $request['filter_quantity_to'];
        }
        if (! empty($request['filter_available'])) {
            $options['conditions']['Product.status'] = $request['filter_available'];
        }
        
        if (! empty($request['filter_limit'])) {
            $tmp = explode(', ', $request['filter_limit']);
            $options['limit'] = $tmp[1];
            $options['offset'] = $tmp[0];
        }
        if (! empty($request['filter_sort'])) {
            $tmp = explode(' ', $request['filter_sort']);
            if ($tmp[0] == 'price') {
                $tmp[0] = 'price_tax';
            }
            if ($tmp[0] == 'quantity') {
                $tmp[0] = 'store_quantity';
            }
            $options['order'] = [
                "Product.{$tmp[0]}" => $tmp[1]
            ];
        }
        
        $this->ProductsCategory->contain([
            'Product' => [
                'fields' => [
                    'id',
                    'name',
                    'store_quantity',
                    'price_tax',
                    'ean'
                ]
            ]
        ]);
        $products = $this->ProductsCategory->find('all', $options);
        
        $response = [];
        foreach ( $products as $product ) {
            $response[$product['Product']['id']] = array(
                'name' => $product['Product']['name'],
                'quantity' => intval($product['Product']['store_quantity']),
                'price' => $product['Product']['price_tax'],
                'sku' => ''
            ); // $product['Product']['ean']
        }
        return $response;
    }



    /**
     * Funkcja zwraca szczegółowe dane wybranych produktów
     * Zwracane liczby (np ceny) powinny mieć format typu: 123456798.12 (kropka oddziela część całkowitą, 2 miejsca po przecinku)
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu zawierająca pola:
     *            products_id => tablica z numerami id produktów
     *            fields => tablica z nazwami pól do zwrócenia (jeśli pusta zwracany jest cały wynik)
     * @return array $response tablica z listą produktów w formacie:
     *         id produktu =>
     *         'name' => nazwa produktu, 'ean' => Kod EAN, 'sku' => numer katalogowy, 'model' => nazwa modelu lub inny identyfikator np ISBN,
     *         'description' => opis produktu (może zawierać tagi HTML), 'description_extra1' => drugi opis produktu (np opis krótki) 'weight' => waga produktu w kg,
     *         'quantity' => dostępna ilość, 'man_name' => nazwa producenta, 'man_image' => pełny adres obrazka loga producenta,
     *         'category_id' => numer ID głównej kategorii, 'category_name' => nazwa kategori do której należy przedmiot, 'tax' => wielkość podatku w formie liczby (np 23)
     *         'price' => cena brutto w PLN,
     *         'images' => tablica z pełnymi adresami dodatkowych obrazków (pierwsze zdjęcie główne, reszta w odpowiedniej kolejności),
     *         'features' => tablica z opisem cech produktu. Poszczególny element tablicy zawiera nazwę i wartość cechy, np array('Rozdzielczość','Full HD')
     *         'variants' => tablica z wariantami produktu do wyboru (np kolor, rozmiar). Format pola opisany jest w kodzie poniżej
     */
    protected function shop_ProductsData($request) {
        $options = [];
        if (! empty($request['products_id'])) {
            $options['conditions']['Product.id'] = $request['products_id'];
        }
        $this->Product->contain([
            'Manufacturer',
            'ProdOptions.ProductOptionValue',
            'Category',
            'ProductsImage'
        ]);
        $products = $this->Product->find('all', $options);
        
        $prodData = $response = [];
        
        foreach ( $products as $product ) {
            $variants = [];
            foreach ( $product['ProdOptions'] as $option ) {
                if ($option['product_availability_id'] != 1) {
                    continue;
                }
                $variants[$option['id']] = [
                    'full_name' => "{$product['Product']['name']} - {$option['ProductOptionValue']['name']}",
                    'name' => $option['ProductOptionValue']['name'],
                    'price' => $option['change_price_tax'],
                    'quantity' => intval($option['store_quantity'])
                ];
            }
            
            $prodData[$product['Product']['id']] = [
                'name' => $product['Product']['name'],
                'model' => '',
                'ean' => $product['Product']['ean'],
                'sku' => '',
                'description' => $product['Product']['description'],
                'description_extra1' => '',
                'weight' => '',
                'quantity' => intval($product['Product']['store_quantity']),
                'man_name' => $product['Manufacturer']['name'],
                'man_image' => Router::url('/manufacturers/origin/' . $product['Manufacturer']['image'], true),
                'category_id' => $product['Category'][0]['id'],
                'category_name' => $product['Category'][0]['name'],
                'tax' => 23,
                'price' => $product['Product']['price_tax'],
                'images' => [
                    Router::url('/products/origin/' . $product['ProductsImage'][0]['image'], true)
                ],
                'features' => [],
                'variants' => $variants
            ];
        }
        
        // wyrzucanie niepotrzebnych wartości jeśli określono pola do pobrania
        // poniższy kod może pozostać identyczny niezależnie od platformy sklepu
        foreach ( $prodData as $key => $prod ) {
            if (isset($request['fields']) && ! (count($request['fields']) == 1 && $request['fields'][0] == "") && ! count($request['fields']) == 0) {
                $temp_p = array();
                foreach ( $request['fields'] as $field ) {
                    $temp_p[$field] = $p[$field];
                }
                $prod = $temp_p;
            }
            
            $response[$key] = $prod;
        }
        
        return $response;
    }



    /**
     * Funkcja zwraca stan magazynowy wszystkich produktów i ich wariantów
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu, w przypadku tej funkcji nie używana
     * @return array $response tablica ze stanem magazynowym wszystkich produktów, w formacie:
     *         id produktu => ID produktu jest kluczem tablicy, wartością jest tablica składająca się ze stanów wariantów
     *         id wariantu => kluczem tablicy jest ID wariantu (0 w przypadku produktu głównego)
     *         stan => wartościa jest stan magazynowy
     *         Przykład: array('432' => array('0' => 4, '543' => 2, '567' => 3)) - produkt ID 432, stan głównego produktu to 4, posiada dwa warianty (ID 543 i 563) o stanach 2 i 3.
     */
    protected function shop_ProductsQuantity($request) {
        $response = array();
        
        $this->Product->contain([
            'ProdOptions'
        ]);
        $products = $this->Product->find('all', [
            'fields' => [
                'id',
                'store_quantity'
            ]
        ]);
        
        foreach ( $products as $product ) {
            $response[$product['Product']['id']] = [
                '0' => intval($product['Product']['store_quantity'])
            ];
            if (! empty($product['ProdOptions'])) {
                foreach ( $product['ProdOptions'] as $option ) {
                    $response[$product['Product']['id']][$option['id']] = $option['store_quantity'];
                }
            }
        }
        
        return $response;
    }



    /**
     * Funkcja ustawia stan magazynowy wybranych produktów i ich wariantów
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu:
     *            products => tablica zawierająca informacje o zmianach stanu produktu. Każdy element tablicy jest również tablicą składającą się z pól:
     *            product_id => ID produktu
     *            variant_id => ID wariantu (0 jeśli produkt główny)
     *            operation => rodzaj zmiany, dopuszczalne wartości to: 'set' (ustawia konkretny stan), 'change' (dodaje do stanu magazynowego, ujemna liczba w polu quantity zmniejszy stan o daną ilość sztuk, dodatnia zwiększy)
     *            quantity => zmiana stanu magazynowego (ilośc do ustawienia/zmniejszenia/zwiększenia zależnie od pola operation)
     * @return array $response tablica zawierajaca pole z ilością zmienionych produktów:
     *         counter => ilość zmienionych produktów
     */
    protected function shop_ProductsQuantityUpdate($request) {
        
        // Uwaga: zmieniając stan wariantu zmieniamy stan produktu głównego!
        foreach ( $request['products'] as $prod ) {
            $this->Product->contain([
                'ProdOptions'
            ]);
            $product = $this->Product->read(null, $prod['product_id']);
            
            if ($prod['operation'] == 'set') {
                $storeQuantity = $prod['quantity'];
            } elseif ($prod['operation'] == 'change') {
                if ($prod['variant_id'] == 0) {
                    $storeQuantity = $product['Product']['store_quantity'] + $prod['quantity'];
                } else {
                    $option = Hash::extract($product, 'ProdOptions.{n}[id=' . $prod['variant_id'] . '].store_quantity')[0];
                    $storeQuantity = $option + $prod['quantity'];
                }
            }
            if ($prod['variant_id'] == 0) {
                $this->Product->updateAll([
                    'Product.store_quantity' => $storeQuantity
                ], [
                    'Product.id' => $product['Product']['id']
                ]);
            } else {
                $this->Product->ProdOptions->updateAll([
                    'ProdOptions.store_quantity' => $storeQuantity
                ], [
                    'ProdOptions.id' => $prod['variant_id']
                ]);
                $this->Product->contain([
                    'ProdOptions'
                ]);
                $product = $this->Product->read(null, $product['Product']['id']);
                $storeQuantity = array_sum(Hash::extract($product['ProdOptions'], '{n}[product_availability_id=1].store_quantity'));
                $this->Product->updateAll([
                    'Product.store_quantity' => $storeQuantity
                ], [
                    'Product.id' => $product['Product']['id']
                ]);
            }
        }
        
        return array(
            'counter' => count($request['products'])
        );
    }



    protected function shop_FileVersion($request) {
        $response['platform'] = "KeyShop";
        $response['version'] = "4.1.4"; // wersja pliku integracyjnego, nie wersja sklepu!
        $response['standard'] = 4; // standard struktury pliku integracyjnego - obecny standard to 4.
        
        return $response;
    }



    /**
     * Funkcja zwracająca listę zaimplementowanych metod pliku
     * Zalecane jest pozostawienie funkcji w tej postaci niezależnie od platformy
     */
    protected function shop_SupportedMethods() {
        $result = array();
        $methods = get_class_methods('BaseLinkerController');
        foreach ( $methods as $m ) {
            if (stripos($m, 'shop_') === 0) {
                $result[] = substr($m, 5);
            }
        }
        
        return $result;
    }



    /**
     * Funkcja tworzy zamówienie w sklepie na podstawie nadesłanych danych
     * Jeśli funkcja otrzyuje na wejściu ID zamówienia, aktualizuje dane zamówienie zamiast tworzyć nowe
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu, zawiera informacje o zamówieniu w formacie:
     *            previous_order_id => ID zamówienia (jeśli pierwszy raz dodawane do sklepu, wartość jest pusta. Peśli było już wcześniej dodane, wartość zawiera poprzedni numer zamówienia)
     *            delivery_fullname, delivery_company, delivery_address, delivery_city, delivery_postcode, delivery_country => dane dotyczące adresu wysyłki
     *            invoice_fullname, invoice_company, invoice_address, invoice_city, invoice_postcode, invoice_country, invoice_nip => dane dotyczące adresu płatnika faktury
     *            phone => nr telefonu, email => adres email,
     *            delivery_method => nazwa sposóbu wysyłki, delivery_method_id => numer ID sposobu wysyłki, delivery_price => cena wysyłki
     *            user_comments => komentarz kupującego, currency => waluta zamówienia, status_id => status nowego zamówienia
     *            change_products_quantity => flaga (bool) informująca, czy po stworzeniu zamówienia zmniejszony ma zostać stan zakupionych produktów
     *            products => tablica z zakupionymi produktami w formacie:
     *            [] =>
     *            id => ID produktu
     *            variant_id => ID wariantu
     *            name => nazwa produktu (używana jeśli nie można pobrać jej z bazy na podstawie id)
     *            price => cena brutto w PLN
     *            currency => waluta
     *            quantity => zakupiona ilość
     *            attributes => tablica z atrybutami produktu w formacie:
     *            [] =>
     *            name => nazwa atrybutu (np. "kolor")
     *            value => wartość atybutu (np. "czerwony")
     *            price => różnica ceny dla tego produktu (np. "-10.00")
     *            zmiana ceny jest już uwzględniona w cenie produktu
     * @return array $response tablica zawierająca numer nowego zamówienia:
     *         'order_id' => numer utworzonego zamówienia
     */
    protected function shop_OrderAdd($request) {
        $orderStatusId = (isset($request['status_id']))? ( int )$request['status_id']: 1;
        
        // jeśli status nie jest podany, używamy pierwszego (wg ID) statusu z listy dostępnych
        if (! $orderStatusId) {
            $statuses = array_keys($this->shop_StatusesList($request));
            ksort($statuses, SORT_NUMERIC);
            $orderStatusId = array_shift($statuses);
        }
        
        // jeśli zamówienie jest ponownie dodawane do bazy sklepu, wcześniejsze dane są usuwane
        // przy ponownym dodawaniu zamówienia (aktualizowaniu), $request['previous_order_id'] zawiera poprzedni numer danego zamówienia w sklepie
        if (!empty($request['previous_order_id'])) {
            $this->Order->delete($request['previous_order_id'], true);
        }
        
        foreach ( $request['products'] as $product ) {
            if (! empty($product['variant_id'])) {
                $option = $this->Product->ProdOptions->read(null, $product['variant_id']);
                $this->CartManage->addProduct($product['id'], $product['quantity'], [$option['ProdOptions']['product_option_id'] => $option['ProdOptions']['product_option_value_id']], $product['price']);
            } else {
                $this->CartManage->addProduct($product['id'], $product['quantity'], null, $product['price']);
            }
        }
        
        
        $this->CartManage->setShipping($request['delivery_method_id'], $request['delivery_price']);
        $payments = $this->Order->getPaymentsForOrder(0, array(
            'shipping_id' => $request['delivery_method_id']
        ), false);
        $fullname = explode(' ', $request['invoice_fullname']);
        $billingAddr = [
            'is_company' => 0,
            'alias' => 'Adres podstawowy',
            'country_id' => 170,
            'company' => $request['invoice_company'],
            'first_name' => array_shift ($fullname),
            'last_name' => implode('', $fullname),
            'email' => $request['email'],
            'phone' => $request['phone'],
            'street_address' => $request['invoice_address'],
            'post_code' => $request['invoice_postcode'],
            'city' => $request['invoice_city']
        ];
        
        $saveData = [
            'BillingOrderUserAddress' => $billingAddr,
            'Customer' => ['email' => $request['email']]
        ];
        $saveData['Cart']['shipping_id'] = $request['delivery_method_id'];
        $saveData['Cart']['payment_id'] = 17;//PayU  
        
        if (! empty($request['nip'])) {
            $saveData['Cart']['invoice'] = 1;
            
            $invoiceAddr = $billingAddr;
            $invoiceAddr['nip'] = $request['nip'];
            $invoiceAddr['is_company'] = 1;
            $invoiceAddr['aliad'] = 'Dane do faktury';
            $saveData['InvoiceOrderUserAddress'] = $invoiceAddr;
        }
        
        list($cartId, $orderId) = $this->CartManage->bulkUpdateAndConvert($saveData);
        
        if (! empty($request['previous_order_id'])) {
            $this->Order->updateAll([
                'Order.id' => $request['previous_order_id']
            ], [
                'Order.id' => $orderId
            ]);
            $orderId = $request['previous_order_id'];
        }
        $inpostData = null;
        if (strpos($request['user_comments'], 'Paczkomat ') !== false) {
            $inpostData = substr($request['user_comments'], strpos($request['user_comments'], 'Paczkomat ')+10);
            $inpostData = substr($inpostData, 0, strpos($inpostData, ']'));            
        }
        $this->Order->updateAll([
            'Order.is_allegro' => 1,
            'Order.paid' => (!empty($request['paid']))? 1: 0,
            'Order.add_info' => "'{$request['user_comments']}'",
            'Order.shipping_inpost' => (!empty($inpostData))? "'{$inpostData}'": null
        ], [
            'Order.id' => $orderId
        ]);
        $response = array(
            'order_id' => $orderId
        );
        return $response;
    }



    /**
     * Funkcja pobiera zamówienia złożone w sklepie internetowym
     * Zwracane liczby (np ceny) powinny mieć format typu: 123456798.12 (kropka oddziela część całkowitą, 2 miejsca po przecinku)
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu, zawiera informacje o zamówieniu w formacie:
     *            time_from => czas od którego mają zastać pobrane zamówienia - format UNIX TIME
     *            id_from => ID od którego mają zastać pobrane zamówienia
     *            only_paid => flaga określająca czy pobierane mają być tylko zamówienia opłacone (0/1)
     * @return array $response tablica zawierająca dane zamówień:
     *         id zamówienia => array:
     *         delivery_fullname, delivery_company, delivery_address, delivery_city, delivery_postcode, delivery_country => dane dotyczące adresu wysyłki
     *         invoice_fullname, invoice_company, invoice_address, invoice_city, invoice_postcode, invoice_country, invoice_nip => dane dotyczące adresu płatnika faktury
     *         phone => nr telefonu, email => adres email,
     *         date_add => data złożenia zamówienia,
     *         payment_method => nazwa metody płatności,
     *         user_comments => komentarz klienta do zamówienia,
     *         status_id => numer ID statusu zamówienia,
     *         delivery_method_id => numer ID metody wysyłki, delivery_method => nazwa metody wysyłki,
     *         delivery_price => cena wysyłki
     *         products => array:
     *         [] =>
     *         id => id produktu, variant_id => id wariantu produktu (0 jeśli produkt główny),
     *         name => nazwa produktu
     *         quantity => zakupiona ilość, price => cena sztuki brutto (uwzględniająca atrybuty)
     *         weight => waga produktu w kg, tax => wysokość podatku jako liczba z zakresu 0-100
     *         attributes => array: - tablica z wybieralnymi atrybutami produktów (jeśli istnieją)
     *         [] =>
     *         name => nazwa atrybutu (np 'kolor'),
     *         value => wartość atrubutu (np 'czerwony'),
     *         price => różnica w cenie w stosunku do ceny standardowe
     */
    protected function shop_OrdersGet($request) {
        $response = array();
        
        $options = [];
        if (! empty($request['time_from'])) {
            $options['conditions']['Order.created >='] = date('Y-m-d H:i:s', $request['time_from']);
        }
        if (! empty($request['id_from'])) {
            $options['conditions']['Order.id >='] = $request['id_from'];
        }
        if (! empty($request['only_paid'])) {
            $options['conditions']['Order.paid'] = 1;
        }
        
        $this->Order->contain(array(
            'Shipping',
            'Payment',
            'Customer',
            'BillingOrderUserAddress',
            'InvoiceOrderUserAddress',
            'ShippingOrderUserAddress',
            'OrderProduct'
        ));
        $orders = $this->Order->find('all', $options);
        foreach ( $orders as $order ) {
            $tmp = array();
            $tmp['delivery_fullname'] = "{$order['ShippingOrderUserAddress']['first_name']} {$order['ShippingOrderUserAddress']['last_name']}";
            $tmp['delivery_company'] = $order['ShippingOrderUserAddress']['company'];
            $tmp['delivery_address'] = $order['ShippingOrderUserAddress']['street_address'];
            $tmp['delivery_city'] = $order['ShippingOrderUserAddress']['city'];
            $tmp['delivery_postcode'] = $order['ShippingOrderUserAddress']['post_code'];
            if (! empty($order['InvoiceOrderUserAddress']['id'])) {
                $tmp['invoice_fullname'] = "{$order['InvoiceOrderUserAddress']['first_name']} {$order['InvoiceOrderUserAddress']['last_name']}";
                $tmp['invoice_company'] = $order['InvoiceOrderUserAddress']['company'];
                $tmp['invoice_nip'] = $order['InvoiceOrderUserAddress']['nip'];
                $tmp['invoice_address'] = $order['InvoiceOrderUserAddress']['street_address'];
                $tmp['invoice_city'] = $order['InvoiceOrderUserAddress']['city'];
                $tmp['invoice_postcode'] = $order['InvoiceOrderUserAddress']['post_code'];
            } else {
                $tmp['invoice_fullname'] = "{$order['BillingOrderUserAddress']['first_name']} {$order['BillingOrderUserAddress']['last_name']}";
                $tmp['invoice_company'] = ''; // $order['InvoiceOrderUserAddress']['company'];
                $tmp['invoice_nip'] = ''; // $order['InvoiceOrderUserAddress']['nip'];
                $tmp['invoice_address'] = $order['BillingOrderUserAddress']['street_address'];
                $tmp['invoice_city'] = $order['BillingOrderUserAddress']['city'];
                $tmp['invoice_postcode'] = $order['BillingOrderUserAddress']['post_code'];
            }
            
            $tmp['phone'] = $order['Customer']['contact_phone'];
            $tmp['email'] = $order['Customer']['email'];
            $tmp['date_add'] = strtotime($order['Order']['created']);
            $tmp['payment_method'] = $order['Payment']['name'];
            $tmp['user_comments'] = $order['Order']['add_info'];
            $tmp['delivery_method'] = $order['Shipping']['name'];
            $tmp['delivery_price'] = $order['Order']['shipping_price_tax'];
            $tmp['delivery_point_name'] = $order['Order']['shipping_inpost'];
            
            $tmp['products'] = array();
            foreach ( $order['OrderProduct'] as $product ) {
                $prod = [];
                $prod['id'] = $product['product_id'];
                $prod['name'] = $product['name'];
                $prod['quantity'] = intval($product['amount']);
                $prod['price'] = $product['price_tax'];
                $prod['tax'] = 23;
                $prod['weight'] = 0;
                $prod['attributes'] = [];
                $prod['variant_id'] = (! empty($product['products_products_option_id'])) ? $product['products_products_option_id'] : 0;
                
                $tmp['products'][] = $prod;
            }
            $response[$order['Order']['id']] = $tmp;
        }
        
        return $response;
    }



    /**
     * Funkcja aktualizuje zamówienia wcześniej dodane do bazy
     * W przypadku zapisywania numeru nadawaczego, parametr orders_ids będzie przyjmował zawsze tablicę z jedną pozycją
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu, zawiera informacje o aktualizacji zamówienia w formacie:
     *            orders_ids => ID zamówień
     *            update_type => typ zmiany - 'status', `delivery_number`, lub 'paid' (aktualizacja statusu zamówienia, dodanie numeru nadawczego i dodanie/usunięcie wpłaty)
     *            update_value => aktualizowana wartość - ID statusu, numer nadawczy lub informacja o opłaceniu zamówienia (bool true/false)
     * @return array $response tablica zawierająca potwierdzenie zmiany:
     *         'counter' => ilość zamówień w których dokonano zmiany (nawet jeśli zamówienie pozostało takie jak wcześniej)
     */
    protected function shop_OrderUpdate($request) {
        $counter = 0;
        
        $this->loadModel('KeyAdmin.OrderStatus');
        $this->loadModel('KeyAdmin.OrderStatusHistory');
        // dla wszystkich nadesłanych numerów zamówień
        foreach ( $request['orders_ids'] as $orderId ) {
            if (intval($orderId) != $orderId) {
                continue;
            }
            
            // zmiana statusu
            if ($request['update_type'] == 'status') {
                $this->Order->updateAll([
                    'Order.order_status_id' => $request['update_value']
                ], [
                    'Order.id' => $orderId
                ]);
                
                $this->OrderStatusHistory->create();
                $toSave = [
                    'user_id' => 15,
                    'order_id' => $orderId,
                    'order_status_id' => $request['update_value']
                ];
            } elseif ($request['update_type'] == 'delivery_number') {
                $this->Order->updateAll([
                    'Order.shipment_no' => $request['update_value']
                ], [
                    'Order.id' => $orderId
                ]);
            } elseif ($request['update_type'] == 'paid') {
                $this->Order->updateAll([
                    'Order.paid' => intval($request['update_value'])
                ], [
                    'Order.id' => $orderId
                ]);
            }
            
            $counter ++;
        }
        
        return array(
            'counter' => $counter
        );
    }



    /**
     * Funkcja zwraca listę dostępnych statusów zamówień
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu, w przypadku tej funkcji nie używana
     * @return array $response tablica zawierająca dostępne statusy zamówień:
     *         'status_id' => nazwa statusu
     */
    protected function shop_StatusesList($request) {
        $this->loadModel('KeyAdmin.OrderStatus');
        
        return $this->OrderStatus->find('list');
    }



    /**
     * Funkcja zwraca listę dostępnych sposobów wysyłki
     *
     * @global array $options : tablica z ustawieniami ogólnymi z początku pliku
     * @param array $request
     *            tablica z żadaniem od systemu, w przypadku tej funkcji nie używana
     * @return array $response tablica zawierająca dostępne sposoby_wysyłki:
     *         'delivery_id' => nazwa wysyłki
     */
    protected function shop_DeliveryMethodsList($request) {
        $this->loadModel('KeyAdmin.Shipping');
        
        return $this->Shipping->find('list');
    }



    public function errorHandle($errorCode, $errorText = '') {
        print json_encode(array(
            'error' => true,
            'error_code' => $errorCode,
            'error_text' => $errorText
        ));
        exit();
    }
}
