<?php

App::uses('CommonController', CLIENT.'.Controller');

class CartController extends CommonController {

  public $uses = array('KeyAdmin.Product', 'KeyAdmin.Cart', 'KeyAdmin.CartProduct', 'KeyAdmin.Customer', 'KeyAdmin.Order');
  public $components = array('RequestHandler', 'KeyAdmin.NotificationCenter', 'CartManage');
  public $helpers = array();
  
  public function __construct($request = null, $response = null) {
    $this->helpers[] = CLIENT.'.User';
    parent::__construct($request, $response);
  }
  
  /**
   * (non-PHPdoc)
   * @see Controller::beforeFilter()
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow();
  }

  public function index() {
    $this->CartManage->recalcCart();
    $this->CartManage->reloadSessionCart();
    
    $cart = $this->CartManage->getSessionCart();
    $products_weight = 0;
    $products_amount= 0;
    if (isset($cart['CartProduct'])) {
      foreach ((array) $cart['CartProduct'] as $cartProduct) {
        $products_weight += (float) $cartProduct['weight'];
        $products_amount += (float) $cartProduct['amount'];
      }
    }
    
    $params = array(
      'weight' => (float) $products_weight,
      'products_price_tax' => isset($cart['Cart']['products_price_tax']) ? (float) $cart['Cart']['products_price_tax'] : 0,
      'products_amount' => (float) $products_amount,
      'country_id' => (!empty($cart['ShippingOrderUserAddress']['country_id']))? intval($cart['ShippingOrderUserAddress']['country_id']): 170,
      'products' => isset($cart['CartProduct']) ? $cart['CartProduct'] : array()
    );
    
    $shippingsFull = $this->Order->getShippingForOrder(0, $params);
    
    $prices = Hash::extract($shippingsFull, '{n}.Shipping.for_order_price_tax');
    $deliveryPrices = array_filter($prices, function($value){      
      return $value > 0;
    });
    $this->set(compact('deliveryPrices', 'shippingsFull'));
    
    $this->render('Cart/index');
  }
  
  public function add_product() {
    $mode = $this->data['mode'];
    $productId = (int) $this->data['productId'];
    $productOptions = isset($this->data['productOptions']) ? $this->data['productOptions'] : array();
    $amount = isset($this->data['amount']) ? $this->data['amount'] : 'auto';
    $multiData = isset($this->data['multiData']) ? $this->data['multiData'] : array();
    
    if (empty($mode)) {
      $mode = 'normal';
    }
    
    $error = null;
    $message = '';
    
    if ($mode == 'quick_multi') {
      $success = true;
      try {
        foreach ($multiData as $item) {
          $cartProduct = $this->CartManage->addProduct($item['product_id'], $amount, isset($item['options']) ? $item['options'] : null);
          if (!$cartProduct) {
            $success = false;
          }
        }
        
        if ($success) {
          $message = __('Produkty zostały dodane do koszyka');
        } else {
          $error = true;
          $message = __('Nie udało się dodać jednego lub więcej produktów do koszyka, spróbuj ponownie.');
        }
      } catch (Exception $ex) {
        $error = true;
        if ($ex->getMessage() == 'choose_options') {
          $message = __('Jeden lub więcej produkt dostępnych jest w wielu wersjach, przejdź do strony produktu, aby wybrać właściwą.');
        } else {
          $message = $ex->getMessage();
        }
      }
    } else {
      try {
        $cartProduct = $this->CartManage->addProduct($productId, $amount, $productOptions);

        if ($cartProduct) {
          $message = __('Produkt "'.$cartProduct['name'].'" został dodany do koszyka');
        } else {
          $error = true;
          $message = __('Nie udało się dodać produktu do koszyka, spróbuj ponownie.');
        }
      } catch (Exception $ex) {
        $error = true;
        if ($ex->getMessage() == 'choose_options' && $mode == 'quick') {
          $message = __('Produkt dostępny w wielu wersjach, przejdź do strony produktu, aby wybrać właściwą.');
        } else {
          $message = $ex->getMessage();
        }
      }
    }
    
    $view = new View($this, false);
    $cart = $this->CartManage->getSessionCart();
    $miniCart = $view->element('mini_cart', array('cart' => $cart));
    $miniCartMobile = $view->element('mini_cart_mobile', array('cart' => $cart));
    $cartResponse = $view->element('cart_response', array('cart' => $cart));
    
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'result' => array(
        'success' => !$error,
        'message' => $message,
        'miniCart' => $miniCart,
        'miniCartMobile' => $miniCartMobile,
        'cartResponse' => $cartResponse
      )
    ));
    $this->set('_serialize', array('result'));
  }
  
  public function delete_cart_product() {
    $cartProductId = (int) $this->params->query['cartProductId'];
    
    $error = null;
    $message = '';
    
    try {
      if ($cartProduct = $this->CartManage->deleteCartProduct($cartProductId)) {
        $message = __('Produkt "'.$cartProduct['name'].'" został usunięty z koszyka');
      } else {
        $error = true;
        $message = __('Nie udało się usunąć produktu z koszyka, spróbuj ponownie.');
      }
    } catch (Exception $ex) {
      $error = true;
      $message = $ex->getMessage();
    }
    
    $view = new View($this, false);
    $cart = $this->CartManage->getSessionCart();
    $miniCart = $view->element('mini_cart', array('cart' => $cart));
    $miniCartMobile = $view->element('mini_cart_mobile', array('cart' => $cart));
    $cartResponse = $view->element('cart_response', array('cart' => $cart));
    
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'result' => array(
        'success' => !$error,
        'message' => $message,
        'miniCart' => $miniCart,
        'miniCartMobile' => $miniCartMobile,
        'cartResponse' => $cartResponse
      )
    ));
    $this->set('_serialize', array('result'));
  }
  
  public function save_cart_product_amounts() {
    $error = null;
    $message = '';
    
    try {
      foreach ($this->data['amounts'] as $cartProductId => $amount) {
        if ($amount == 0) {
          $this->CartManage->deleteCartProduct($cartProductId);
        } else {
          $this->CartManage->changeCartProductAmount($cartProductId, $amount, false);
        }
      }
      $this->CartManage->reloadSessionCart();
      $this->CartManage->recalcCart();
    } catch (Exception $ex) {
      $error = true;
      $message = $ex->getMessage();
    }
    
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'result' => array(
        'success' => !$error,
        'message' => $message,
      )
    ));
    $this->set('_serialize', array('result'));
  }
  
  public function order_account() {
    $this->checkOrderProcessAvailable('account');
    
    $this->setRegiterValidations();
    if ($this->request->is(array('post', 'put'))) {
      if ($this->request->data['mode'] == 'login') {
        $logged = $this->requestAction(
            array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'login'),
            array('named' => array('internal' => true, 'email' => $this->request->data['User']['email'], 'password' => $this->request->data['User']['password'])));
        
        if ($logged) {
          return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'));
        } else {
          $this->request->data['User']['password'] = '';
          $this->Session->setFlash(__('Niepoprawny e-mail i/lub hasło'), 'flash-error', array(), 'order_account_login');
        }
      } elseif ($this->request->data['mode'] == 'register') {
        $this->Customer->create();
        
        $group = $this->Customer->UserGroup->find('first', array('conditions' => array('admin' => 0)));
        $this->request->data['Customer']['user_group_id'] = $group['UserGroup']['id'];
        $this->request->data['Customer']['user_address'] = '';
        $this->request->data['Customer']['guest_account'] = $this->request->data['Customer']['non_guest_account'] ? 0 : 1;
        
        $this->request->data['UserData'] = array(
          'status' => 1
        );
        
        $this->request->data['UserAddress'][0]['alias'] = __('Adres główny');
        $this->request->data['UserAddress'][0]['default_billing'] = true;
        $this->request->data['UserAddress'][0]['default_shipping'] = $this->request->data['Customer']['is_shipping_address'] ? 1 : 0;
        $this->request->data['UserAddress'][0]['is_company'] = empty($this->request->data['Customer']['company']) ? 0 : 1;
        $this->request->data['UserAddress'][0]['company'] = $this->request->data['Customer']['company'];
        $this->request->data['UserAddress'][0]['first_name'] = $this->request->data['Customer']['first_name'];
        $this->request->data['UserAddress'][0]['last_name'] = $this->request->data['Customer']['last_name'];
        $this->request->data['UserAddress'][0]['phone'] = $this->request->data['Customer']['contact_phone'];
        
        if (!empty($this->request->data['Customer']['guest_account'])){
          $this->request->data['Customer']['password'] = '';
          $this->request->data['Customer']['password2'] = '';
        }
        if ($this->Customer->saveAll($this->request->data, array('noAddress' => true))) {
          $this->CartManage->setUserId($this->Customer->getLastInsertID());
          
          if (!$this->request->data['Customer']['guest_account']) {
            $this->Customer->recursive = 2;
            $customer = $this->Customer->findById($this->Customer->getLastInsertID());
            $this->Auth->login($customer['Customer']);

            $this->NotificationCenter->send('account_registration', array($customer['Customer']['email'], trim($customer['Customer']['first_name'].' '.$customer['Customer']['last_name'])), $customer);
          }
          
          if ($this->request->data['UserAddress'][0]['default_shipping']) {
            return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'));
          } else {
            return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'));
          }
        } else {
          $this->request->data['Customer']['non_guest_account'] = $this->request->data['Customer']['guest_account'] ? 0 : 1;
          $this->request->data['Customer']['password'] = $this->request->data['Customer']['password2'] = '';
          $this->Session->setFlash(__('Formularz zawiera błędy'), 'flash-error', array(), 'order_account_register');
        }
      }
    } else {
      $this->request->data['Customer'] = array('non_guest_account' => 1);
    }
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $defaultCountry = $this->Customer->UserAddress->Country->find('first', array(
      'conditions' => array(
        'Country.default' => 1
      )
    ));
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => isset($this->request->data['UserAddress'][0]['country_id']) ? $this->request->data['UserAddress'][0]['country_id'] :  $defaultCountry['Country']['id']
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    $this->set(compact('countries', 'zones'));
    $this->set('defaultCountryId', $defaultCountry['Country']['id']);
    
  }
  
  protected function setRegiterValidations() {
    if (!$this->request->is(array('post', 'put'))) {
      /*$this->Customer->validator()->add('password', array(
        'minLength' => array(
          'rule' => array('minLength', '3'),
          'allowEmpty' => false,
          'required' => true,
          'message' => 'Hasło jest wymagane, min. 3 znaki'
        ),
      ));
      $this->Customer->validator()->add('password2', array(
        'same' => array(
          'rule' => 'validatePasswordConfirm',
          'allowEmpty' => false,
          'required' => true,
          'message' => 'Hasła różnią się od siebie'
        ),
      ));*/
    }
    $this->Customer->validator()->add('accept_rules', array(
      'rule1' => array(
        'rule' => 'naturalNumber',
        'allowEmpty' => false,
        'message' => 'Musisz zaakceptować regulamin i politykę prywatności'
      ),
    ));
  }
  
  public function order_address() {
    $this->checkOrderProcessAvailable('addresses');
    
    $cart = $this->CartManage->getSessionCart(true);
    $this->Customer->contain('UserAddress');
    $customer = $this->Customer->findById($cart['Cart']['user_id']);
    
    $changed = false;
    if ($cart['Cart']['billing_order_address_id'] <= 0) {
      foreach ($customer['UserAddress'] as $address) {
        if ($address['default_billing']) {
          $this->CartManage->setBillingAddress($address['id']);
          $changed = true;
        }
      }
    }
    if ($cart['Cart']['shipping_order_address_id'] <= 0) {
      foreach ($customer['UserAddress'] as $address) {
        if ($address['default_shipping']) {
          $this->CartManage->setShippingAddress($address['id']);
          $changed = true;
        }
      }
    }
    
    if ($changed) {
      $cart = $this->CartManage->getSessionCart();
    }
    
    if ($cart['Cart']['billing_order_address_id'] <= 0) {
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'billing'));
    }
    if ($cart['Cart']['shipping_order_address_id'] <= 0) {
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'shipping'));
    }
  }
  
  public function order_address_edit($id) {
    $this->checkOrderProcessAvailable('addresses');
    
    if (!$id) {
      throw new NotFoundException(__('Wybrany adres nie istnieje'));
    }
    $cart = $this->CartManage->getSessionCart(true);
    $address = $this->Customer->UserAddress->findById($id);
    if (!$address || $address['UserAddress']['user_id'] != $cart['Cart']['user_id']) {
      throw new NotFoundException(__('Nie odnaleziono wybranego adresu'));
    }
    
    $from = $this->params['named']['from'];
    
    $this->Customer->UserAddress->contain('Country');
    $userAddresses = $this->Customer->UserAddress->find('all', array(
      'conditions' => array(
        'UserAddress.user_id' => $cart['Cart']['user_id'],
        'UserAddress.id !=' => $id
      )
    ));
    
    
    if ($this->request->is(array('post', 'put'))) {
      $this->request->data['UserAddress']['id'] = $id;
      $this->request->data['UserAddress']['user_id'] = $cart['Cart']['user_id'];
      $this->request->data['UserAddress']['alias'] = $address['UserAddress']['alias'];
      if (empty($this->request->data['UserAddress']['alias'])) {
        $this->request->data['UserAddress']['alias'] = __('Adres dodatkowy');
      }
      
      if ($this->Customer->UserAddress->save($this->request->data)) {
        $cart = $this->CartManage->getSessionCart(true);
        if (isset($cart['BillingOrderUserAddress']['user_address_id']) && $cart['BillingOrderUserAddress']['user_address_id'] == $id) {
          $this->CartManage->setBillingAddress($id);
        }
        if (isset($cart['ShippingOrderUserAddress']['user_address_id']) && $cart['ShippingOrderUserAddress']['user_address_id'] == $id) {
          $this->CartManage->setShippingAddress($id);
        }
        
        //$this->Session->setFlash(__('Adres został zapisany.'), 'flash-success');
        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać adresu. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->data = $address;
    }
    
    
    $this->set('create', false);
    $this->set('from', $from);
    $this->set('froma', 'edit-'.$id);
    $this->set('userAddresses', $userAddresses);
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $defaultCountry = $this->Customer->UserAddress->Country->find('first', array(
      'conditions' => array(
        'Country.default' => 1
      )
    ));
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => $this->request->data['UserAddress']['country_id']
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    $this->set(compact('countries', 'zones'));
    $this->set('defaultCountryId', $defaultCountry['Country']['id']);
    
    $this->render('Cart/order_address_form');
  }
  
  public function order_address_new() {
    $this->checkOrderProcessAvailable('addresses');
    
    $from = isset($this->params['named']['from']) ? $this->params['named']['from'] : null;
    $cart = $this->CartManage->getSessionCart(true);
    
    $this->Customer->UserAddress->contain('Country');
    $userAddresses = $this->Customer->UserAddress->find('all', array(
      'conditions' => array(
        'UserAddress.user_id' => $cart['Cart']['user_id']
      )
    ));
    
    
    if ($this->request->is(array('post', 'put'))) {
      $this->Customer->UserAddress->create();
      $this->request->data['UserAddress']['user_id'] = $cart['Cart']['user_id'];
      $this->request->data['UserAddress']['alias'] = __('Adres dodatkowy');
      
      if ($this->Customer->UserAddress->save($this->request->data)) {
        $id = $this->Customer->UserAddress->getLastInsertID();
        $cart = $this->CartManage->getSessionCart();
        if ($from == 'billing') {
          $this->CartManage->setBillingAddress($id);
        }
        if ($from == 'shipping' || (isset($cart['BillingOrderUserAddress']['user_address_id']) && isset($cart['ShippingOrderUserAddress']['user_address_id']) && $cart['BillingOrderUserAddress']['user_address_id'] == $cart['ShippingOrderUserAddress']['user_address_id'])) {
          $this->CartManage->setShippingAddress($id);
        }
        
        //$this->Session->setFlash(__('Adres został zapisany.'), 'flash-success');
        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać adresu. Sprawdź poprawność podanych danych.'), 'flash-error');
    }
    
    $this->set('create', true);
    $this->set('from', $from);
    $this->set('froma', 'new');
    $this->set('userAddresses', $userAddresses);
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $defaultCountry = $this->Customer->UserAddress->Country->find('first', array(
      'conditions' => array(
        'Country.default' => 1
      )
    ));
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => isset($this->request->data['UserAddress']['country_id']) ? $this->request->data['UserAddress']['country_id'] :  $defaultCountry['Country']['id']
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    $this->set(compact('countries', 'zones'));
    $this->set('defaultCountryId', $defaultCountry['Country']['id']);
    
    $this->render('Cart/order_address_form');
  }
  
  public function order_address_delete($id) {
    $this->checkOrderProcessAvailable('addresses');
    
    if (!$id) {
      throw new NotFoundException(__('Wybrany adres nie istnieje'));
    }
    
    $cart = $this->CartManage->getSessionCart(true);
    $address = $this->Customer->UserAddress->findById($id);
    if (!$address || $address['UserAddress']['user_id'] != $cart['Cart']['user_id']) {
      throw new NotFoundException(__('Nie odnaleziono wybranego adresu'));
    }
    
    $from = $this->params['named']['from'];
    $froma = $this->params['named']['froma'];
    
    if (!$address['UserAddress']['default_billing'] && !$address['UserAddress']['default_shipping']) {
      $this->Customer->UserAddress->delete($id);
    }
    
    if ($froma == 'new') {
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => $from));
    } elseif (substr($froma, 0, strlen('edit-')) == 'edit-') {
      $eid = substr($froma, strlen('edit-'));
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_edit', $eid, 'from' => $from));
    }
  }
  
  public function order_address_choose($id) {
    $this->checkOrderProcessAvailable('addresses');
    
    if (!$id) {
      throw new NotFoundException(__('Wybrany adres nie istnieje'));
    }
    
    $cart = $this->CartManage->getSessionCart(true);
    $address = $this->Customer->UserAddress->findById($id);
    if (!$address || $address['UserAddress']['user_id'] != $cart['Cart']['user_id']) {
      throw new NotFoundException(__('Nie odnaleziono wybranego adresu'));
    }
    
    $from = $this->params['named']['from'];
    
    $cart = $this->CartManage->getSessionCart();
    if ($from == 'billing') {
      $this->CartManage->setBillingAddress($id);
    }
    if ($from == 'shipping' || (isset($cart['BillingOrderUserAddress']['user_address_id']) && isset($cart['ShippingOrderUserAddress']['user_address_id']) && $cart['BillingOrderUserAddress']['user_address_id'] == $cart['ShippingOrderUserAddress']['user_address_id'])) {
      $this->CartManage->setShippingAddress($id);
    }
    
    return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'));
  }
  
  public function order_payment_shipping() {
    $this->checkOrderProcessAvailable('payment_shipping');
    $cart = $this->CartManage->getSessionCart(true);
    
    if ($this->request->is(array('post', 'put'))) {
      //print_r($this->data);die;
      if (!isset($this->data['Cart'])) {
        $this->request->data['Cart'] = array();
      }
      if (!isset($this->request->data['Cart']['shipping_id'])) {
        $this->request->data['Cart']['shipping_id'] = 0;
      }
      if (!isset($this->request->data['Cart']['payment_id'])) {
        $this->request->data['Cart']['payment_id'] = 0;
      }
      
      if (isset($this->request->data['Cart']['shipping_id']) && $this->request->data['Cart']['shipping_id'] > 0) {
        $this->CartManage->setShipping($this->data['Cart']['shipping_id']);
      }
      if (isset($this->request->data['Cart']['payment_id']) && $this->request->data['Cart']['payment_id'] > 0) {
        $this->CartManage->setPayment($this->data['Cart']['payment_id']);
      }
      $cart = $this->CartManage->getSessionCart();
      
      if ($cart['Cart']['shipping_id'] <= 0 && $cart['Cart']['payment_id'] <= 0) {
        $this->Session->setFlash(__('Wybierz formę dostawy i płatności'), 'flash-error', array(), 'order_process');
      } elseif ($cart['Cart']['shipping_id'] <= 0) {
        $this->Session->setFlash(__('Wybierz formę dostawy'), 'flash-error', array(), 'order_process');
      } elseif ($cart['Cart']['payment_id'] <= 0) {
        $this->Session->setFlash(__('Wybierz formę płatności'), 'flash-error', array(), 'order_process');
      } else {
        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_confirm'));
      }
    }
    
    $products_weight = 0;
    $products_amount= 0;
    foreach ($cart['CartProduct'] as $cartProduct) {
      $products_weight += (float) $cartProduct['weight'];
      $products_amount += (float) $cartProduct['amount'];
    }
    
    $params = array(
      'weight' => (float) $products_weight,
      'products_price_tax' => (float) $cart['Cart']['products_price_tax'],
      'products_amount' => (float) $products_amount,
      'country_id' => (int) $cart['ShippingOrderUserAddress']['country_id'],
      'products' => $cart['CartProduct']
    );
    
    $shippingsFull = $this->Order->getShippingForOrder(0, $params);
    $this->set('shippingsFull', $shippingsFull);
    
    $payments = array();
    if (!empty($cart['Cart']['shipping_id'])) {
      $payments = $this->Order->getPaymentsForOrder(0, array('shipping_id' => (int) $cart['Cart']['shipping_id']), false);
    }
    $this->set('payments', $payments);
    //print_r($payments);die; 
  }
  
  public function order_confirm() {
    $this->checkOrderProcessAvailable('confirm');
    
    if ($this->request->is(array('post', 'put'))) {
      list($cartId, $orderId) = $this->CartManage->createOrder();
      CakeSession::write('Cart.order_id', $orderId);
      
      //notify
      $this->Order->recursive = 2;
      $orderForNotify = $this->Order->findById($orderId);
      $userForNotify = $this->Order->Customer->findById($orderForNotify['Order']['user_id']);

      if (preg_match('/^[a-zA-Z0-9._\+-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/', $userForNotify['Customer']['email'])) {
        $this->NotificationCenter->send('new_order', array($userForNotify['Customer']['email'], trim($userForNotify['Customer']['first_name'].' '.$userForNotify['Customer']['last_name'])), $userForNotify, $orderForNotify);
        
        $adminNotificationParams = $this->NotificationCenter->getBody('new_order_admin', $orderForNotify);
        
        $this->loadModel('QueueEmail');
        $emails = Configure::read('Email.order_notification_emails');
        foreach ($emails as $email) {
          $this->QueueEmail->create();
          $toSave = array('QueueEmail' => array(
            'to' => $email,
            'subject' => $adminNotificationParams['subject'],
            'body' => $adminNotificationParams['message']
          ));
          $this->QueueEmail->save($toSave);
        }
      }
      
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'pay_for_order', $orderId));
    }
  }
  
  public function checkOrderProcessAvailable($step = null) {
    $cart = $this->CartManage->getSessionCart();
    if (!isset($cart['CartProduct']) || empty($cart['CartProduct'])) {
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index'));
    }
    
    if (in_array($step, array('addresses', 'payment_shipping', 'confirm'))) {
      if ($cart['Cart']['user_id'] <= 0) {
        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'));
      }
      
      $changed = false;
      $this->Customer->contain('UserAddress');
      $customer = $this->Customer->findById($cart['Cart']['user_id']);
      if ($cart['Cart']['billing_order_address_id'] <= 0) {
        foreach ($customer['UserAddress'] as $address) {
          if ($address['default_billing']) {
            $this->CartManage->setBillingAddress($address['id']);
            $changed = true;
          }
        }
      }
      if ($cart['Cart']['shipping_order_address_id'] <= 0) {
        foreach ($customer['UserAddress'] as $address) {
          if ($address['default_shipping']) {
            $this->CartManage->setShippingAddress($address['id']);
            $changed = true;
          }
        }
      }

      if ($changed) {
        $cart = $this->CartManage->getSessionCart();
      }
    }
    
    if (in_array($step, array('payment_shipping', 'confirm'))) {
      if (empty($cart['BillingOrderUserAddress']['user_address_id'])) {
        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'billing'));
      }
      if (empty($cart['ShippingOrderUserAddress']['user_address_id'])) {
        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'shipping'));
      }
    }
    
    if (in_array($step, array('confirm'))) {
      if (empty($cart['Cart']['payment_id']) || empty($cart['Cart']['shipping_id'])) {
        return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'));
      }
    }
  }
  
  public function get_payments() {
    $cart = $this->CartManage->getSessionCart(true);
    $cart['Cart']['payment_id'] = isset($this->request->query['payment_id']) ? $this->request->query['payment_id'] : 0;
    $payments = $this->Order->getPaymentsForOrder(0, array('shipping_id' => (int) $this->request->query['shipping_id']), false);
    //print_r($payments);die;
    $view = new View($this, false);
    $view->viewPath = 'Cart';
    $view->layout = false;
    $view->set(compact('cart', 'payments'));
    $html = $view->render('order_payments');
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array('result' => array(
      'success' => 1,
      'html' => $html
    )));
    $this->set('_serialize', array('result'));
  }

}
