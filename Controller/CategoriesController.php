<?php

App::uses('CommonController', CLIENT.'.Controller');

class CategoriesController extends CommonController {

  public $uses = array('KeyAdmin.Category', 'KeyAdmin.Product', 'KeyAdmin.ProductsCategory');
  public $components = array('Paginator', 'RequestHandler');
  
  /**
   *
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('index', 'view', 'get_brands', 'get_models', 'get_wipers', 'search_wipers', 'search_wipers_index');
  }
  
  /**
   * 
   */
  public function index(){
    $this->render('Category/index');
  }
  
  /**
   * 
   */
  public function view($slug){
    $categoryId = explode('-', $slug);
    $categoryId = substr(end($categoryId), 1);
    
    if (!$this->Category->exists($categoryId)){
      throw new NotFoundException('Could not find that category');
    }
    $this->Category->id = $categoryId;
    $category = $this->Category->read();
    
    $this->set(array(
      'meta_title' => (!empty($category['Category']['meta_title_tag']))? $category['Category']['meta_title_tag']: $category['Category']['name'],
      'meta_desc' => $category['Category']['meta_desc_tag'],
      'meta_keywords' => $category['Category']['meta_keywords_tag'],
    ));
    
    if ($categoryId == 854 && isset($this->params['named']['page']) && $this->params['named']['page']){
      $this->set('meta_title', 'Akcesoria sam.');
    }
    //--
    
    $breadcrumbsCategories = array();
    $current = $category;
    $activeCategories = array($category['Category']['id']);
    while ($current['Category']['parent_id'] > 0) {
      $current = $this->Category->find('first', array(
        'conditions' => array(
          'Category.id' => $current['Category']['parent_id']
        )
      ));
      $activeCategories[] = $current['Category']['id'];
      $breadcrumbsCategories[] = $current;
    }
    $breadcrumbsCategories = array_reverse($breadcrumbsCategories);
    $breadcrumbsCategories[] = $category;
    
    //--
    
    $parentCategory = $breadcrumbsCategories[0];
    $subcategories = array();
    foreach ($activeCategories as $activeCategoryId) {
      $subtmp = $this->Category->find('all', array(
        'conditions' => array(
          'Category.parent_id' => $activeCategoryId,
          'Category.status' => 1
        ),
        'order' => 'Category.sort_order ASC'
      ));
      if (!empty($subtmp)) {
        $subcategories[$activeCategoryId] = $subtmp;
      }
    }
    
    //--
    
    $sort = 'al_default';
    $sortAvailable = array('al_default', 'al_price', 'al_price-desc');
    if (isset($this->request->params['named']['sort'])) {
      $sort = $this->request->params['named']['sort'];
    } else {
      $sort = CakeSession::read('CategoryProducts.sort');
    }
    if (!in_array($sort, array('al_default', 'al_price', 'al_price-desc'))) {
      $sort = 'al_default';
    }
    CakeSession::write('CategoryProducts.sort', $sort);
    $this->Paginator->settings['sort'] = $sort;
    
    
    
    //--
    $childs = $this->Category->find('all', array(
  	  'fields' => array('Category.id'),
      'conditions' => array('Category.parent_id' => $categoryId)
    ));
    $categoryIds = array($categoryId);
    if (!empty($childs)){
      $childIds = Hash::extract($childs, '{n}.Category.id');
      $categoryIds = array_merge($categoryIds, $childIds);
    }
    
    
    $this->Paginator->settings['contain'] = array(
      'Category' => array('conditions' => array('Category.status' => 1)),  
      'Product' => array('conditions' => array('Product.status' => 1)), 
      'Product.ProductsImage' => array('order' => array(
      'sort_order' => 'ASC'
    )), 'Product.Manufacturer');
    $this->Paginator->settings['conditions'] = array('ProductsCategory.category_id' => $categoryIds);
    $this->Paginator->settings['limit'] = 20;
    $this->Paginator->settings['group'] = array('ProductsCategory.product_id');
    $products = $this->Paginator->paginate('ProductsCategory');
    
    //$dbo = $this->ProductsCategory->getDatasource();
    //print_r($dbo->getLog());die;
  
    
    $this->set(compact('category', 'subcategories', 'breadcrumbsCategories', 'activeCategories', 'sort', 'products'));
    
    App::uses('AppHelper', 'View/Helper');
    $appHelper = new AppHelper(new View());
    $this->set('canonical', $appHelper->getCategoryViewUrl($category));
    
    $this->render('Category/index');
  }
  
  
  /**
   * 
   */
  public function get_brands(){
    $this->loadModel(CLIENT.'.WipersCategory');
    $brands = $this->WipersCategory->find('list', array(
      'conditions' => array('WipersCategory.parent_id' => 0),
      'fields' => array('id', 'name'),
      'order' => array('WipersCategory.name' => 'ASC')      
    ));
    $this->layout = 'ajax';
    
    $this->set(compact('brands'));
  }
  
  /**
   * 
   */
  public function get_models($categoryId){
    $this->loadModel(CLIENT.'.WipersCategory');
    $models = $this->WipersCategory->find('list', array(
        'conditions' => array('WipersCategory.parent_id' => $categoryId),
        'fields' => array('id', 'name'),
        'order' => array('WipersCategory.name' => 'ASC')
    ));
    $this->layout = 'ajax';
    
    $this->set(compact('models'));
  }
  
  /**
   * 
   */
  public function search_wipers($categoryId){
    $this->Paginator->settings['contain'] = array('Category', 'Product', 'Product.ProductsImage');
    $this->Paginator->settings['conditions'] = array('ProductsCategory.category_id' => $categoryId);
    $this->Paginator->settings['limit'] = 6;
    $products = $this->Paginator->paginate('ProductsCategory');
    
    $this->set(compact('products'));
  }
  
  public function search_wipers_index() {
    $categoryId = (isset($this->params->named['category_id']))? $this->params->named['category_id']: 0;
    
    $this->loadModel(CLIENT.'.WipersCategory');
    $model = $this->WipersCategory->findById($categoryId);
    $brandId = (isset($model['WipersCategory']['parent_id']))? $model['WipersCategory']['parent_id']: null;
    $modelId = $categoryId;
    
    
    $brands = $this->WipersCategory->find('list', array(
      'conditions' => array('WipersCategory.parent_id' => 0),
      'fields' => array('id', 'name'),
      'order' => array('WipersCategory.name' => 'ASC')      
    ));
    
    $models = $this->WipersCategory->find('list', array(
      'conditions' => array('WipersCategory.parent_id' => (isset($model['WipersCategory']['parent_id']))? $model['WipersCategory']['parent_id']: null),
      'fields' => array('id', 'name'),
      'order' => array('WipersCategory.name' => 'ASC')
    ));
    
    $wipers = $this->WipersCategory->find('list', array(
      'conditions' => array('WipersCategory.parent_id' => $categoryId),
      'fields' => array('id', 'name'),
      'order' => array('WipersCategory.name' => 'ASC')
    ));
    
    $wipersIds = array_keys($wipers);
    
    $this->Paginator->settings['contain'] = array('WipersCategory', 'Product', 'Product.ProductsImage', 'ProductsProductsOption', 'ProductsProductsOption.ProductOptionValue');
    $this->Paginator->settings['conditions'] = array('WipersProductsCategory.category_id' => $wipersIds);
    
    $this->loadModel(CLIENT.'.WipersProductsCategory');
    $products = $this->Paginator->paginate('WipersProductsCategory');
    
    $groupedProducts = array();
    foreach ($products as $product) {
      $categoryId = $product['WipersProductsCategory']['category_id'];
      if (!isset($groupedProducts[$categoryId])) {
        $groupedProducts[$categoryId] = array(
          'Category' => $product['WipersCategory'],
          'Products' => array(),
          'group_price' => 0
        );
      }
      
      $prod = $product['Product'];
      $prod['Product'] = $product['Product'];
      $prod['ProductsProductsOption'] = $product['ProductsProductsOption'];
      
      $groupedProducts[$categoryId]['Products'][] = $prod;
      if (!empty($prod['ProductsProductsOption']['id'])){
        $groupedProducts[$categoryId]['group_price'] += $prod['ProductsProductsOption']['change_price_tax'];
      }else{
        $groupedProducts[$categoryId]['group_price'] += $prod['price_tax'];
      }      
    }
    $groupedProducts = Hash::sort($groupedProducts, '{n}.group_price', 'DESC'); 
    
    $this->set(compact('brands', 'models', 'wipers', 'brandId', 'modelId', 'groupedProducts', 'products'));
  }
}
