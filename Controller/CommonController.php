<?php

App::uses('AppController', 'Controller');

class CommonController extends AppController {

  public $helpers = array('AssetCompress.AssetCompress');
  public $components = array('RequestHandler', 'CartManage');

  public function __construct($request = null, $response = null) {
    $this->helpers[] = CLIENT.'.Cart';
    parent::__construct($request, $response);
  }
  
  public function beforeFilter() {
    parent::beforeFilter();
    
    AuthComponent::$sessionKey = CLIENT;
    $this->Auth->loginAction = array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'login', 'admin' => false);
    $this->Auth->loginRedirect = array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'account', 'admin' => false);
    $this->Auth->logoutRedirect = array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home', 'admin' => false);
    $this->Auth->authError = __('Zaloguj się, aby wyświetlić podaną stronę');
    $this->Auth->authenticate = array(
      'Form' => array(
        'passwordHasher' => array(
          'className' => 'Simple',
          'hashType' => 'sha1'
        ),
        'fields' => array(
          'username' => 'email',
          'password' => 'password'
        )
      )
    );
    
    $this->set('authUser', $this->Auth->user());
  }
}
