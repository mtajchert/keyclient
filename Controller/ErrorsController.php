<?php

App::uses('CommonController', CLIENT.'.Controller');

class ErrorsController extends CommonController {
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow();
  }

  public function error404() {
    $this->render('Errors/error400');
  }

}
