<?php

App::uses('CommonController', CLIENT.'.Controller');

class OrderController extends CommonController {

  public $uses = array('KeyAdmin.Page', 'KeyAdmin.Order');
  public $components = array('RequestHandler', 'KeyAdmin.NotificationCenter', 'CartManage');
  
  public function __construct($request = null, $response = null) {
    $this->helpers[] = CLIENT.'.User';
    parent::__construct($request, $response);
  }
  
  /**
   * (non-PHPdoc)
   * @see Controller::beforeFilter()
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('payment_status', 'pay_for_order', 'status', 'after_payment');
  }

  public function index() {
    //--
  }
  
  public function pay_for_order($id) {
    $orderId = CakeSession::read('Cart.order_id');
    
    if ($id != $orderId && $this->Auth->user('id') <= 0) {
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'login'));
    }
    
    if (!$id) {
      throw new NotFoundException(__('Wybrane zamówienie nie istnieje'));
    }
    
    $this->Order->contain(array('Customer', 'Payment', 'BillingOrderUserAddress', 'Shipping'));
    $order = $this->Order->findById($id);
    if (!$order || ($id != $orderId && $order['Order']['user_id'] != $this->Auth->user('id'))) {
      throw new NotFoundException(__('Nie odnaleziono wybranego zamówienia'));
    }
    
    $page = $this->Page->find('first', array(
      'conditions' => array(
        'Page.group' => 'payment',
        'Page.code' => $order['Payment']['code']
      )
    ));
    if (!empty($page)){
      $page['Page']['content'] = str_replace('{ORDER_ID}', '#'.$order['Order']['id'], $page['Page']['content']);
      $page['Page']['content'] = str_replace('{PRICE}', $order['Order']['order_price_tax'], $page['Page']['content']);
      $page['Page']['content'] = str_replace('{DELIVERY}', $order['Shipping']['name'], $page['Page']['content']);
    }
    

    $this->set('page', $page);
    
    if ($order['Payment']['code'] == 'przelewy24') {
      App::import('Lib', CLIENT.'.Przelewy24');
      $P24 = new Przelewy24(Configure::read('P24.config'));
      
      //echo $order['Order']['id'];die();
      $P24->addValue('p24_session_id', $order['Order']['id']);
      $P24->addValue('p24_amount', $order['Order']['order_price_tax']*100);
      $P24->addValue('p24_currency', 'PLN');
      $P24->addValue('p24_description', 'Zamówienie nr #'.$order['Order']['id']);
      $P24->addValue('p24_email', $order['Customer']['email']);
      $P24->addValue('p24_client', $order['BillingOrderUserAddress']['first_name'].' '.$order['BillingOrderUserAddress']['last_name']);
      $P24->addValue('p24_address', $order['BillingOrderUserAddress']['street_address']);
      $P24->addValue('p24_zip', $order['BillingOrderUserAddress']['post_code']);
      $P24->addValue('p24_city', $order['BillingOrderUserAddress']['city']);
      $P24->addValue('p24_country', 'PL');
      $P24->addValue('p24_api_version', '3.2');
      $P24->addValue('p24_url_return', Router::url(array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'after_payment', $order['Order']['id']), true));
      $P24->addValue('p24_url_status', Router::url(array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'payment_status', $order['Order']['id']), true));
      //$P24->addValue('p24_url_status', 'http://859ee813.ngrok.io/zamowienie/status/'.$order['Order']['id']);
      $P24->addValue('p24_encoding', 'UTF-8');      
       
      $response = $P24->trnRegister(false);
      
      if ($response['error'] == 0){           
        $P24->trnRequest($response['token']);
        die();
      }else{
        $this->Session->setFlash(__('Błąd systemu płatności. Spróbuj ponowanie za chwilę.'), 'flash-error');
      }
    }else{
      //$this->Session->setFlash(__('Błąd systemu płatności. Spróbuj ponowanie za chwilę.'), 'flash-error');
      
    }
  }
  

  
  /**
   * 
   */
  public function after_payment($orderId){
    $this->Session->setFlash(__('Oczekujemy na potwierdzenie z serwisu transakcyjnego.'), 'flash-success', array(), 'payment_process');
    $userId = $this->Auth->user('id');
    if (!empty($userId)){
      $this->redirect(array('plugin' => CLIENT,'controller' => 'Order', 'action' => 'view', $orderId));
    }else{
      $this->redirect('/');
    }       
  }
  
  /**
   * 
   */
  public function payment_status($orderId){     
    if (empty($orderId)){
      die();
    }
    
    if (!$this->Order->exists($orderId)){
      die();
    }
     
    $this->Order->id = $orderId;
    $this->Order->updateAll(array('Order.payment_data' => "'".json_encode($this->request->data)."'"), array('Order.id' => $orderId));
    
     
    App::import('Lib', CLIENT.'.Przelewy24');
    $P24 = new Przelewy24(Configure::read('P24.config'));
    foreach ($this->request->data as $key => $row){
      $P24->addValue($key, $row);
    }
    $response = $P24->trnVerify();
     
    if ($response['error'] == '0'){
      $this->Order->updateAll(array('Order.paid' => 1), array('Order.id' => $orderId));
    }
    
    /* if ($this->Order->save($order)){
      file_put_contents('payment.txt', '1');
    }else{
      file_put_contents('payment.txt', print_r($this->Order->validationErrors, true));
    } */
    $this->render(false);
  }
  
  
  public function view($id) {
    if ($this->Auth->user('id') <= 0) {
      return $this->redirect(array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'login'));
    }
    
    if (!$id) {
      throw new NotFoundException(__('Wybrane zamówienie nie istnieje'));
    }
    
    $this->Order->contain(array('OrderStatus', 'OrderProduct', 'OrderProduct.Product', 'OrderProduct.Product.Unit', 'OrderProduct.Product.ProductsImage', 'Shipping', 'Payment', 'BillingOrderUserAddress', 'ShippingOrderUserAddress'));
    $order = $this->Order->findById($id);
    if (!$order || $order['Order']['user_id'] != $this->Auth->user('id')) {
      throw new NotFoundException(__('Nie odnaleziono wybranego zamówienia'));
    }
    
    $this->set('order', $order);
  }

}
