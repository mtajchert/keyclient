<?php

App::uses('CommonController', CLIENT.'.Controller');

class PagesController extends CommonController {
  
  public $uses = array('KeyAdmin.Page', 'KeyAdmin.Category', 'KeyAdmin.Product');

  /**
   * 
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('home', 'view', 'terms', 'privacy', 'contact');
  }
  
  /**
   * 
   */
  public function home(){
    $page_home_under_slider = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'home_under_slider'
      )
    ));
    
    $page_home_main_categories = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'home_main_categories'
      )
    ));
    
    $main_categories = $this->Category->find('all', array(
      'conditions' => array(
        'Category.parent_id' => 0,
        'Category.status' => 1
      ),
      'order' => 'Category.sort_order',
      'limit' => 3
    ));
    
    //promotions
    $promotionFind = array(
      'conditions' => array(
        'Product.promotion_status' => 1,
        array(
          'OR' => array(
            'Product.promotion_date' => null,
            'Product.promotion_date =' => '0000-00-00 00:00',
            'Product.promotion_date >=' => date('Y-m-d H:i:s')
          )
        ),
        array(
          'OR' => array(
            'Product.promotion_date_end' => null,
            'Product.promotion_date_end =' => '0000-00-00 00:00',
            'Product.promotion_date_end <' => date('Y-m-d H:i:s')
          )
        )
      ),
      'order' => 'Product.sort_order'
    );
    
    $promotionFind['limit'] = 8;
    $this->Product->hasMany['ProductsImage']['limit'] = 1;
    $this->Product->contain('ProductsImage');
    $promotions = $this->Product->find('all', $promotionFind);
    
    $page_home_promotions = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'home_promotions'
      )
    ));
    
    //our hits
    $our_hitsFind = array(
      'conditions' => array(
        'Product.our_hit_status' => 1,
        array(
          'OR' => array(
            'Product.our_hit_date' => null,
            'Product.our_hit_date =' => '0000-00-00 00:00',
            'Product.our_hit_date >=' => date('Y-m-d H:i:s')
          )
        ),
        array(
          'OR' => array(
            'Product.our_hit_date_end' => null,
            'Product.our_hit_date_end =' => '0000-00-00 00:00',
            'Product.our_hit_date_end <' => date('Y-m-d H:i:s')
          )
        )
      )
    );
    
    $our_hitsFind['limit'] = 8;
    $this->Product->hasMany['ProductsImage']['limit'] = 1;
    $this->Product->contain('ProductsImage');
    $our_hits = $this->Product->find('all', $our_hitsFind);
    $page_home_our_hits = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'home_our_hits'
      )
    ));
    
    $page_home_before_footer = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'home_before_footer'
      )
    ));
    
    
    // Slider
    $this->loadModel('KeyAdmin.Slider');
    $sliders = $this->Slider->find('all', array('order' => array('sort' => 'ASC')));
    
    
    //print_r($page_home_before_footer);die;
    
    $this->set(compact('sliders', 'page_home_under_slider', 'page_home_main_categories', 'main_categories', 'promotions', 'page_home_promotions', 'our_hits', 'page_home_our_hits', 'page_home_before_footer'));
    
    $this->set('home_description', 'Sklep motoryzacyjny z prawdziwego zdarzenia dla wszystkich miłośników produktów do różnego typu pojazdów');
    $this->render('Pages/home');
    
    
    //print_r($this->Product->getDataSource()->getLog(false, false));die;
  }
  
  public function view($slugOrIdOrArray){
    $page = null;
    if (is_numeric($slugOrIdOrArray)) {
      $pageId = $slugOrIdOrArray;
    } elseif (is_array($slugOrIdOrArray)) {
      $page = $slugOrIdOrArray;
    } else {
      $pageId = explode('-', $slugOrIdOrArray);
      $pageId = substr(end($pageId), 1);
    }
    
    if (empty($page)) {
      if (!$this->Page->exists($pageId)){
        throw new NotFoundException('Could not find that page');
      }
      $this->Page->id = $pageId;
      $page = $this->Page->read();
    }
    
    $this->set(array(
      'meta_title' => $page['Page']['meta_title_tag'],
      'meta_desc' => $page['Page']['meta_desc_tag'],
      'meta_keywords' => $page['Page']['meta_keywords_tag'],
    ));
    
    $this->set('page', $page);
    $this->render('Pages/view');
  }
  
  public function contact(){
    $slugOrIdOrArray = $this->Page->find('first', array('conditions' => array('Page.code' => 'contact')));
    $page = null;
    if (is_numeric($slugOrIdOrArray)) {
      $pageId = $slugOrIdOrArray;
    } elseif (is_array($slugOrIdOrArray)) {
      $page = $slugOrIdOrArray;
    } else {
      $pageId = explode('-', $slugOrIdOrArray);
      $pageId = substr(end($pageId), 1);
    }
    
    if (empty($page)) {
      if (!$this->Page->exists($pageId)){
        throw new NotFoundException('Could not find that page');
      }
      $this->Page->id = $pageId;
      $page = $this->Page->read();
    }
    
    $this->set(array(
      'meta_title' => $page['Page']['meta_title_tag'],
      'meta_desc' => $page['Page']['meta_desc_tag'],
      'meta_keywords' => $page['Page']['meta_keywords_tag'],
    ));
    
    $this->set('page', $page);
    
  }
  
  public function terms(){
    $page = $this->Page->find('first', array('conditions' => array('Page.code' => 'terms')));
    $this->view($page);
  }
  
  public function privacy(){
    $page = $this->Page->find('first', array('conditions' => array('Page.code' => 'privacy')));
    $this->view($page);
  }

}
