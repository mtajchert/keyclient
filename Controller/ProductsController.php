<?php

App::uses('CommonController', CLIENT.'.Controller');

class ProductsController extends CommonController {
  
  public $uses = array('KeyAdmin.Product');
  
  /**
   *
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('view');
  }
  

  /**
   * 
   */
  public function view($slug){
    $productId = explode('-', $slug);
    $productId = substr(end($productId), 1);
    
    $optionId = isset($this->params->named['option_id']) ? (int) $this->params->named['option_id'] : 0;

    if (!$this->Product->exists($productId)){
      throw new NotFoundException('Could not find that post');
    }
    
    // Get random products
    $options = array(
      'limit' => 3,
      'conditions' => array('Product.status' => 1, 'Product.id !=' => $productId, 'Categories.status' => 1),
      'order' => array('rand()'),
      'joins' => array(
        array(
            'table' => 'products_categories',
            'type' => 'RIGHT',
            'alias' => 'ProductsCategories',
            'conditions' => "Product.id = ProductsCategories.product_id"
        ),
        array(
            'table' => 'categories',
            'type' => 'RIGHT',
            'alias' => 'Categories',
            'conditions' => "ProductsCategories.category_id = Categories.id"
        )
      ),
    );
    $this->loadModel('KeyAdmin.Product');
    $this->Product->contain(array('ProductsImage', 'Manufacturer'));
    $products = $this->Product->find('all', $options);   
    
    $this->set('newProductReview', $this->handleReviewForm($productId));
    
    $this->Product->id = $productId;
    $this->Product->contain(array('ProductsImage', 'ProductsProductsOption', 'ProductsProductsOption.ProductOption', 'ProductsProductsOption.ProductOptionValue', 'Category' => array(
      'order' => 'category_default DESC',
      'limit' => 1
    ), 'ProductReview' => array('order' => 'ProductReview.created DESC', 'limit' => 10)));
    $product = $this->Product->read();
    
    $this->set(array(
      'meta_title' => (!empty($product['Product']['meta_title_tag']))? $product['Product']['meta_title_tag']: $product['Product']['name'],
      'meta_desc' => $product['Product']['meta_desc_tag'],
      'meta_keywords' => $product['Product']['meta_keywords_tag'],
    ));
    
    
    $categoryPath = array();
    if (!empty($product['Category'])) {
      $categoryPath = $this->Product->Category->getNestedCategories($product['Category'][0]['id']);
    }
    
    $product['ProductsImage'] = Hash::sort($product['ProductsImage'], '{n}.sort_order', 'asc');
    //print_r($this->Product->getDatasource()->getLog());die;
    //print_r($categoryPath);die;
    //print_r($product);die;
    
    App::uses('AppHelper', 'View/Helper');
    $appHelper = new AppHelper(new View());
    $this->set('canonical', $appHelper->getProductViewUrl($product));
    
    $this->set('products', $products);
    $this->set('product', $product);
    $this->set('optionId', $optionId);
    $this->set('categoryPath', $categoryPath);
    
    /**
     * 
     */
    $this->loadModel(CLIENT.'.ViewWiper');
    if (empty($product['ProductsProductsOption'])){
      $brands = $this->ViewWiper->find('all', array(
        'conditions' => array('product_id' => $productId),
        'group' => array('brand_name', 'type_name')  
      ));
    }else{
      $brands = $this->ViewWiper->find('all', array(
        'conditions' => array('product_id' => $productId),
        'group' => array('brand_name', 'type_name', 'kind_name')
      ));
    }
    
    $this->set(compact('brands'));
  }
  
  protected function handleReviewForm($productId) {
    if ($this->request->is('post')) {
      $this->Product->ProductReview->create();
      $this->request->data['ProductReview']['product_id'] = $productId;
      
      $this->request->data['ProductReview']['rate'] = number_format($this->request->data['ProductReview']['rate'], 2);
      
      if ($this->Product->ProductReview->save($this->request->data)) {
        $this->Session->setFlash(__('Twoja opinia została zapisana.'), 'flash-success', array(), 'product_review_success');
        
        $reviews = $this->Product->ProductReview->find('all', array(
          'conditions' => array('ProductReview.product_id' => $productId),
          'fields' => array('ProductReview.rate')
        ));
        
        if (count($reviews) > 0) {
          $rate_sum = 0;
          foreach ($reviews as $review) {
            $rate_sum += $review['ProductReview']['rate'];
          }
          $avg_rate = round($rate_sum / count($reviews), 2);
          $this->Product->updateAll(array('Product.average_rate' => $avg_rate, 'review_count' => count($reviews)), array('Product.id' => $productId));
        }
        
      } else {
        $this->Session->setFlash(__('Nie udało się zapisać opini. Sprawdź poprawność podanych danych.'), 'flash-error', array(), 'product_review_error');
        return $this->request->data;
      }
    }
    return null;
  }
  
}