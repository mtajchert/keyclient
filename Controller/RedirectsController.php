<?php

App::uses('CommonController', CLIENT.'.Controller');

class RedirectsController extends CommonController {
  
  public $uses = array('KeyAdmin.Page', 'KeyAdmin.Category', 'KeyAdmin.Product');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow();
  }
  
  public function index() {
    //--
  }
  
  public function redirectUrl($slug, $type) { 
    $url = array();
    if ($type == 'product') {
      $productId = explode('-', $slug);
      $productId = substr(end($productId), 0, -5);
      
      $product = $this->Product->findById($productId);
      if (empty($product)){
        throw new NotFoundException('Produkt nie istnieje');
      }
      
      $seoUrl = (false)? $product['Product']['seo_url']: Inflector::slug($product['Product']['name'], '-');
      $slug = strtolower($seoUrl.'-p'.$product['Product']['id']);
      $url = array('plugin' => CLIENT, 'controller' => 'Products', 'action' => 'view', 'slug' => $slug);
    } elseif ($type == 'category') {
      $categoryId = explode('-', $slug);
      $categoryId = substr(end($categoryId), 0, -5);
      
      $category = $this->Category->findById($categoryId);
      
      $seoUrl = (!empty($category['Category']['seo_url']))? $category['Category']['seo_url']: Inflector::slug($category['Category']['name'], '-');
      $slug = strtolower($seoUrl.'-c'.$category['Category']['id']);
      $url = array('plugin' => CLIENT, 'controller' => 'Categories', 'action' => 'view', 'slug' => $slug);
    } elseif ($type == 'other') {
      $pageCode = '';
      switch ($slug) {
        case 'platnosci-i-dostawa-pm-3.html':
          $pageCode = 'shipping_and_payments';
          break;
        case 'certyfikat-prokonsumencki-pm-21.html':
          $pageCode = 'certificates';
          break;
        case 'reklamacja-towaru-pm-17.html':
          $pageCode = 'complaint';
          break;
        case 'odstapienie-od-umowy-pm-18.html':
          $pageCode = 'termination';
          break;
        
        case 'regulamin-pm-5.html':
          $url = array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'terms');
          break;
        case 'polityka-prywatnosci-pm-19.html':
          $url = array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'privacy');
          break;
        case 'kontakt-f-1.html':
        case 'dane-firmy-pm-11.html':
        case 'jak-dojechac-pm-10.html':
          $url = array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'contact');
          break;
        case 'logowanie.html':
          $url = array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'login');
          break;
        case 'rejestracja.html':
          $url = array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'register');
          break;
      }
      
      if (empty($url) && !empty($pageCode)) {
        $page = $this->Page->find('first', array('conditions' => array('Page.code' => $pageCode)));
        $slug = $page['Page']['seo_url'].'-s'.$page['Page']['id'];
        $url = array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'view', 'slug' => $slug);
      }
    }
    
    if (empty($url)) {
      $url = array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home');
    }
    
    return $this->redirect($url, 301);
  }
}
