<?php

App::uses('CommonController', CLIENT.'.Controller');

class SearchController extends CommonController {

  public $uses = array('KeyAdmin.Product');
  public $components = array('Paginator', 'RequestHandler');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow();
  }

  public function index() {
    $query = $this->params->named['q'];
    //var_dump($query);die;
    list($products, $sort) = $this->getSearchedProducts($query);
    $this->set(compact('sort', 'products', 'query'));
  }

  public function get_search_results() {
    $query = $this->params->query['q'];
    list($products, $sort) = $this->getSearchedProducts($query);
    
    //print_r($products);die;
    $view = new View($this, false);
    $view->viewPath = 'Search';
    $view->layout = false;
    $view->set(compact('sort', 'products', 'query'));
    $html = $view->render('results');

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'result' => array(
        'success' => true,
        'results' => $html
      )
    ));
    $this->set('_serialize', array('result'));
  }
  
  protected function getSearchedProducts($query) {
    $sort = 'al_default';
    $sortAvailable = array('al_default', 'al_price', 'al_price-desc');
    if (isset($this->request->params['named']['sort'])) {
      $sort = $this->request->params['named']['sort'];
    } else {
      $sort = CakeSession::read('CategoryProducts.sort');
    }
    if (!in_array($sort, array('al_default', 'al_price', 'al_price-desc'))) {
      $sort = 'al_default';
    }
    CakeSession::write('CategoryProducts.sort', $sort);
    $this->Paginator->settings['sort'] = $sort;
    
    if (!empty($query)) {
      $this->Paginator->settings['conditions'] = array(
        'OR' => array(
          'Product.name LIKE' => '%'.$query.'%',
          'Product.description LIKE' => '%'.$query.'%'
        )
      );
      
      $this->Paginator->settings['contain'] = array('ProductsImage');
      $this->Paginator->settings['limit'] = 16;
      $products = $this->Paginator->paginate('Product');

      //$dbo = $this->Product->getDatasource(); print_r($dbo->getLog());die;
    }
    
    return array($products, $sort);
  }

}
