<?php

App::uses('CommonController', CLIENT.'.Controller');

class SitemapController extends CommonController {

  public $uses = array('KeyAdmin.Category', 'KeyAdmin.Product');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow();
  }

  public function index() {
    $categories = $this->Category->find('all', array(
      'conditions' => array(
        'Category.status' => '1'
      ),
      'fields' => array(
        'id', 'seo_url', 'name', 'image'
      )
    ));

    $products = $this->Product->find('all', array(
      'conditions' => array(
        'Product.status' => '1',
      ),
      'fields' => array(
        'Product.id', 'Product.seo_url', 'Product.name', 'ProductsImage.image', 'ProductsImage.description'
      ),
      'joins' => array(
        array(
          'table' => 'products_images',
          'alias' => 'ProductsImage',
          'type' => 'LEFT',
          'conditions' => array(
            'ProductsImage.id = Product.id'
          )
        )
      ),
      'order' => 'Product.id ASC, ProductsImage.sort_order ASC'
    ));

    //$dbo = $this->Product->getDatasource(); print_r($dbo->getLog()); die;
    //print_r($products); die;

    $view = new View($this, false);

    $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\r\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

    $base_url = Router::url('/', true);

    foreach ($categories as $category) {
      $item_xml = "\r\n\t<url>";
      $item_xml .= "\r\n\t\t<loc>".$view->App->getCategoryViewUrl($category).'</loc>';

      if (!empty($category['Category']['image'])) {
        $img_url = $base_url.CLIENT.'/categories/origin/'.$category['Category']['image'];
        $item_xml .= "\r\n\t\t<image:image>\r\n\t\t\t<image:loc>".$img_url."</image:loc>\r\n\t\t\t<image:title>".htmlspecialchars($category['Category']['name'])."</image:title>\r\n\t\t</image:image>";
      }

      $item_xml .= "\r\n\t</url>";
      $xml .= $item_xml;
    }
    
    $products_done = array();
    foreach ($products as $product) {
      if (array_key_exists($product['Product']['id'], $products_done)) {
        continue;
      }
      $products_done[$product['Product']['id']] = true;
          
      $item_xml = "\r\n\t<url>";
      $item_xml .= "\r\n\t\t<loc>".$view->App->getProductViewUrl($product).'</loc>';

      if (isset($product['ProductsImage']['image']) && !empty($product['ProductsImage']['image'])) {
        $img_url = $base_url.CLIENT.'/products/origin/'.$product['ProductsImage']['image'];
        $title = empty($product['Product']['description']) ? $product['Product']['name'] : $product['Product']['description'];
        $item_xml .= "\r\n\t\t<image:image>\r\n\t\t\t<image:loc>".$img_url."</image:loc>\r\n\t\t\t<image:title>".htmlspecialchars($title)."</image:title>\r\n\t\t</image:image>";
      }

      $item_xml .= "\r\n\t</url>";
      $xml .= $item_xml;
    }

    $xml .= "\r\n</urlset>";

    $this->response->body($xml);
    $this->response->type('xml');
    return $this->response;
  }

}
