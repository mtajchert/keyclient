<?php

App::uses('CommonController', CLIENT.'.Controller');

class TuningController extends CommonController {

  /**
   *
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('index');
  }
  
  /**
   * 
   */
  public function index(){
    
    $this->render('Category/index');
  }
}
