<?php

App::uses('CommonController', CLIENT.'.Controller');

class UserController extends CommonController {
  
  public $uses = array('KeyAdmin.Page', 'KeyAdmin.Order', 'KeyAdmin.Customer');
  public $components = array('RequestHandler');
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('get_zones');
  }
  
  /**
   * 
   */
  public function account() {
    $pages = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'account'
      )
    ));
    
    $this->set('pages', $pages);
    $this->render('User/account');
  }
  
  public function orders() {
    $this->Order->contain(array('OrderProduct', 'Payment', 'Shipping', 'OrderStatus'));
    $orders = $this->Order->find('all', array(
      'conditions' => array(
        'Order.user_id' => $this->Auth->user('id')
      ),
      'order' => 'Order.created desc'
    ));
    
    $pages = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'orders'
      )
    ));
    
    $this->set('orders', $orders);
    $this->set('pages', $pages);
    $this->render('User/orders');
  }
  
  public function addresses() {
    $this->Customer->UserAddress->contain(array('Country', 'Zone'));
    $addresses = $this->Customer->UserAddress->find('all', array(
      'conditions' => array(
        'UserAddress.user_id' => $this->Auth->user('id')
      ),
      'order' => 'UserAddress.default_billing desc, UserAddress.default_shipping desc'
    ));
    
    $pages = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'addresses'
      )
    ));
    //print_r($addresses);die;
    $this->set('addresses', $addresses);
    $this->set('pages', $pages);
    $this->render('User/addresses');
  }
  
  public function create_address() {
    if ($this->request->is('post')) {
      $this->Customer->UserAddress->create();
      $this->request->data['UserAddress']['user_id'] = $this->Auth->user('id');
      $this->request->data['UserAddress']['alias'] = __('Adres dodatkowy');
      
      if ($this->Customer->UserAddress->save($this->request->data)) {
        $this->Session->setFlash(__('Adres został dodany.'), 'flash-success');
        return $this->redirect(array('action' => 'addresses'));
      }
      $this->Session->setFlash(__('Nie udało się dodać adresu. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->request->data = array('UserAddress' => array('is_company' => 0));
    }
    
    $pages = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'add_address'
      )
    ));
    
    $this->set('pages', $pages);
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    
    $defaultCountry = $this->Customer->UserAddress->Country->find('first', array(
      'conditions' => array(
        'Country.default' => 1
      )
    ));
    $this->set('defaultCountryId', $defaultCountry['Country']['id']);
    
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => isset($this->request->data['UserAddress']['country_id']) ? $this->request->data['UserAddress']['country_id'] : $defaultCountry['Country']['id']
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    $this->set(compact('countries', 'zones'));
    
    $this->render('User/address_form');
  }
  
  public function get_zones() {
    $country_id = (int) $this->request->query['country_id'];

    $zonesArr = array();

    $zones = $this->Customer->UserAddress->Zone->find('all', array(
      'conditions' => array(
        'Zone.country_id' => $country_id
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    foreach ($zones as $zone) {
      $zonesArr[] = array('id' => $zone['Zone']['id'], 'name' => $zone['Zone']['name']);
    }

    $result = array(
      'success' => 1,
      'zones' => $zonesArr
    );

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }
  
  public function delete_address($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany adres nie istnieje'));
    }
    $address = $this->Customer->UserAddress->findById($id);
    if (!$address) {
      throw new NotFoundException(__('Nie odnaleziono wybranego adresu'));
    }
    if ($address['UserAddress']['default_billing']) {
      $this->Session->setFlash(__('Nie można usunąć domyślnego adresu rozliczeniowego.'), 'flash-error');
      return $this->redirect(array('action' => 'addresses'));
    }
    if ($address['UserAddress']['default_shipping']) {
      $this->Session->setFlash(__('Nie można usunąć domyślnego adresu dostawy.'), 'flash-error');
      return $this->redirect(array('action' => 'addresses'));
    }

    if ($this->Customer->UserAddress->delete($id, true)) {
      $this->Session->setFlash(__('Adres został usunięty.'), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć adresu.'), 'flash-error');
    }
    
    return $this->redirect(array('action' => 'addresses'));
  }
  
  public function edit_address($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany adres nie istnieje'));
    }
    $address = $this->Customer->UserAddress->findById($id);
    if (!$address || $address['UserAddress']['user_id'] != $this->Auth->user('id')) {
      throw new NotFoundException(__('Nie odnaleziono wybranego adresu'));
    }
    //print_r($this->request->data);die;
    if ($this->request->is(array('post', 'put'))) {
      $this->request->data['UserAddress']['id'] = $id;
      $this->request->data['UserAddress']['user_id'] = $this->Auth->user('id');
      $this->request->data['UserAddress']['alias'] = $address['UserAddress']['alias'];
      if (empty($this->request->data['UserAddress']['alias'])) {
        $this->request->data['UserAddress']['alias'] = __('Adres dodatkowy');
      }
      
      if ($this->Customer->UserAddress->save($this->request->data)) {
        $this->Session->setFlash(__('Adres został zapisany.'), 'flash-success');
        return $this->redirect(array('action' => 'addresses'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać adresu. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->request->data = $address;
    }
    
    $pages = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'edit_address'
      )
    ));
    
    $this->set('pages', $pages);
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    
    $this->set('defaultCountryId', 0);
    
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => $this->request->data['UserAddress']['country_id']
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    $this->set(compact('countries', 'zones'));
    
    $this->render('User/address_form');
  }
  
  public function settings() {
    $id = $this->Auth->user('id');
    $customer = $this->Customer->findById($id);
    if (!$customer) {
      throw new NotFoundException(__('Nie odnaleziono danych konta'));
    }
    
    if ($this->request->is(array('post', 'put'))) {
      $this->Customer->id = $this->request->data['Customer']['id'] = $id;
      if ($this->Customer->save($this->request->data, array('noAddress' => true))) {
        $this->Session->setFlash(__('Dane konta zostały zapisane.'), 'flash-success');
        $this->request->data['Customer']['password'] = $this->request->data['Customer']['password2'] = '';
        return;
      }
      $this->Session->setFlash(__('Nie udało się zapisać danych konta. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->request->data = $customer;
    }
    
    $this->request->data['Customer']['password'] = $this->request->data['Customer']['password2'] = '';
    
    $pages = $this->Page->find('all', array(
      'conditions' => array(
        'Page.group' => 'account_settings'
      )
    ));
    
    $this->set('pages', $pages);
    
    $this->render('User/settings');
  }

}
