<?php

App::uses('AppModel', 'Model');

/**
 * ChangePassword Model
 *
 */
class ChangePassword extends AppModel {

  public $useTable = false;
  protected $_schema = array(
    'email' => array(
      'type' => 'string'
    )
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'user_id' => array(
      'int_gt_0' => array(
        'rule' => array('naturalNumber', false),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nie wybrano klienta'
      ),
    ),
    'password' => array(
      'minLength' => array(
        'rule' => array('minLength', '3'),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wpisz hasło o długości min. 3 znaków'
      ),
    ),
    'password2' => array(
      'same' => array(
        'rule' => 'validatePasswordConfirm',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Hasła różnią się od siebie'
      )
    )
  );
  
  public function validatePasswordConfirm($value) {
    if (empty($this->data['ChangePassword']['password']) || strcmp($value['password2'], $this->data['ChangePassword']['password']) == 0) {
      return true;
    }
    
    $this->data['ChangePassword']['password'] = $this->data['ChangePassword']['password2'] = '';
    
    return false;
  }
  
  public function save($data = null, $validate = true, $fieldList = array()) {
    $this->set($data);
    if ($validate) {
      if (!$this->validates()) {
        return false;
      }
    }
    
    $this->Customer = ClassRegistry::init('KeyAdmin.Customer');
    $pass = $this->Customer->hashPassword($this->data['ChangePassword']['password']);
    $this->Customer->updateAll(array(
      'Customer.password' => "'".$pass."'",
      'Customer.hash' => "'".md5(uniqid(mt_rand(), true).time())."'",
      ), array('Customer.id' => $this->data['ChangePassword']['user_id']));
    
    return true;
  }
  
}
