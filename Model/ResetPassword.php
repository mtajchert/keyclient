<?php

App::uses('AppModel', 'Model');

/**
 * ResetPassword Model
 *
 */
class ResetPassword extends AppModel {

  public $useTable = false;
  protected $_schema = array(
    'email' => array(
      'type' => 'string'
    )
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'email' => array(
      'format' => array(
        'rule' => 'email',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Podany e-mail jest nieprawidłowy'
      ),
      'exists' => array(
        'rule' => 'validateEmail',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Podany e-mail nie jest zarejestrowany w sklepie'
      )
    ),
  );
  
  public function validateEmail($email) {
    $this->Customer = ClassRegistry::init('KeyAdmin.Customer');
    $this->Customer->contain('UserData');
    $customer = $this->Customer->find('first', array(
      'conditions' => array(
        'Customer.email' => $email,
        'UserData.status' => 1,
        'Customer.guest_account' => 0
      )
    ));
    
    if (!empty($customer) && $customer['Customer']['id'] > 0) {
      return true;
    }
    
    return false;
  }
  
  public function save($data = null, $validate = true, $fieldList = array()) {
    $this->set($data);
    if ($validate) {
      if (!$this->validates()) {
        return false;
      }
    }
    return true;
  }
  
}
