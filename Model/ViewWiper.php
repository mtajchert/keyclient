<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 */
class ViewWiper extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
}
