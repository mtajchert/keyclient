<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 */
class WipersCategory extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  
  public $belongsTo = array(
    'ParentCategory' => array(
      'className' => CLIENT.'.WipersCategory',
      'foreignKey' => 'parent_id'
    )
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'parent_id' => array(
      'rule1' => array(
        'rule' => array('numeric'),
        'allowEmpty' => true,
        'message' => 'Niepoprawne id kategorii nadrzędnej'
      ),
    ),
    'sort_order' => array(
      'rule1' => array(
        'rule' => array('range', -1),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0'
      ),
    ),
    'status' => array(
      'rule1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => false,
        'message' => 'Niepoprawny status'
      ),
    ),
    'color' => array(
      'rule1' => array(
        'rule' => array('custom', '/^#[0-9a-fA-F]{3,6}$/'),
        'allowEmpty' => true,
        'message' => 'Kolor musi być w formacie szesnastkowym lub pole musi być puste'
      ),
    ),
    'color_status' => array(
      'rule1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => true
      ),
    ),
    'name' => array(
      'rule1' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa kategorii jest wymagana'
      )
    )
  );
  
  
  /**
   * 
   */
  public function beforeSave($options = array()){  
    if (empty($this->data['WipersCategory']['seo_url']) && !empty($this->data['WipersCategory']['name'])) {
      $this->data['WipersCategory']['seo_url'] = Inflector::slug($this->data['WipersCategory']['name'], '-');
    }
    
    if (is_array($this->data['WipersCategory']['image']) && $this->data['WipersCategory']['image']['error'] != UPLOAD_ERR_NO_FILE){
      if (empty($this->data['WipersCategory']['image']['size'])){
        $this->data['WipersCategory']['image'] = null;
        return true;
      }
      if (!is_uploaded_file($this->data['WipersCategory']['image']['tmp_name']) || $this->data['WipersCategory']['image']['error'] != UPLOAD_ERR_OK){
        return false;
      }
      
      App::import('Component','KeyAdmin.Image');
      $oImage = new ImageComponent(new ComponentCollection());
      
      $categoriesDir = CLIENT_WWW.'categories/origin/';
      if (!is_dir($categoriesDir)){
        mkdir($categoriesDir, 0777, true);
      }
      $categoriesDirList = CLIENT_WWW.'categories/list/';
      if (!is_dir($categoriesDirList)){
        mkdir($categoriesDirList, 0777, true);
      }
      $categoriesDirThumb = CLIENT_WWW.'categories/thumb/';
      if (!is_dir($categoriesDirThumb)){
        mkdir($categoriesDirThumb, 0777, true);
      }
      $fileName = md5(time().$this->data['WipersCategory']['image']['name']).'.jpg';
      if (move_uploaded_file($this->data['WipersCategory']['image']['tmp_name'], $categoriesDir.$fileName)){
        $oImage->resizeImage($categoriesDir.$fileName, $categoriesDirList.$fileName, 50, 34);
        $oImage->resizeImage($categoriesDir.$fileName, $categoriesDirThumb.$fileName, 480, 320);
        $this->data['WipersCategory']['image'] = $fileName;      
      }else{
        return false;
      }  
    } else {
      unset($this->data['WipersCategory']['image']);   
    }
    
    if (isset($this->data['WipersCategory']['banner']) && is_array($this->data['WipersCategory']['banner']) && $this->data['WipersCategory']['banner']['error'] != UPLOAD_ERR_NO_FILE){
      if (empty($this->data['WipersCategory']['banner']['size'])){
        $this->data['WipersCategory']['banner'] = null;
        return true;
      }
      if (!is_uploaded_file($this->data['WipersCategory']['banner']['tmp_name']) || $this->data['WipersCategory']['banner']['error'] != UPLOAD_ERR_OK){
        return false;
      }
      
      App::import('Component','KeyAdmin.Image');
      $oImage = new ImageComponent(new ComponentCollection());
      
      $categoriesDir = CLIENT_WWW.'categories-banners/origin/';
      if (!is_dir($categoriesDir)){
        mkdir($categoriesDir, 0777, true);
      }
      $categoriesDirShow = CLIENT_WWW.'categories-banners/show/';
      if (!is_dir($categoriesDirShow)){
        mkdir($categoriesDirShow, 0777, true);
      }
      $fileName = md5(time().$this->data['WipersCategory']['banner']['name']).'.jpg';
      if (move_uploaded_file($this->data['WipersCategory']['banner']['tmp_name'], $categoriesDir.$fileName)){
        $oImage->resizeImage($categoriesDir.$fileName, $categoriesDirShow.$fileName, 850, 0);
        $this->data['WipersCategory']['banner'] = $fileName;      
      }else{
        return false;
      }  
    } else {
      if (isset($this->data['WipersCategory']['banner_'])){
        $this->data['WipersCategory']['banner'] = $this->data['WipersCategory']['banner_'];        
      }      
    }
        
    return true;
  }
  
  public function getNestedId($categoryId) {
    $this->recursive = 100;
    $category = $this->findById($categoryId);
    $category['WipersCategory']['ParentCategory'] = isset($category['ParentCategory']) ? $category['ParentCategory'] : null;
    $category = $category['WipersCategory'];
    
    $nid = '';
    while (!empty($category) && isset($category['id']) && $category['id'] > 0) {
      $nid = $category['id'].'_'.$nid;
      $category = isset($category['ParentCategory']) ? $category['ParentCategory'] : null;
    }
    
    return trim($nid, '_');
  }

  public function getNestedName($categoryId) {
    $this->recursive = 100;
    $category = $this->findById($categoryId);
    $category[$this->alias]['ParentCategory'] = isset($category['ParentCategory']) ? $category['ParentCategory'] : null;
    $category = $category[$this->alias];
    
    $nname = '';
    while (!empty($category) && isset($category['id']) && $category['id'] > 0) {
      $nname = $category['name'].' \ '.$nname;
      $category = isset($category['ParentCategory']) ? $category['ParentCategory'] : null;
    }
    
    return trim($nname, '\ ');
  }
  
  public function getNestedCategories($categoryId) {
    $this->recursive = 100;
    $category = $this->findById($categoryId);
    $category[$this->alias]['ParentCategory'] = isset($category['ParentCategory']) ? $category['ParentCategory'] : null;
    $category = $category[$this->alias];
    
    $categories = array();
    while (!empty($category) && isset($category['id']) && $category['id'] > 0) {
      $categories[] = array('WipersCategory' => $category);
      $category = isset($category['ParentCategory']) ? $category['ParentCategory'] : null;
    }
    
    return array_reverse($categories);
  }
  
  public function beforeValidate($options = array()) {
    parent::beforeValidate($options);
    if (isset($this->data['WipersCategory']['banner_redirect_type']) && $this->data['WipersCategory']['banner_redirect_type'] == 'product') {
      $this->validator()->add('banner_product_id', array(
        'choose' => array(
          'rule' => array('naturalNumber', false),
          'allowEmpty' => false,
          'required' => true,
          'message' => 'Wybierz produkt'
        ),
      ));
    } elseif (isset($this->data['WipersCategory']['banner_redirect_type']) && $this->data['WipersCategory']['banner_redirect_type'] == 'url') {
      $this->validator()->add('banner_url', array(
        'noEmpty' => array(
          'rule' => array('minLength', '1'),
          'allowEmpty' => false,
          'required' => true,
          'message' => 'Wpisz adres url'
        ),
      ));
    }
    
    return true;
  }

}
