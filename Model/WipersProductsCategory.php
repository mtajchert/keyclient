<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 */
class WipersProductsCategory extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  public $belongsTo = array(
    'Product' => array(
      'className' => 'KeyAdmin.Product',
      'foreignKey' => 'product_id'
    ),
    'WipersCategory' => array(
      'className' => CLIENT.'.WipersCategory',
      'foreignKey' => 'category_id'
    ),
    'ProductsProductsOption' => array(
      'className' => 'KeyAdmin.ProductsProductsOption',
      'foreignKey' => 'products_products_option_id'
    )
  );

  public function beforeFind($queryData) {
    if (isset($queryData['sort']) && !empty($queryData['sort'])) {
      switch ($queryData['sort']) {
        case 'al_default':
          $queryData['order'] = 'Product.sort_order ASC, Product.id ASC';
          break;
        case 'al_price':
          $queryData['order'] = 'Product.price_tax ASC, Product.sort_order ASC, Product.id ASC';
          break;
        case 'al_price-desc':
          $queryData['order'] = 'Product.price_tax DESC, Product.sort_order ASC, Product.id ASC';
          break;
      }
    }

    return $queryData;
  }

}
