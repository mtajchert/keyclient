<div class="container main-container headerOffset">
  
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Moje konto'), array('controller' => 'User', 'action' => 'account')),
    array(__('Resetowanie hasła'), $this->here)
  ))); ?>

  <?php echo $this->Session->flash(); ?>


  <div class="row">

    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner"><span><i class="fa fa-map-marker"></i>
          <?php if (isset($pages[0])): ?>
            <?php echo h($pages[0]['Page']['title']); ?>
          <?php else: ?>
            <?php echo __('Ustawienia konta'); ?>
          <?php endif; ?>
        </span></h1>

      <div class="row userInfo">
        <div class="col-lg-12">
          <?php if (isset($pages[0])): ?>
            <?php foreach ($pages as $key => $page): ?>
              <?php if ($key > 0): ?>
                <h2 class="block-title-2"><span><?php echo h($page['Page']['title']); ?></span></h2>
              <?php endif; ?>
              <?php echo $page['Page']['content']; ?>
            <?php endforeach; ?>
          <?php endif; ?>
          <br/>
        </div>
      </div>

      <div class="row">
        <div class="w100 clearfix">
          <?php if ($step == 1): ?>
            <?php echo $this->Form->create('ResetPassword'); ?>
            <div class="col-xs-12 col-sm-12">
              <div class="form-group required <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('email', array('label' => __('E-mail').': <sup>*</sup>', 'placeholder' => __('Wpisz swój adres e-mail'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-unlock"> </i> <?php echo __('Dalej'); ?></button>
            </div>
            <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
          <?php elseif ($step == 2): ?>
            <?php echo $this->Form->create('ChangePassword'); ?>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group required <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password', array('label' => __('Nowe hasło').': <sup>*</sup>', 'placeholder' => __('Wpisz nowe hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
              <div class="form-group required <?php echo $this->Form->isFieldError('password2') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password2', array('type' => 'password', 'label' => __('Powtórz nowe hasło').': <sup>*</sup>', 'placeholder' => __('Wpisz powtórnie nowe hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-unlock"> </i> <?php echo __('Zmień hasło'); ?></button>
            </div>
            <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 clearfix">
          <ul class="pager">
            <li class="previous pull-right">
              <?php echo $this->Html->link('&larr; '.__('Powrót do logowania'), '#', array('title' => __('Powrót do logowania'), 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#ModalLogin')); ?>
            </li>
          </ul>
        </div>
      </div>
      <!--/row end-->
    </div>

  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->