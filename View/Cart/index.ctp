<div class="container main-container headerOffset">
  
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Koszyk'), $this->here)
  ))); ?>

  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner">
        <span><i class="glyphicon glyphicon-shopping-cart"></i> <?php echo __('Koszyk'); ?> </span></h1>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 rightSidebar">
      <h4 class="caps">
        <?php echo $this->Html->link('<i class="fa fa-chevron-left"></i> '.__('Wróć do zakupów'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Wróć do zakupów'), 'escape' => false)); ?>
      </h4>
    </div>
  </div>
  <!--/.row-->

  <?php if (empty($cart['CartProduct'])): ?>
    <div class="row">
      <div class="col-xs-12 text-center empty-cart-msg">
        Koszyk jest pusty
      </div>
    </div>
  <?php else: ?>
    <div class="row">
      <div class="col-lg-8 col-md-9 col-sm-7">
        <div class="row userInfo">
          <div class="col-xs-12 col-sm-12">
            <div class="cartContent w100">
              <table class="cartTable table-responsive" style="width:100%">
                <tbody>

                  <tr class="CartProduct cartTableHeader">
                    <td style="width:15%"> <?php echo __('Produkt'); ?></td>
                    <td style="width:40%"><?php echo __('Szczegóły'); ?></td>
                    <td style="width:10%" class="delete">&nbsp;</td>
                    <td style="width:10%"> <?php echo __('Ilość'); ?></td>
                    <td style="width:10%"> <?php echo __('Rabat'); ?></td>
                    <td style="width:15%"> <?php echo __('Cena razem'); ?></td>
                  </tr>

                  <?php foreach ($cart['CartProduct'] as $cartProduct): ?>
                    <tr class="CartProduct">
                      <td class="CartProductThumb">
                        <div>
                          <a href="<?php echo $this->App->getProductViewUrl($cartProduct); ?>">
                            <?php if (isset($cartProduct['Product']['ProductsImage'][0]['image']) && !empty($cartProduct['Product']['ProductsImage'][0]['image'])): ?>
                              <?php echo $this->App->showProductsImage($cartProduct['Product']['ProductsImage'][0]['image'], 'LIST', array('class' => 'img-responsive', 'alt' => h($cartProduct['name']))); ?>
                            <?php endif; ?>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div class="CartDescription">
                          <h4>
                            <a href="<?php echo $this->App->getProductViewUrl($cartProduct); ?>">
                              <?php echo h($cartProduct['name']); ?>
                            </a>
                          </h4>
                          <?php /* <span class="size"> <?php echo h($cartProduct['add_info']); ?> </span>*/ ?>

                          <div class="price"><span> <?php echo h($cartProduct['price_tax']).' '.__('zł'); ?> </span></div>
                        </div>
                      </td>
                      <td class="delete">
                        <a title="<?php echo __('Usuń'); ?>" onclick="App.Cart.deleteCartProduct(<?php echo h($cartProduct['id']); ?>, 'cart');"> <i class="glyphicon glyphicon-trash fa-2x"></i></a>
                      </td>
                      <td>
                        <input style="width: 40px;" class="quanitySniper" type="text" value="<?php echo round($cartProduct['amount']); ?>" name="data[CartProduct][<?php echo h($cartProduct['id']); ?>][amount]">
                      </td>
                      <td>0</td>
                      <td class="price"> <?php echo h($cartProduct['total_price_tax']).' '.__('zł'); ?> </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!--cartContent-->

            <div class="cartFooter w100">
              <div class="box-footer">
                <div class="pull-left">
                  <?php echo $this->Html->link('<i class="fa fa-chevron-left"></i> '.__('Wróć do zakupów'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Wróć do zakupów'), 'escape' => false, 'class' => 'btn btn-default')); ?>
                </div>
                <div class="pull-right">
                  <button type="submit" class="btn btn-default" onclick="App.Cart.saveAmountsAndRefresh();">
                    <i class="fa fa-undo"></i> <?php echo __('Odśwież koszyk'); ?>
                  </button>
                </div>
              </div>
            </div>
            <!--/ cartFooter -->

          </div>
        </div>
        <!--/row end-->

      </div>
      <div class="col-lg-4 col-md-3 col-sm-5 rightSidebar">
        <div class="contentBox">
          <div class="w100 costDetails">
            <div class="table-block" id="order-detail-content">             

              <div class="w100 cartMiniTable">
                <table id="cart-summary" class="std table">
                  <tbody>
                    <tr>
                      <td><?php echo __('Produkty'); ?></td>
                      <td class="price"> <?php echo h($cart['Cart']['products_price_tax']).' '.__('zł'); ?> </td>
                    </tr>
                  	<?php if (empty($deliveryPrices)): ?>
                    	<tr style="">                    	
                    		<td><?php echo __('Dostawa'); ?></td>
                      	<td class="price"><span class="success">GRATIS!</span></td>
                    	</tr>
                  	<?php endif; ?>
                    <tr>
                      <td> <?php echo __('Razem'); ?></td>
                      <td class=" site-color" id="total-price"> <?php echo h($cart['Cart']['order_price_tax']).' '.__('zł'); ?> </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="input-append couponForm">
                          <input class="col-lg-8" id="appendedInputButton" type="text" placeholder="<?php echo __('Kupon rabatowy'); ?>">
                          <button class="col-lg-4 btn btn-primary" type="button" onclick="App.Cart.setDiscountCode(this);"><?php echo __('Ok'); ?></button>
                        </div>
                      </td>
                    </tr>
                    
                  	<?php if (!empty($deliveryPrices)): ?>
                  		<tr>
                  			<td><?php echo __('Opcje dostawy'); ?></td>
                  			<td>&nbsp;</td>
                			</tr>
                			<?php foreach ($shippingsFull as $shipping): ?>                				
                				<tr>
                    			<td style="border: none;"><?php echo __($shipping['Shipping']['name']); ?></td>
                    			<td class="price" style="border: none;"><?php echo h($shipping['Shipping']['for_order_price_tax']).' '.__('zł'); ?></td>
                  			</tr>                				
                			<?php endforeach; ?>
              			<?php endif; ?>
                  </tbody>
                  <tbody>
                  </tbody>
                </table>
              </div>
              
              <?php if(isset($authUser) && $authUser): ?>
                <?php echo $this->Html->link(__('Do kasy').' <i class="fa fa-arrow-right"></i>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('Do kasy'), 'escape' => false, 'class' => 'btn btn-primary btn-lg btn-block', 'style' => 'margin-bottom:20px')); ?>
              <?php else: ?>
                <?php echo $this->Html->link(__('Do kasy').' <i class="fa fa-arrow-right"></i>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'), array('title' => __('Do kasy'), 'escape' => false, 'class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-bottom:20px')); ?>
              <?php endif; ?>
              
            </div>
          </div>
        </div>
        <!-- End popular -->

      </div>
      <!--/rightSidebar-->

    </div>
    <!--/row-->
  <?php endif; ?>

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->