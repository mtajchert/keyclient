<?php if (empty($cart['CartProduct'])): ?>
  <div class="miniCartFooter  <?php echo $mobile ? 'miniCartFooterInMobile' : ''; ?> text-right">
    <p class="text-center"><?php echo __('Koszyk jest pusty'); ?></p>
  </div>
  <!--/.miniCartFooter--> 
<?php else: ?>
  <div class="w100 miniCartTable scroll-pane">
    <table>
      <tbody>
        <?php foreach ($cart['CartProduct'] as $cartProduct): ?>
          <tr class="miniCartProduct">
            <td style="width:20%" class="miniCartProductThumb">
              <div>
                <a href="<?php echo $this->App->getProductViewUrl($cartProduct); ?>">
                  <?php if (isset($cartProduct['Product']['ProductsImage'][0]['image']) && !empty($cartProduct['Product']['ProductsImage'][0]['image'])): ?>
                    <?php echo $this->App->showProductsImage($cartProduct['Product']['ProductsImage'][0]['image'], 'LIST', array('class' => 'img-responsive', 'alt' => h($cartProduct['name']))); ?>
                  <?php endif; ?>
                </a>
              </div>
            </td>
            <td style="width:40%">
              <div class="miniCartDescription">
                <h4>
                  <a href="<?php echo $this->App->getProductViewUrl(array('Product' => $cartProduct['Product'])); ?>">
                    <?php echo h($cartProduct['name']); ?>
                  </a>
                </h4>
                <span class="size"> <?php echo h($cartProduct['add_info']); ?> </span>
                <div class="price"><span> <?php echo h($cartProduct['price_tax']).' '.__('zł'); ?> </span></div>
              </div>
            </td>
            <td style="width:10%" class="miniCartQuantity">
              <a> X <?php echo h(round($cartProduct['amount'], 2)); ?> </a>
            </td>
            <td style="width:18%" class="miniCartSubtotal">
              <span> <?php echo h($cartProduct['total_price_tax']).' '.__('zł'); ?> </span>
            </td>
            <td style="width:1%" class="delete">
              <a onclick="App.Cart.deleteCartProduct(<?php echo h($cartProduct['id']); ?>);"> x </a>
            </td>
          </tr>
        <?php endforeach; ?>
        
      </tbody>
    </table>
  </div>
  <!--/.miniCartTable-->

  <div class="miniCartFooter  <?php echo $mobile ? 'miniCartFooterInMobile' : ''; ?> text-right">
    <h3 class="text-right subtotal"> <?php echo __('Razem:').' '.h($cart['Cart']['order_price_tax']).' '.__('zł'); ?> </h3>
    <?php echo $this->Html->link('<i class="fa fa-shopping-cart"> </i> '.__('POKAŻ KOSZYK'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index'), array('title' => __('Pokaż koszyk'), 'escape' => false, 'class' => 'btn btn-sm btn-danger', 'onclick' => 'window.location.href=$(this).attr(\'href\')')); ?>
    <?php if(isset($authUser) && $authUser): ?>
      <?php echo $this->Html->link(__('KUPUJĘ'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('Kupuję'), 'escape' => false, 'class' => 'btn btn-sm btn-primary', 'onclick' => 'window.location.href=$(this).attr(\'href\')')); ?>
    <?php else: ?>
      <?php echo $this->Html->link(__('KUPUJĘ'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'), array('title' => __('Kupuję'), 'escape' => false, 'class' => 'btn btn-sm btn-primary', 'onclick' => 'window.location.href=$(this).attr(\'href\')')); ?>
    <?php endif; ?>
    
  </div>
  <!--/.miniCartFooter-->
<?php endif; ?>