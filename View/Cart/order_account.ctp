<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Koszyk'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index')),
    array(__('Zamówienie'), $this->here)
  ))); ?>

  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner">
        <span><i class="glyphicon glyphicon-shopping-cart"></i> <?php echo __('Zamówienie'); ?> </span></h1>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 rightSidebar">
      <h4 class="caps">
        <?php echo $this->Html->link('<i class="fa fa-chevron-left"></i> '.__('Wróć do zakupów'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Wróć do zakupów'), 'escape' => false)); ?>
      </h4>
    </div>
  </div>
  <!--/.row-->
  
  <div class="row">
    <div class="col-xs-12">
      <div class="w100 clearfix">
        <ul class="orderStep orderStepLook2">
          <li class="active">
            <?php echo $this->Html->link('<i class="fa fa-envelope"> </i> <span>'.__('Konto').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'), array('title' => __('Konto'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-map-marker"> </i> <span>'.__('Adresy').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('Adresy'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-truck"> </i> <span>'.__('Dostawa i płatność').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('Dostawa i płatność'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-check-square"> </i> <span>'.__('Potwierdzenie').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_confirm'), array('title' => __('Potwierdzenie'), 'escape' => false)); ?>
          </li>
        </ul>
        <!--/.orderStep end-->
      </div>
    </div>
  </div>

  <div class="row">
    <?php if(isset($authUser) && $authUser): ?>
      <div class="col-xs-12">
        <p><?php echo __('Jesteś zalogowany jako ').trim($authUser['first_name'].' '.$authUser['last_name']); ?></p>
        <?php echo $this->Html->link('<i class="fa fa-lock"></i> '.__('Moje konto'), array('controller' => 'User', 'action' => 'account'), array('title' => __('Moje konto'), 'escape' => false)); ?>
      </div>
      <div class="col-xs-12">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-sm-offset-8">
            <?php echo $this->Html->link(__('DALEJ').' <i class="fa fa-arrow-right"></i>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('DALEJ'), 'escape' => false, 'class' => 'btn  btn-block btn-lg btn-primary')); ?>
          </div>
        </div>
      </div>
    <?php else: ?>
      <div class="col-lg-6 col-md-6">
        <h2><?php echo __('Jestem już klientem Alcatras'); ?></h2>
        <?php if ($this->Session->check('Message.order_account_login')): ?>
          <div class="row">
            <?php echo $this->Session->flash('order_account_login'); ?>
          </div>
        <?php endif; ?>
        <?php echo $this->Form->create('User', array('url' => array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'))); ?>
          <input type="hidden" name="data[mode]" value="login" />
          <div class="row">
            <div class="col-xs-12 col-md-10">
              <div class="form-group <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('email', array('label' => __('Adres e-mail').': <sup>*</sup>', 'placeholder' => __('Wpisz e-mail'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-md-10">
              <div class="form-group login-password <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password', array('label' => __('Hasło').': <sup>*</sup>', 'placeholder' => __('Wpisz hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <!--<div class="row">
            <div class="col-xs-12 col-md-10">
              <div class="form-group">
                <div>
                  <div class="checkbox login-remember">
                    <label>
                        <input name="rememberme" value="forever" checked="checked" type="checkbox"><?php echo __('Zapamiętaj mnie'); ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>-->
          <div class="row">
            <div class="col-xs-12 col-md-10">
              <div>
                <div>
                  <button name="submit" class="btn  btn-block btn-lg btn-primary" value="<?php echo __('ZALOGUJ SIĘ'); ?>" type="submit">
                    <?php echo __('ZALOGUJ SIĘ'); ?> <i class="fa fa-arrow-right"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
        <br/><br/><br/>
      </div>
      <div class="col-lg-6 col-md-6">
        <h2><?php echo __('Jestem nowym klientem'); ?></h2>
        <?php if ($this->Session->check('Message.order_account_register')): ?>
          <div class="row">
            <?php echo $this->Session->flash('order_account_register'); ?>
          </div>
        <?php endif; ?>
        <?php echo $this->Form->create('Customer', array('url' => array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'))); ?>
          <input type="hidden" name="data[mode]" value="register" />
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group <?php echo $this->Form->isFieldError('first_name') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('first_name', array('label' => __('Imię').': <sup>*</sup>', 'placeholder' => __('Wpisz imię'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group <?php echo $this->Form->isFieldError('last_name') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('last_name', array('label' => __('Nazwisko').': <sup>*</sup>', 'placeholder' => __('Wpisz nazwisko'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required is_company <?php echo $this->Form->isFieldError('company') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('company', array('label' => __('Nazwa firmy').':', 'placeholder' => __('Wpisz nazwę firmy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required is_company <?php echo $this->Form->isFieldError('UserAddress.0.nip') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('UserAddress.0.nip', array('label' => __('Numer NIP firmy').':', 'placeholder' => __('Wpisz numer NIP firmy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required <?php echo $this->Form->isFieldError('UserAddress.0.street_address') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('UserAddress.0.street_address', array('label' => __('Ulica i nr domu').': <sup>*</sup>', 'placeholder' => __('Wpisz ulicę i nr domu'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-5">
              <div class="form-group required <?php echo $this->Form->isFieldError('UserAddress.0.post_code') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('UserAddress.0.post_code', array('label' => __('Kod pocztowy').': <sup>*</sup>', 'placeholder' => __('Wpisz kod pocztowy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
            <div class="col-xs-7">
              <div class="form-group required <?php echo $this->Form->isFieldError('UserAddress.0.city') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('UserAddress.0.city', array('label' => __('Miejscowość').': <sup>*</sup>', 'placeholder' => __('Wpisz miejscowość'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group required <?php echo $this->Form->isFieldError('UserAddress.0.country_id') ? 'has-error' : ''; ?>">
                <?php echo $this->Form->input('UserAddress.0.country_id', array('label' => __('Kraj').': <sup>*</sup>', 'empty' => __('Wybierz kraj'), 'default' => $defaultCountryId, 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'onchange' => 'App.Cart.registrationChangeCountry($(this).val());')); ?>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group <?php echo $this->Form->isFieldError('UserAddress.0.zone_id') ? 'has-error' : ''; ?>">
                <?php echo $this->Form->input('UserAddress.0.zone_id', array('label' => __('Województwo').':', 'empty' => __('Wybierz województwo'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required <?php echo $this->Form->isFieldError('contact_phone') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('contact_phone', array('label' => __('Telefon').': <sup>*</sup>', 'placeholder' => __('Wpisz nr telefonu'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('email', array('label' => __('Adres e-mail').': <sup>*</sup>', 'placeholder' => __('Wpisz e-mail'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required <?php echo $this->Form->isFieldError('non_guest_account') ? 'has-error' : ''; ?>">
                  <label class="checkbox-inline c-checkbox">
                    <?php echo $this->Form->input('non_guest_account', array('type' => 'checkbox', 'label' => false, 'div' => false, 'onchange' => 'App.Cart.registrationChangeCreateAccount();')); ?>
                    <?php echo __('Chcę zarejestrować konto i mieć wgląd w historię zamówień'); ?>
                  </label>
              </div>
            </div>
          </div>
          <div class="row register-password" <?php echo $this->data['Customer']['non_guest_account'] ? 'style="display:none;"' : ''; ?>>
            <div class="col-xs-6">
              <div class="form-group <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password', array('label' => __('Hasło').': <sup>*</sup>', 'placeholder' => __('Wpisz hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group <?php echo $this->Form->isFieldError('password2') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password2', array('label' => __('Powtórz hasło').': <sup>*</sup>', 'placeholder' => __('Wpisz ponownie hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'type' => 'password')); ?>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required <?php echo $this->Form->isFieldError('is_shipping_address') ? 'has-error' : ''; ?>">
                  <label class="checkbox-inline c-checkbox">
                    <?php echo $this->Form->input('is_shipping_address', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                    <?php echo __('Adres do rozliczeń jest również adresem dostawy'); ?>
                  </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required <?php echo $this->Form->isFieldError('accept_rules') ? 'has-error' : ''; ?>">
                  <label class="checkbox-inline c-checkbox">
                    <?php echo $this->Form->input('accept_rules', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                    <?php
                      $text = __('Akceptuję <a href="%rulesUrl">regulamin</a> oraz <a href="%privacyUrl">politykę prywatności</a> Alcatras');
                      $text = str_replace('%rulesUrl', Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'terms')), $text);
                      $text = str_replace('%privacyUrl', Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'privacy')), $text);
                    ?>
                    <?php echo $text.'<sup>*</sup>'; ?>
                  </label>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12">
              <button name="submit" class="btn  btn-block btn-lg btn-primary" value="<?php echo __('DALEJ'); ?>" type="submit">
                <?php echo __('DALEJ'); ?> <i class="fa fa-arrow-right"></i>
              </button>
            </div>
          </div>
        <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
      </div>
    <?php endif; ?>
  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->