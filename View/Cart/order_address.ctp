<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Koszyk'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index')),
    array(__('Zamówienie'), $this->here)
  ))); ?>

  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner">
        <span><i class="glyphicon glyphicon-shopping-cart"></i> <?php echo __('Zamówienie'); ?> </span></h1>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 rightSidebar">
      <h4 class="caps">
        <?php echo $this->Html->link('<i class="fa fa-chevron-left"></i> '.__('Wróć do zakupów'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Wróć do zakupów'), 'escape' => false)); ?>
      </h4>
    </div>
  </div>
  <!--/.row-->
  
  <div class="row">
    <div class="col-xs-12">
      <div class="w100 clearfix">
        <ul class="orderStep orderStepLook2">
          <li>
            <?php echo $this->Html->link('<i class="fa fa-envelope"> </i> <span>'.__('Konto').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'), array('title' => __('Konto'), 'escape' => false)); ?>
          </li>
          <li class="active">
            <?php echo $this->Html->link('<i class="fa fa-map-marker"> </i> <span>'.__('Adresy').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('Adresy'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-truck"> </i> <span>'.__('Dostawa i płatność').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('Dostawa i płatność'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-check-square"> </i> <span>'.__('Potwierdzenie').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_confirm'), array('title' => __('Potwierdzenie'), 'escape' => false)); ?>
          </li>
        </ul>
        <!--/.orderStep end-->
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
      <h2><?php echo __('Adres do rozliczenia'); ?></h2>
      <p>
        <?php if (!empty($cart['BillingOrderUserAddress'])): ?>
          <?php echo $this->User->getFormattedAddress($cart['BillingOrderUserAddress'], '<br/>'); ?>
          <?php if (!empty($cart['BillingOrderUserAddress']['phone'])): ?>
            <br/><?php echo __('tel.').' '.h($cart['BillingOrderUserAddress']['phone']); ?>
          <?php endif; ?>
        <?php endif; ?>
      </p>
      
      <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Edytuj adres'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_edit', $cart['BillingOrderUserAddress']['user_address_id'], 'from' => 'billing'), array('title' => __('Edytuj adres'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
      <?php echo $this->Html->link('<i class="fa fa-plus-circle"></i> '.__('Dodaj nowy'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'billing'), array('title' => __('Dodaj nowy'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
      <br/><br/>
    </div>
    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
      <h2><?php echo __('Adres do dostawy'); ?></h2>
      <?php if ($cart['BillingOrderUserAddress']['user_address_id'] == $cart['ShippingOrderUserAddress']['user_address_id']): ?>
        <p><?php echo __('Zgodny z adresem do rozliczenia'); ?></p>
        <?php echo $this->Html->link('<i class="fa fa-plus-circle"></i> '.__('Dodaj nowy'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'shippping'), array('title' => __('Dodaj nowy'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
      <?php else: ?>
        <p>
          <?php if (!empty($cart['ShippingOrderUserAddress'])): ?>
            <?php echo $this->User->getFormattedAddress($cart['ShippingOrderUserAddress'], '<br/>'); ?>
            <?php if (!empty($cart['ShippingOrderUserAddress']['phone'])): ?>
              <br/><?php echo __('tel.').' '.h($cart['ShippingOrderUserAddress']['phone']); ?>
            <?php endif; ?>
          <?php endif; ?>
        </p>
        <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Edytuj adres'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_edit', $cart['ShippingOrderUserAddress']['user_address_id'], 'from' => 'shipping'), array('title' => __('Edytuj adres'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
        <?php echo $this->Html->link('<i class="fa fa-plus-circle"></i> '.__('Dodaj nowy'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'shipping'), array('title' => __('Dodaj nowy'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
      <?php endif; ?>
    </div>
  </div>
  <div class="row">
    <br/><br/>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-8">
          <?php echo $this->Html->link(__('DALEJ').' <i class="fa fa-arrow-right"></i>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('DALEJ'), 'escape' => false, 'class' => 'btn  btn-block btn-lg btn-primary')); ?>
        </div>
      </div>
    </div>
  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->