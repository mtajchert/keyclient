<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Koszyk'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index')),
    array(__('Zamówienie'), $this->here)
  ))); ?>

  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner">
        <span><i class="glyphicon glyphicon-shopping-cart"></i> <?php echo __('Zamówienie'); ?> </span></h1>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 rightSidebar">
      <h4 class="caps">
        <?php echo $this->Html->link('<i class="fa fa-chevron-left"></i> '.__('Wróć do zakupów'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Wróć do zakupów'), 'escape' => false)); ?>
      </h4>
    </div>
  </div>
  <!--/.row-->
  
  <div class="row">
    <div class="col-xs-12">
      <div class="w100 clearfix">
        <ul class="orderStep orderStepLook2">
          <li>
            <?php echo $this->Html->link('<i class="fa fa-envelope"> </i> <span>'.__('Konto').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'), array('title' => __('Konto'), 'escape' => false)); ?>
          </li>
          <li class="active">
            <?php echo $this->Html->link('<i class="fa fa-map-marker"> </i> <span>'.__('Adresy').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('Adresy'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-truck"> </i> <span>'.__('Dostawa i płatność').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('Dostawa i płatność'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-check-square"> </i> <span>'.__('Potwierdzenie').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_confirm'), array('title' => __('Potwierdzenie'), 'escape' => false)); ?>
          </li>
        </ul>
        <!--/.orderStep end-->
      </div>
    </div>
  </div>

  <div class="row">
  	<div class="col-sm-5 col-xs-12">
      <?php if (!empty($userAddresses)): ?>
        <h2><?php echo $from == 'billing' ? __('Wybierz zapisany adres do rozliczenia') : __('Wybierz zapisany adres do dostawy'); ?></h2>
        <br/>
        <?php echo $this->element('../Cart/order_address_list_side', array('userAddresses' => $userAddresses, 'from' => $from, 'froma' => $froma)); ?>
      <?php endif; ?>
    </div>
    <div class="col-sm-7 col-xs-12">
      <?php if (isset($create) && $create): ?>
        <h2><?php echo $from == 'billing' ? __('Dodawanie adresu do rozliczenia') : __('Dodawanie adresu do dostawy'); ?></h2>
      <?php else: ?>
        <h2><?php echo $from == 'billing' ? __('Edycja adresu do rozliczenia') : __('Edycja adresu do dostawy'); ?></h2>
      <?php endif; ?>
      <?php echo $this->Form->create('UserAddress'); ?>
        <div class="row">
          <div class="col-xs-6">
            <div class="form-group required <?php echo $this->Form->isFieldError('first_name') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('first_name', array('label' => __('Imię').': <sup>*</sup>', 'placeholder' => __('Wpisz imię'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group required <?php echo $this->Form->isFieldError('last_name') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('last_name', array('label' => __('Nazwisko').': <sup>*</sup>', 'placeholder' => __('Wpisz nazwisko'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group required <?php echo $this->Form->isFieldError('company') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('company', array('label' => __('Nazwa firmy').':', 'placeholder' => __('Wpisz nazwę firmy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group required <?php echo $this->Form->isFieldError('nip') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('nip', array('label' => __('Numer NIP firmy').':', 'placeholder' => __('Wpisz numer NIP firmy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group required <?php echo $this->Form->isFieldError('street_address') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('street_address', array('label' => __('Ulica i nr domu').': <sup>*</sup>', 'placeholder' => __('Wpisz ulicę i nr domu'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-5">
            <div class="form-group required <?php echo $this->Form->isFieldError('post_code') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('post_code', array('label' => __('Kod pocztowy').': <sup>*</sup>', 'placeholder' => __('Wpisz kod pocztowy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
          <div class="col-xs-7">
            <div class="form-group required <?php echo $this->Form->isFieldError('city') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('city', array('label' => __('Miejscowość').': <sup>*</sup>', 'placeholder' => __('Wpisz miejscowość'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <div class="form-group required <?php echo $this->Form->isFieldError('country_id') ? 'has-error' : ''; ?>">
              <?php echo $this->Form->input('country_id', array('label' => __('Kraj').': <sup>*</sup>', 'empty' => __('Wybierz kraj'), 'default' => $defaultCountryId, 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'onchange' => 'App.Users.changeCountry($(this).val());')); ?>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="form-group <?php echo $this->Form->isFieldError('zone_id') ? 'has-error' : ''; ?>">
              <?php echo $this->Form->input('zone_id', array('label' => __('Województwo').':', 'empty' => __('Wybierz województwo'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group required <?php echo $this->Form->isFieldError('phone') ? 'has-error' : ''; ?>">
              <div>
                <?php echo $this->Form->input('phone', array('label' => __('Telefon').': <sup>*</sup>', 'placeholder' => __('Wpisz nr telefonu'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group required <?php echo $this->Form->isFieldError('default_billing') ? 'has-error' : ''; ?>">
              <label class="checkbox-inline c-checkbox">
                <?php echo $this->Form->input('default_billing', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                <?php echo __('domyślny adres rozliczeniowy'); ?>
              </label>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group required <?php echo $this->Form->isFieldError('default_shipping') ? 'has-error' : ''; ?>">
              <label class="checkbox-inline c-checkbox">
                <?php echo $this->Form->input('default_shipping', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                <?php echo __('domyślny adres dostawy'); ?>
              </label>
            </div>
          </div>
        </div>

        <div class="col-lg-12 col-xs-12 clearfix">
          <button type="submit" class="btn   btn-primary"><i class="fa fa-map-marker"></i> <?php echo __('Zapisz adres'); ?></button>
        </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>    
  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->