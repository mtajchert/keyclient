<?php foreach ($userAddresses as $address): ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">
            <!--<strong><?php echo h($address['UserAddress']['alias']); ?></strong>-->
            <?php if ($address['UserAddress']['default_billing']): ?>
              <small> <?php echo __('domyślny adres rozliczeniowy'); ?> </small>
            <?php endif; ?>
            <?php if ($address['UserAddress']['default_shipping']): ?>
              <small> <?php echo ($address['UserAddress']['default_billing'] ? ', ' : '').__('domyślny adres dostawy'); ?> </small>
            <?php endif; ?>
          </h3>
        </div>
        <div class="panel-body">
          <ul>
            <li><span class="address-name">
                <strong><?php echo h($address['UserAddress']['first_name'].' '.$address['UserAddress']['last_name']); ?></strong></span>
            </li>
            <?php if ($address['UserAddress']['is_company']): ?>
              <li><span class="address-company"> <?php echo h($address['UserAddress']['company']); ?> </span></li>
            <?php endif; ?>
            <li><span class="address-line1"> <?php echo h($address['UserAddress']['street_address']); ?> </span></li>
            <li><span class="address-line1"> <?php echo h($address['UserAddress']['post_code'].' '.$address['UserAddress']['city']); ?> </span></li>
            <?php if (isset($address['Zone']['name']) && !empty($address['Zone']['name']) || !$address['Country']['default']): ?>
              <li><span class="address-line2"> <?php echo h($address['Country']['name'].(isset($address['Zone']['name']) && !empty($address['Zone']['name']) ? ', '.$address['Zone']['name'] : '')); ?> </span></li>
            <?php endif; ?>
            <?php if (!empty($address['UserAddress']['phone'])): ?>
            <li><span> Tel.: <?php echo h($address['UserAddress']['phone']); ?> </span></li>
            <?php endif; ?>
          </ul>
        </div>
        <div class="panel-footer panel-footer-address">
          <?php echo $this->Html->link('<i class="fa fa-check-square"></i> '.__('Wybierz'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_choose', $address['UserAddress']['id'], 'from' => $from), array('title' => __('Wybierz'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
          <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Edytuj'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_edit', $address['UserAddress']['id'], 'from' => $from), array('title' => __('Edytuj'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
          <?php if (!$address['UserAddress']['default_billing'] && !$address['UserAddress']['default_shipping']): ?>
            <?php echo $this->Html->link('<i class="fa fa-plus-circle"></i> '.__('Usuń'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_delete', $address['UserAddress']['id'], 'from' => $from, 'froma' => $froma), array('title' => __('Usuń'), 'escape' => false, 'class' => 'btn btn-sm btn-danger')); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
