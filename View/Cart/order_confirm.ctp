<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Koszyk'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index')),
    array(__('Zamówienie'), $this->here)
  ))); ?>

  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner">
        <span><i class="glyphicon glyphicon-shopping-cart"></i> <?php echo __('Zamówienie'); ?> </span></h1>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 rightSidebar">
      <h4 class="caps">
        <?php echo $this->Html->link('<i class="fa fa-chevron-left"></i> '.__('Wróć do zakupów'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Wróć do zakupów'), 'escape' => false)); ?>
      </h4>
    </div>
  </div>
  <!--/.row-->
  
  <div class="row">
    <div class="col-xs-12">
      <div class="w100 clearfix">
        <ul class="orderStep orderStepLook2">
          <li>
            <?php echo $this->Html->link('<i class="fa fa-envelope"> </i> <span>'.__('Konto').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'), array('title' => __('Konto'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-map-marker"> </i> <span>'.__('Adresy').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('Adresy'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-truck"> </i> <span>'.__('Dostawa i płatność').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('Dostawa i płatność'), 'escape' => false)); ?>
          </li>
          <li class="active">
            <?php echo $this->Html->link('<i class="fa fa-check-square"> </i> <span>'.__('Potwierdzenie').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_confirm'), array('title' => __('Potwierdzenie'), 'escape' => false)); ?>
          </li>
        </ul>
        <!--/.orderStep end-->
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
      <h2><?php echo __('Potwierdzenie zamówienia'); ?></h2>
    </div>
    <div class="col-xs-12 col-sm-4">
      <div class="row">
        <div class="col-xs-12">
          <h4><?php echo __('Adres do rozliczenia'); ?></h4>
          <p>
            <?php if (!empty($cart['BillingOrderUserAddress'])): ?>
              <?php echo $this->User->getFormattedAddress($cart['BillingOrderUserAddress'], '<br/>'); ?>
              <?php if (!empty($cart['BillingOrderUserAddress']['phone'])): ?>
                <br/><?php echo __('tel.').' '.h($cart['BillingOrderUserAddress']['phone']); ?>
              <?php endif; ?>
            <?php endif; ?>
          </p>
          <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Edytuj adres'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_edit', $cart['BillingOrderUserAddress']['user_address_id'], 'from' => 'billing'), array('title' => __('Edytuj adres'), 'escape' => false, 'class' => 'btn btn-sm btn-default')); ?>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12">
          <h4><?php echo __('Adres do dostawy'); ?></h4>
          <?php if ($cart['BillingOrderUserAddress']['user_address_id'] == $cart['ShippingOrderUserAddress']['user_address_id']): ?>
            <p><?php echo __('Zgodny z adresem do rozliczenia'); ?></p>
            <?php echo $this->Html->link('<i class="fa fa-plus-circle"></i> '.__('Dodaj nowy'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_new', 'from' => 'shippping'), array('title' => __('Dodaj nowy'), 'escape' => false, 'class' => 'btn btn-sm btn-default')); ?>
          <?php else: ?>
            <p>
              <?php if (!empty($cart['ShippingOrderUserAddress'])): ?>
                <?php echo $this->User->getFormattedAddress($cart['ShippingOrderUserAddress'], '<br/>'); ?>
                <?php if (!empty($cart['ShippingOrderUserAddress']['phone'])): ?>
                  <br/><?php echo __('tel.').' '.h($cart['ShippingOrderUserAddress']['phone']); ?>
                <?php endif; ?>
              <?php endif; ?>
            </p>
            <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Edytuj adres'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address_edit', $cart['ShippingOrderUserAddress']['user_address_id'], 'from' => 'shipping'), array('title' => __('Edytuj adres'), 'escape' => false, 'class' => 'btn btn-sm btn-default')); ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12 order-confirm-shipping">
          <h4><?php echo __('Forma dostawy'); ?></h4>
          <p>
          <?php if (!empty($cart['Shipping']['image'])): ?>
            <?php echo $this->App->showShippingImage($cart['Shipping']['image'], 'THUMB'); ?>
          <?php else: ?>
            <?php echo h($cart['Shipping']['shop_name']); ?>
          <?php endif; ?>
          </p>
          <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Zmień'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('Zmień'), 'escape' => false, 'class' => 'btn btn-sm btn-default')); ?>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12 order-confirm-payment">
          <h4><?php echo __('Forma płatności'); ?></h4>
          <p>
          <?php if (!empty($cart['Payment']['image'])): ?>
            <?php echo $this->App->showPaymentImage($cart['Payment']['image'], 'THUMB'); ?>
          <?php else: ?>
            <?php echo h($cart['Payment']['name']); ?>
          <?php endif; ?>
          </p>
          <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Zmień'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('Zmień'), 'escape' => false, 'class' => 'btn btn-sm btn-default')); ?>

        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-8">
      <div class="row">
        <h4><?php echo __('Koszyk'); ?></h4>
        <div class="cartContent w100">
          <table class="cartTable table-responsive" style="width:100%">
            <tbody>
              <tr class="CartProduct cartTableHeader">
                <td style="width:15%"> <?php echo __('Produkt'); ?></td>
                <td style="width:40%"><?php echo __('Szczegóły'); ?></td>
                <td style="width:10%"> <?php echo __('Ilość'); ?></td>
                <td style="width:10%"> <?php echo __('Rabat'); ?></td>
                <td style="width:15%"> <?php echo __('Cena razem'); ?></td>
              </tr>
              <?php foreach ($cart['CartProduct'] as $cartProduct): ?>
                <tr class="CartProduct">
                  <td class="CartProductThumb">
                    <div>
                      <a href="<?php echo $this->App->getProductViewUrl($cartProduct); ?>">
                        <?php if (isset($cartProduct['Product']['ProductsImage'][0]['image']) && !empty($cartProduct['Product']['ProductsImage'][0]['image'])): ?>
                          <?php echo $this->App->showProductsImage($cartProduct['Product']['ProductsImage'][0]['image'], 'LIST', array('class' => 'img-responsive', 'alt' => h($cartProduct['name']))); ?>
                        <?php endif; ?>
                      </a>
                    </div>
                  </td>
                  <td>
                    <div class="CartDescription">
                      <h4>
                        <a href="<?php echo $this->App->getProductViewUrl($cartProduct); ?>">
                          <?php echo h($cartProduct['name']); ?>
                        </a>
                      </h4>
                      <span class="size"> <?php echo h($cartProduct['add_info']); ?> </span>

                      <div class="price"><span> <?php echo h($cartProduct['price_tax']).' '.__('zł'); ?> </span></div>
                    </div>
                  </td>
                  <td><?php echo round($cartProduct['amount']); ?></td>
                  <td>0</td>
                  <td class="price"> <?php echo h($cartProduct['total_price_tax']).' '.__('zł'); ?> </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-8">
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12 col-sm-7">
          <p><?php echo str_replace("\n", '<br/>', h($cart['Shipping']['info'])); ?></p>
          <p><?php echo str_replace("\n", '<br/>', h($cart['Payment']['info'])); ?></p>
        </div>
        <div class="col-xs-12 col-sm-5">
          <div class="w100 cartMiniTable">
            <table id="cart-summary" class="std table">
              <tbody>
                <tr>
                  <td><?php echo __('Produkty'); ?></td>
                  <td class="price"> <?php echo h($cart['Cart']['products_price_tax']).' '.__('zł'); ?> </td>
                </tr>
                <tr style="">
                  <td><?php echo __('Dostawa'); ?></td>
                  <td class="price">
                    <?php if ($cart['Cart']['shipping_id'] > 0 && empty($cart['Cart']['shipping_price_tax'])): ?>
                      <span class="success">GRATIS!</span>
                    <?php else: ?>
                      <?php echo h($cart['Cart']['shipping_price_tax']).' '.__('zł'); ?>
                    <?php endif; ?>
                  </td>
                </tr>
                <tr>
                  <td> <?php echo __('Razem'); ?></td>
                  <td class=" site-color" id="total-price"> <?php echo h($cart['Cart']['order_price_tax']).' '.__('zł'); ?> </td>
                </tr>
                <tr>
                  <td colspan="2" class="text-right">
                    <small><?php echo __('w tym VAT'); ?></small>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <br/>
    </div>
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-8">
          <?php echo $this->Form->create('Cart'); ?>
            <?php
            $confirm_text = __('KUPUJĘ I PŁACĘ');
            if (!empty($cart['Payment']['confirm_btn_text'])) {
              $confirm_text = h($cart['Payment']['confirm_btn_text']);
            }
            ?>
            <button name="submit" class="btn  btn-block btn-lg btn-primary" value="<?php echo __('DALEJ'); ?>" type="submit">
              <?php echo $confirm_text; ?> <i class="fa fa-arrow-right"></i>
            </button>
          <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
        </div>
      </div>
    </div>
  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->