<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Koszyk'), array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index')),
    array(__('Zamówienie'), $this->here)
  ))); ?>

  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner">
        <span><i class="glyphicon glyphicon-shopping-cart"></i> <?php echo __('Zamówienie'); ?> </span></h1>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 rightSidebar">
      <h4 class="caps">
        <?php echo $this->Html->link('<i class="fa fa-chevron-left"></i> '.__('Wróć do zakupów'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Wróć do zakupów'), 'escape' => false)); ?>
      </h4>
    </div>
  </div>
  <!--/.row-->
  
  <div class="row">
    <div class="col-xs-12">
      <div class="w100 clearfix">
        <ul class="orderStep orderStepLook2">
          <li>
            <?php echo $this->Html->link('<i class="fa fa-envelope"> </i> <span>'.__('Konto').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_account'), array('title' => __('Konto'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-map-marker"> </i> <span>'.__('Adresy').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_address'), array('title' => __('Adresy'), 'escape' => false)); ?>
          </li>
          <li class="active">
            <?php echo $this->Html->link('<i class="fa fa-truck"> </i> <span>'.__('Dostawa i płatność').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_payment_shipping'), array('title' => __('Dostawa i płatność'), 'escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link('<i class="fa fa-check-square"> </i> <span>'.__('Potwierdzenie').'</span>', array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'order_confirm'), array('title' => __('Potwierdzenie'), 'escape' => false)); ?>
          </li>
        </ul>
        <!--/.orderStep end-->
      </div>
    </div>
  </div>

  <div class="row">
    <?php echo $this->Session->flash('order_process'); ?>
  </div>
  
  <div class="row">
    <?php echo $this->Form->create('Cart'); ?>
      <div class="col-xs-12 col-md-6">
        <h2><?php echo __('Forma dostawy'); ?></h2>
        <?php foreach ($shippingsFull as $shipping): ?>
          <hr/>
          <div class="row shipping-form-item" onclick="App.Cart.selectShippingRow(this);">
            <div class="col-xs-8">
              <div class="radio">
                <label>
                  &nbsp; <input name="data[Cart][shipping_id]" value="<?php echo h($shipping['Shipping']['id']); ?>" <?php echo $cart['Cart']['shipping_id'] == $shipping['Shipping']['id'] ? 'checked="checked"' : ''; ?> type="radio" onchange="App.Cart.changeShipping();" data-price="<?php echo h($shipping['Shipping']['for_order_price_tax']); ?>" />
                  <?php echo h($shipping['Shipping']['shop_name']); ?>
                </label>
              </div>
            </div>
            <div class="col-xs-4 text-center">
              <div class="radio"><?php echo h($shipping['Shipping']['for_order_price_tax']).' '.__('zł'); ?></div>
            </div>
          </div>
        <?php endforeach; ?>
        <hr/>
      </div>
      <div class="col-xs-12 visible-xs visible-sm">&nbsp;</div>
      <div class="col-xs-12  col-md-6">
        <h2><?php echo __('Forma płatności'); ?></h2>
        <div id="payments-forms">
          <?php echo $this->element('../Cart/order_payments', array('cart' => $cart, 'payments' => $payments)); ?>
        </div>
        <hr/>
      </div>
      <div class="col-xs-12">
        <div class="row">
          <div class="cart-summary pull-right">
            <table>
              <tr>
                <td><?php echo __('Dostawa:'); ?></td>
                <td class="text-right">
                  <strong id="shipping-price"><?php echo h($cart['Cart']['shipping_price_tax']); ?></strong> <strong><?php echo __('zł'); ?></strong>
                </td>
              </tr>
              <tr>
                <td><?php echo __('Razem:'); ?></td>
                <td class="text-right">
                  <input type="hidden" id="products_price_tax" value="<?php echo h($cart['Cart']['products_price_tax']); ?>" />
                  <strong id="all-price"><?php echo h($cart['Cart']['order_price_tax']); ?></strong> <strong><?php echo __('zł'); ?></strong>
                </td>
              </tr>
              <tr>
                <td colspan="2" class="text-right"><small><?php echo __('w tym VAT'); ?></small></td>
              </tr>
            </table>
          </div>
        </div>
        <br/>
      </div>
      <div class="col-xs-12">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-sm-offset-8">
            <button name="submit" class="btn  btn-block btn-lg btn-primary" value="<?php echo __('DALEJ'); ?>" type="submit">
              <?php echo __('DALEJ'); ?> <i class="fa fa-arrow-right"></i>
            </button>
          </div>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->