<?php if (empty($payments)): ?>
  <div class="row">
    <div class="col-xs-12 text-center">
      <?php echo __('Wybierz formę dostawy, aby zobaczyć dostępne formy płatności'); ?>
    </div>
  </div>
<?php else: ?>
  <?php foreach ($payments as $payment): ?>
    <hr/>
    <div class="row payment-form-item" onclick="App.Cart.selectPaymentRow(this);">
      <div class="col-xs-6">
        <div class="radio">
          <label>
            &nbsp; <input name="data[Cart][payment_id]" value="<?php echo h($payment['Payment']['id']); ?>" <?php echo $cart['Cart']['payment_id'] == $payment['Payment']['id'] ? 'checked="checked"' : ''; ?> type="radio" />
            <?php echo h($payment['Payment']['name']); ?>
          </label>
        </div>
      </div>
      <div class="col-xs-6">
        <?php if (!empty($payment['Payment']['image'])): ?>
          <?php echo $this->App->showPaymentImage($payment['Payment']['image'], 'THUMB'); ?>
        <?php endif; ?>
      </div>
      <div class="col-xs-12 col-sm-7">
        <?php echo h($payment['Payment']['description']); ?>
      </div>
    </div>
  <?php endforeach; ?>
<?php endif; ?>