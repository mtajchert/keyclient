  <div class="row">
    <!--right column-->
    <div class="col-sm-12">
      <div class="row  categoryProduct xsResponse clearfix">
        <?php foreach ($products as $product): ?>
          <div class="item col-sm-4 col-lg-4 col-md-4 col-xs-6" style="height: 470px;">
            <div class="product">
              <div class="image">
                <!--<div class="quickview">
                  <a data-toggle="modal" data-target="#product-details-modal" class="btn btn-xs  btn-quickview" title="Quick View"> Quick View </a>
                </div>-->
                <a href="<?php echo $this->App->getProductViewUrl($product); ?>">
                  <?php if (isset($product['Product']['ProductsImage'][0]['image']) && !empty($product['Product']['ProductsImage'][0]['image'])): ?>
                    <?php echo $this->App->showProductsImage($product['Product']['ProductsImage'][0]['image'], 'THUMB', array('class' => 'img-responsive', 'alt' => h($product['Product']['name']))); ?>
                  <?php endif; ?>
                </a>
                <?php if ($this->Product->isPromotionActive($product) && !empty($product['Product']['promotion_info'])): ?>
                <div class="promotion">
                  <span class="discount"><?php echo h($product['Product']['promotion_info']); ?></span>
                </div>
                <?php endif; ?>
              </div>
              <div class="description">
                <h4>
                  <a href="<?php echo $this->App->getProductViewUrl($product); ?>">
                    <?php echo h($product['Product']['name']); ?>
                  </a>
                </h4>

                <div class="grid-description">
                  <p><?php echo h(mb_substr(strip_tags($product['Product']['short_description']), 0, 50)); ?></p>
                </div>
                <div class="list-description">
                  <p><?php echo h(strip_tags($product['Product']['short_description'])); ?></p>
                </div>
                <!--<span class="size">XL / XXL / S </span>-->
              </div>
              <div class="price">
                <span><?php echo h($product['Product']['price_tax']).' '.__('zł'); ?></span>
                <?php if ($product['Product']['old_price'] > 0): ?>
                  <span class="old-price"><?php echo h($product['Product']['old_price']).' '.__('zł'); ?></span>
                <?php endif; ?>
              </div>
              <div class="action-control">
                <a class="btn btn-primary" href="javascript:void(0);" onclick="App.Cart.addProductQuick(<?php echo h($product['Product']['id']); ?>, this);">
                  <span class="add2cart">
                    <i class="glyphicon glyphicon-shopping-cart"> </i> <?php echo __('Do koszyka'); ?>
                  </span>
                </a>
              </div>
            </div>
          </div>
          <!--/.item-->
        <?php endforeach; ?>
      </div>
      <!--/.categoryProduct || product content end-->
    </div>
    <!--/right column end-->
  </div>
  <!-- /.row  -->
