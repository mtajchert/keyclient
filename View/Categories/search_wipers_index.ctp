<div class="container headerOffset">
  <h1 class="text-left border-title"> <?php echo __('Szukaj wycieraczki'); ?> </h1>

  <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
        <label for="exampleInputEmail1">Marka</label>
        <select class="form-control" placeholder="Wybierz markę" name="data[Wiper][brand]" id="wiper-brand-s">
          <?php if (isset($brands) && !empty($brands)): ?>
            <?php foreach ($brands as $id => $name): ?>
              <option value="<?php echo h($id); ?>" <?php echo $id == $brandId ? 'selected' : ''; ?>> <?php echo h($name); ?> </option>
            <?php endforeach; ?>
          <?php endif; ?>
        </select>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="exampleInputEmail1">Model</label>
        <select class="form-control" placeholder="Wybierz model" name="data[Wiper][model]" id="wiper-model-s" <?php echo isset($models) && !empty($models) ? '' : 'disabled="disabled"'; ?>>
          <?php if (isset($models) && !empty($models)): ?>
            <?php foreach ($models as $id => $name): ?>
              <option value="<?php echo h($id); ?>" <?php echo $id == $modelId ? 'selected' : ''; ?>> <?php echo h($name); ?> </option>
            <?php endforeach; ?>
          <?php endif; ?>
        </select>
      </div>
    </div>
    <div class="col-sm-4">  
      <buttton typ="button" style="margin-top: 27px;" id="search-wiper-btn-s" class="btn btn-block btn-md btn-primary"><?php echo __('SZUKAJ'); ?></buttton>
    </div>
  </div>
  <hr /> 

  <div class="row">
    <!--right column-->
    <div class="col-sm-12">
      <?php foreach ($groupedProducts as $group): ?>
      	<h3><?php echo __($group['Category']['name']); ?></h3>
        <div class="row categoryProduct xsResponse clearfix">
          <?php foreach ($group['Products'] as $product): ?>
            <div class="item col-sm-4 col-lg-4 col-md-4 col-xs-6" style="height: 470px;">
              <div class="product">
                <div class="image">
                  <!--<div class="quickview">
                    <a data-toggle="modal" data-target="#product-details-modal" class="btn btn-xs  btn-quickview" title="Quick View"> Quick View </a>
                  </div>-->
                  <?php
                  $productViewUrl = $this->App->getProductViewUrl($product, isset($product['ProductsProductsOption']['ProductOptionValue']) ? $product['ProductsProductsOption']['id'] : null);
                  ?>
                  <a href="<?php echo $productViewUrl; ?>">
                    <?php if (isset($product['Product']['ProductsImage'][0]['image']) && !empty($product['Product']['ProductsImage'][0]['image'])): ?>
                      <?php echo $this->App->showProductsImage($product['Product']['ProductsImage'][0]['image'], 'THUMB', array('class' => 'img-responsive', 'alt' => h($product['Product']['name']))); ?>
                    <?php endif; ?>
                  </a>
                  <?php if ($this->Product->isPromotionActive($product) && !empty($product['Product']['promotion_info'])): ?>
                    <div class="promotion">
                      <span class="discount"><?php echo h($product['Product']['promotion_info']); ?></span>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="description" style="min-height: 60px;">
                  <h4 style="height: 2em;">
                    <a href="<?php echo $productViewUrl; ?>">
                      <?php echo h($product['Product']['name']); ?>
                    </a>
                  </h4>
                  <?php if (isset($product['ProductsProductsOption']['ProductOptionValue'])): ?>
                    <span class="size"> <?php echo h($product['ProductsProductsOption']['ProductOptionValue']['name']); ?> </span>
                  <?php endif; ?>
                </div>
                <div class="price">
                  <?php if (isset($product['ProductsProductsOption']['ProductOptionValue'])): ?>
                    <span><?php echo h($product['ProductsProductsOption']['change_price_tax']).' '.__('zł'); ?></span>
                  <?php else: ?>
                    <span><?php echo h($product['Product']['price_tax']).' '.__('zł'); ?></span>
                  <?php endif; ?>
                </div>
                <div class="action-control">
                  
                  <?php if (isset($product['ProductsProductsOption']['ProductOptionValue'])): ?>
                    <input type="hidden" name="multi-add-data[]" value="<?php echo htmlspecialchars(json_encode(array('product_id' => $product['Product']['id'], 'options' => array($product['ProductsProductsOption']['product_option_id'] => $product['ProductsProductsOption']['product_option_value_id'])))); ?>" />
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="App.Cart.addProductQuick(<?php echo h($product['Product']['id']); ?>, this, {<?php echo h($product['ProductsProductsOption']['product_option_id']); ?>:<?php echo h($product['ProductsProductsOption']['product_option_value_id']); ?>});">
                  <?php else: ?>
                    <input type="hidden" name="multi-add-data[]" value="<?php echo htmlspecialchars(json_encode(array('product_id' => $product['Product']['id'], 'options' => array()))); ?>" />
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="App.Cart.addProductQuick(<?php echo h($product['Product']['id']); ?>, this);">
                  <?php endif; ?>
                  
                    <span class="add2cart">
                      <i class="glyphicon glyphicon-shopping-cart"> </i> <?php echo __('Do koszyka'); ?>
                    </span>
                  </a>
                </div>
              </div>
            </div>
            <!--/.item-->
          <?php endforeach; ?>
          <div class="col-sm-12">
            <buttton type="button" style="margin-top: 27px;" class="btn btn-block btn-md btn-primary" onclick="App.Cart.addMultiProductQuick(this);"><?php echo __('DODAJ ZESTAW DO KOSZYKA'); ?></buttton>
          </div>
        </div>
        <!--/.categoryProduct || product content end-->
        <hr />
        <?php endforeach; ?>
    </div>
    <!--/right column end-->
  </div>
  <!-- /.row  -->
</div>