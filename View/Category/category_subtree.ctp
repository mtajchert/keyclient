<ul class="category-level-2 dropdown-menu-tree">
  <?php foreach ($categories as $category): ?>
    <?php if (isset($subcategories[$category['Category']['id']])): ?>
      <li class="dropdown-tree open-tree">
        <a <?php echo $category['Category']['id'] == $activeCategoryId ? 'class="dropdown-tree-a"' : ''; ?> href="<?php echo $this->App->getCategoryViewUrl($category); ?>">
          <?php echo h($category['Category']['name']); ?>
        </a>
        <?php
        echo $this->element('../Category/category_subtree', array(
          'categories' => $subcategories[$category['Category']['id']],
          'subcategories' => $subcategories,
          'activeCategoryId' => $activeCategoryId
        ));
        ?>
      </li>
    <?php else: ?>
      <li>
        <a <?php echo $category['Category']['id'] == $activeCategoryId ? 'class="dropdown-tree-a"' : ''; ?> href="<?php echo $this->App->getCategoryViewUrl($category); ?>">
          <?php echo h($category['Category']['name']); ?>
        </a>
      </li>
    <?php endif; ?>
  <?php endforeach; ?>
</ul>