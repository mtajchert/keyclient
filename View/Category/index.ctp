<?php $this->Paginator->options['url'] = $this->App->getCategoryViewUrl($category, true); ?>
<div class="container main-container headerOffset">
  <?php $breadcrumbs = array(); ?>
  <?php for ($i = 0; $i < count($breadcrumbsCategories); $i++): ?>
    <?php $breadcrumbs[] = array(h($breadcrumbsCategories[$i]['Category']['name']), $this->App->getCategoryViewUrl($breadcrumbsCategories[$i])); ?>
  <?php endfor; ?>

  <?php echo $this->element('breadcrumb', array('breadcrumbs' => $breadcrumbs)); ?>

  <div class="row">

    <!--left column-->

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div id="accordionNo" class="panel-group">
        <!--Category-->
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-title">
              <a class="collapseWill expanded" href="#collapseCategory" data-toggle="collapse" aria-expanded="true">
                <span class="pull-left">
                    <i class="fa fa-caret-right"></i></span> <strong><?php echo h($breadcrumbsCategories[0]['Category']['name']); ?></strong>
              </a>
            </div>
          </div>
          <div class="panel-collapse collapse in" id="collapseCategory" aria-expanded="true">
            <div class="panel-body">
              <ul class="nav nav-pills nav-stacked tree open-tree">
                <?php foreach ($subcategories[$breadcrumbsCategories[0]['Category']['id']] as $subcategory): ?>
                  <?php if (isset($subcategories[$subcategory['Category']['id']])): ?>
                    <li class="dropdown-tree open-tree <?php echo in_array($subcategory['Category']['id'], $activeCategories) ? 'active' : ''; ?>">
                      <a class="dropdown-tree-a" href="<?php echo $this->App->getCategoryViewUrl($subcategory); ?>">
                        <!--<span class="badge pull-right"><?php echo 13; //h($subcategory['Category']['products_active']);  ?></span>-->
                        <?php echo h($subcategory['Category']['name']); ?>
                      </a>
                      <?php
                      echo $this->element('../Category/category_subtree', array(
                        'categories' => $subcategories[$subcategory['Category']['id']],
                        'subcategories' => $subcategories,
                        'activeCategoryId' => $category['Category']['id']
                      ));
                      ?>
                    </li>
                  <?php else: ?>
                    <li class="<?php echo in_array($subcategory['Category']['id'], $activeCategories) ? 'active' : ''; ?>">
                      <a href="<?php echo $this->App->getCategoryViewUrl($subcategory); ?>">
                        <!--<span class="badge pull-right"><?php echo 13; //h($subcategory['Category']['products_active']);  ?></span>-->
                    <?php echo h($subcategory['Category']['name']); ?>
                      </a>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <!--/Category menu end-->
      </div>
    </div>

    <!--right column-->
    <div class="col-lg-9 col-md-9 col-sm-12">
    	<?php if ($this->Paginator->current($model = null) < 2): ?>
        <div class="w100 clearfix category-top">
          <h1> <?php echo h($category['Category']['name']); ?> </h1>
            <?php if (!empty($category['Category']['banner'])): ?>
              <div class="categoryImage">
                <?php if ($category['Category']['banner_redirect_type'] == 'product'): ?>
                  <a href="javascript:void(0);" onclick="App.Categories.redirectToProduct(<?php echo h($category['Category']['banner_product_id']); ?>, '<?php echo $this->App->getProductViewUrl($category['Category']['banner_product_id']); ?>');">
                    <?php echo $this->Html->image('/'.CLIENT.'/categories-banners/show/'.$category['Category']['banner']); ?>
                  </a>
                <?php elseif ($category['Category']['banner_redirect_type'] == 'url'): ?>
                  <a href="<?php echo $category['Category']['banner_url']; ?>">
                    <?php echo $this->Html->image('/'.CLIENT.'/categories-banners/show/'.$category['Category']['banner']); ?>
                  </a>
                <?php else: ?>
                  <?php echo $this->Html->image('/'.CLIENT.'/categories-banners/show/'.$category['Category']['banner']); ?>
                <?php endif; ?>
              </div>
              <br/>
            <?php endif; ?>
            <?php if (!empty($category['Category']['description'])): ?>
              <div class="categoryDesc">
                <?php echo $category['Category']['description']; ?>
              </div>
            <?php endif; ?>
        </div>
      <?php endif; ?>
      <!--/.category-top-->

      <div class="w100 productFilter clearfix">
        <!--<p class="pull-left"> Showing <strong>12</strong> products </p>-->

        <div class="pull-right ">
          <div class="change-order pull-right">
            <div style="display:none;">
              <?php echo $this->Paginator->sort('al_default', null, array('id' => 'sortLink-al_default', 'direction' => 'asc', 'lock' => true)); ?>
              <?php echo $this->Paginator->sort('al_price', null, array('id' => 'sortLink-al_price', 'direction' => 'asc', 'lock' => true)); ?>
              <?php echo $this->Paginator->sort('al_price-desc', null, array('id' => 'sortLink-al_price-desc', 'direction' => 'desc', 'lock' => true)); ?>
              <span><?php echo $this->Paginator->sortKey('ProductsCategory'); ?></span>
            </div>
            <select name="orderby" class="form-control" style="width:200px;" onchange="App.Categories.changeOrder(this);">
              <option value="al_default" <?php echo $sort == 'al_default' ? 'selected="selected"' : ''; ?>>Domyślne sortowanie</option>
              <option value="al_price" <?php echo $sort == 'al_price' ? 'selected="selected"' : ''; ?>>Najpierw najtańsze</option>
              <option value="al_price-desc" <?php echo $sort == 'al_price-desc' ? 'selected="selected"' : ''; ?>>Najpierw najdroższe</option>
            </select>
          </div>
          <div class="change-view pull-right">
            <a class="grid-view" title="<?php echo __('Kafelki'); ?>" href="#"> <i class="fa fa-th-large"></i> </a>
            <a class="list-view " title="<?php echo __('Lista'); ?>" href="#"> <i class="fa fa-th-list"></i> </a>
          </div>
        </div>
      </div>
      <!--/.productFilter-->
      <div class="row  categoryProduct xsResponse clearfix">
        <?php foreach ($products as $product): ?>
        	<?php if (empty($product['Product']['id'])): ?>
        		<?php continue; ?>
        	<?php endif; ?>
          <div class="item col-sm-4 col-lg-4 col-md-4 col-xs-6" style="height: 470px;" itemscope itemtype="http://schema.org/Product">
            <div class="product">
              <div class="image">
                <!--<div class="quickview">
                  <a data-toggle="modal" data-target="#product-details-modal" class="btn btn-xs  btn-quickview" title="Quick View"> Quick View </a>
                </div>-->
                <a href="<?php echo $this->App->getProductViewUrl($product); ?>">
                  <?php if (isset($product['Product']['ProductsImage'][0]['image']) && !empty($product['Product']['ProductsImage'][0]['image'])): ?>
                    <?php echo $this->App->showProductsImage($product['Product']['ProductsImage'][0]['image'], 'THUMB', array('class' => 'img-responsive', 'alt' => h($product['Product']['name']), 'itemprop' => 'image')); ?>
                  <?php endif; ?>
                </a>
                <?php if ($this->Product->isPromotionActive($product) && !empty($product['Product']['promotion_info'])): ?>
                <div class="promotion">
                  <span class="discount"><?php echo h($product['Product']['promotion_info']); ?></span>
                </div>
                <?php endif; ?>
              </div>
              <div class="description">
                <h3 itemprop="name">
                	<a href="<?php echo $this->App->getProductViewUrl($product); ?>">
                    <?php echo h($product['Product']['name']); ?>
                	</a>
                </h3>
                <p><?php echo strip_tags($product['Product']['short_description']); ?></p>

                <div class="list-description" itemprop="description">
                  <p><?php echo strip_tags($product['Product']['short_description']); ?></p>
                </div>
                <!--<span class="size">XL / XXL / S </span>-->
              </div>
              <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <meta itemprop="priceCurrency" content="PLN" />
                <span><span itemprop="price"><?php echo h($product['Product']['price_tax']); ?></span><?php echo ' '.__('zł'); ?></span>
                <?php if ($product['Product']['old_price'] > 0): ?>
                  <span class="old-price"><?php echo h($product['Product']['old_price']).' '.__('zł'); ?></span>
                <?php endif; ?>
              </div>
              <div class="action-control">
                <a class="btn btn-primary" href="javascript:void(0);" onclick="App.Cart.addProductQuick(<?php echo h($product['Product']['id']); ?>, this);">
                  <span class="add2cart">
                    <i class="glyphicon glyphicon-shopping-cart"> </i> <?php echo __('Do koszyka'); ?>
                  </span>
                </a>
              </div>
            </div>
          </div>
          <!--/.item-->
        <?php endforeach; ?>
          <p>&nbsp</p>
      </div>
      <!--/.categoryProduct || product content end-->
      
      <div class="w100 categoryFooter">
        <div class="pagination pull-left no-margin-top">
          <ul class="pagination no-margin-top">
            <?php
            
            if ($this->Paginator->current() > 1) {
              echo $this->Html->meta(array('rel' => 'prev', 'link' => array_merge($this->App->getCategoryViewUrl($category, true), array('page' => $this->Paginator->current()-1))), false, array('inline'=>false));
            }
            if ($this->Paginator->current() < intval($this->Paginator->counter('{:pages}'))) {
              echo $this->Html->meta(array('rel' => 'next', 'link' => array_merge($this->App->getCategoryViewUrl($category, true), array('page' => $this->Paginator->current()+1))), false, array('inline'=>false));
            }
            
            echo $this->Paginator->prev('«', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
            echo $this->Paginator->next('»', array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            ?>
          </ul>
        </div>
        <div class="pull-right pull-right col-sm-4 col-xs-12 no-padding text-right text-left-xs">
          <p><?php echo $this->Paginator->counter(array('format' => __('Produkty {:start} - {:end} z {:count}'))); ?></p>
        </div>
      </div>
      <!--/.categoryFooter-->
    </div>
    <!--/right column end-->
  </div>
  <!-- /.row  -->
</div>