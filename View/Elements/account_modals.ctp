<!-- Modal Login start -->
<?php if ($this->Session->check('Message.auth_login') || (isset($loginForm) && $loginForm)): ?>
  <script>
    $(document).ready(function () {
      $('#ModalLogin').modal('show');
    });
  </script>
<?php endif; ?>
<div class="modal signUpContent fade" id="ModalLogin" tabindex="-1" role="dialog">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button style="opacity: 1;margin-top:10px;" type="button" class="close" data-dismiss="modal" aria-hidden="true">
        	<span style="color: white;" class="glyphicon glyphicon-remove"></span>
        </button>
        <div class="modal-title-site text-center"><?php echo __('LOGOWANIE'); ?></div>
      </div>
      <div class="modal-body">
        <div class="row">
          <?php echo $this->Session->flash('auth_login'); ?>
        </div>
        <?php echo $this->Form->create('User', array('url' => array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'login'))); ?>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('email', array('label' => false, 'placeholder' => __('Wpisz e-mail'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group login-password <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password', array('label' => false, 'placeholder' => __('Wpisz hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <!--<div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                <div>
                  <div class="checkbox login-remember">
                    <label>
                        <input name="rememberme" value="forever" checked="checked" type="checkbox"><?php echo __('Zapamiętaj mnie'); ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>-->
          <div class="row">
            <div class="col-xs-12">
              <div>
                <div>
                  <input name="submit" class="btn  btn-block btn-lg btn-primary" value="<?php echo __('ZALOGUJ SIĘ'); ?>" type="submit" />
                </div>
              </div>
            </div>
          </div>
        <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
        <!--userForm-->
      </div>

      <div class="modal-footer">
        <p class="text-center"><?php echo __('Nie masz konta?'); ?> <a data-toggle="modal" data-dismiss="modal" href="#ModalSignup"> <i><?php echo __('Zarejestruj się.'); ?></i> </a> <br>
          <?php echo $this->Html->link(__('Zgubiłeś hasło?'), array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'reset_password'), array('title' => __('Zgubiłeś hasło?'))); ?>
        </p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Login -->

<!-- Modal Signup start -->
<?php if ($this->Session->check('Message.auth_register') || (isset($registerForm) && $registerForm)): ?>
  <script>
    $(document).ready(function () {
      $('#ModalSignup').modal('show');
    });
  </script>
<?php endif; ?>
<div class="modal signUpContent fade" id="ModalSignup" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button style="opacity: 1;margin-top:10px;" type="button" class="close" data-dismiss="modal" aria-hidden="true"> <span style="color: white;" class="glyphicon glyphicon-remove"></span> </button>
        <div class="modal-title-site text-center"> <?php echo __('REJESTRACJA'); ?> </div>
      </div>
      <div class="modal-body">
        <div class="row">
          <?php echo $this->Session->flash('auth_register'); ?>
        </div>
        <?php echo $this->Form->create('Customer', array('url' => array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'register'))); ?>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('first_name') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('first_name', array('label' => false, 'placeholder' => __('Wpisz imię'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('last_name') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('last_name', array('label' => false, 'placeholder' => __('Wpisz nazwisko'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('email', array('label' => false, 'placeholder' => __('Wpisz e-mail'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password', array('label' => false, 'placeholder' => __('Wpisz hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group required <?php echo $this->Form->isFieldError('accept_rules') ? 'has-error' : ''; ?>">
                  <label class="checkbox-inline c-checkbox">
                    <?php echo $this->Form->input('accept_rules', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                    <?php
                      $text = __('Akceptuję <a href="%rulesUrl">regulamin</a> oraz <a href="%privacyUrl">politykę prywatności</a> Alcatras');
                      $text = str_replace('%rulesUrl', Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'terms')), $text);
                      $text = str_replace('%privacyUrl', Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'privacy')), $text);
                    ?>
                    <?php echo $text.'<sup>*</sup>'; ?>
                  </label>
              </div>
            </div>
          </div>
          <!--<div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                <div>
                  <div class="checkbox login-remember">
                    <label>
                      <input name="rememberme" value="forever" checked="checked" type="checkbox">
                      <?php echo __('Zapamiętaj mnie'); ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>-->
          <div>
            <div>
              <input name="submit" class="btn  btn-block btn-lg btn-primary" value="<?php echo __('ZAREJESTRUJ'); ?>" type="submit">
            </div>
          </div>
        <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
        <!-- /userForm-->
      </div>
      <div class="modal-footer">
        <p class="text-center">
          <?php echo __('Masz już konto?'); ?>
          <a data-toggle="modal" data-dismiss="modal" href="#ModalLogin"><?php echo __('Zaloguj się'); ?></a>
        </p>
      </div>
    </div>
    <!-- /.modal-content -->

  </div>
  <!-- /.modal-dialog -->

</div>