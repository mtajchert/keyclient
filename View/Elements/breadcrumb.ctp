<div class="row">
  <div class="breadcrumbDiv col-lg-12">
    <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <?php echo $this->Html->link('<span itemprop="name">'.__('Strona główna').'</span>', array('controller' => 'Pages', 'action' => 'home'), array('title' => __('Strona główna'), 'escape' => false, 'itemprop' => 'item')); ?>
        <meta itemprop="position" content="1" />
      </li>
      <?php for ($i = 0, $cnt = count($breadcrumbs); $i < $cnt; $i++): ?>
        <?php $item = $breadcrumbs[$i]; ?>
        <li <?php echo ($i == $cnt-1 ? 'class="active"' : ''); ?> itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <?php echo $this->Html->link('<span itemprop="name">'.$item[0].'</span>', $item[1], array('title' => $item[0], 'escape' => false, 'itemprop' => 'item')); ?>
          <meta itemprop="position" content="<?php echo $i+2; ?>" />
        </li>
      <?php endfor; ?>
    </ul>
  </div>
</div>
<!--/.row-->
