<div class="col-xs-12">
  <div role="alert" class="alert alert-danger" id="flash_error">
    <?php echo $message; ?>
  </div>
</div>
<script>
  setTimeout(function () {
    $('#flash_error').fadeOut(600);
  }, 10000);
</script>