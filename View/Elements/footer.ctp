<footer>
  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
          <div class="header-3"> Pomoc </div>
          <ul>
            <li class="supportLi">
            	<div class="header-4">
            		<a class="inline" href="callto:+48684005040"> <strong> <i class="fa fa-phone"> </i> 68 400 50 40, 693 932 812 </strong> </a>
          		</div>
          		<div class="header-4">
          			<a class="inline" href="mailto:sklep@alcatras.pl"><i class="fa fa-envelope-o"> </i> sklep@alcatras.pl </a>
        			</div>
        			<br/>
        			<div class="header-4">
        				Alcatras Aleksandra Gembara<br/>
        				ul. Kożuchowska 20D<br/>65-001 Zielona Góra<br/>
        				NIP: 9291857973
        			</div>
            </li>
          </ul>
        </div>
        
        <div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
        	<div class="header-3"> Informacje </div>
          <ul>
            <li>
              <a href="<?php echo $this->App->getPageViewUrl($menuCertificatesPage); ?>" rel="nofollow">
                <?php echo h($menuCertificatesPage['Page']['title']); ?>
              </a>
            </li>
            <li>
              <a href="<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'privacy')); ?>" rel="nofollow">
                <?php echo __('Polityka prywatności'); ?>
              </a>
            </li>
            <li>
              <a href="<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'terms')); ?>" rel="nofollow">
                <?php echo __('Regulamin'); ?>
              </a>
            </li>
            <li>
              <a href="<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'contact')); ?>" >
                <?php echo __('Kontakt'); ?>
              </a>
            </li>
          </ul>
        </div>
        
      	<div style="clear:both" class="hide visible-xs"></div>
      	
      	<div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
      		<div class="header-3"> Obsługa klienta </div>      		
          <ul>
            <li>
              <a href="<?php echo $this->App->getPageViewUrl($menuShippingAndPaymentsPage); ?>" rel="nofollow">
                <?php echo h($menuShippingAndPaymentsPage['Page']['title']); ?>
              </a>
            </li>
            <li>
              <a href="<?php echo $this->App->getPageViewUrl($menuComplaintPage); ?>" rel="nofollow">
                <?php echo h($menuComplaintPage['Page']['title']); ?>
              </a>
            </li>
            <li>
              <a href="<?php echo $this->App->getPageViewUrl($menuTerminationPage); ?>" rel="nofollow">
                <?php echo h($menuTerminationPage['Page']['title']); ?>
              </a>
            </li>
          </ul>
        </div>
        
        <div style="clear:both" class="hide visible-xs"></div>

        <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
          <div class="header-3"> Zostańmy w kontakcie </div>
          <ul class="social">
            <li><a href="https://www.facebook.com/alcatras"> <i class=" fa fa-facebook"> &nbsp; </i> </a></li>
            <li><a href="https://twitter.com/AlcatrasPL"> <i class="fa fa-twitter"> &nbsp; </i> </a></li>
            <li><a href="https://plus.google.com/106712075155904031341/posts"> <i class="fa fa-google-plus"> &nbsp; </i> </a></li>
            <li style="display: none;"><a href="http://pinterest.com"> <i class="fa fa-pinterest"> &nbsp; </i> </a></li>
            <li style="display: none;"><a href="http://youtube.com"> <i class="fa fa-youtube"> &nbsp; </i> </a></li>
          </ul>
        </div>
      </div>
      <!--/.row-->
    </div>
    <!--/.container-->
  </div>
  <!--/.footer-->

  <div class="footer-bottom">
    <div class="container">
      <p class="pull-left"> Copyright &copy; <?php echo date('Y'); ?> M&M Solutions Wszelkie prawa zastrzeżone</p>
  
      <div class="pull-right paymentMethodImg">          
        <?php echo $this->Html->image(CLIENT.'.logo.jpg', array( 'class' => 'hidden-sm hidden-xs img-responsive', 'style' => 'max-height:40px;')); ?>
      </div>
    </div>
  </div>
  <!--/.footer-bottom-->
</footer>