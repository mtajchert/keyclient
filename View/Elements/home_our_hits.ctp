<div class="container">
  <?php if (isset($pages) && !empty($pages)): ?>
    <div class="col-lg-12 text-center">
        <h2 class="boxes-title-1 boxes-title-page-home boxes-title-page-home-color"><span><?php echo h($pages[0]['Page']['title']); ?></span></h2>
    </div>
    <div class="col-lg-8 col-lg-offset-2 text-center text-box">
        <?php echo $pages[0]['Page']['content']; ?>
    </div>
    <div style="clear:both"></div>
    <div class="row">
      <div class="col-lg-12"><br/><br/></div>
    </div>
  <?php endif; ?>
  
  <div class="row xsResponse">
    <?php foreach($our_hits as $our_hit): ?>
      <div class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
        <div class="product">
          <div class="image">
            <!--<div class="quickview">
              <a title="Quick View" class="btn btn-xs  btn-quickview" data-target="#product-details-modal" data-toggle="modal"> Quick View </a>
            </div>-->
            <a href="<?php echo $this->App->getProductViewUrl($our_hit); ?>">
              <?php if (isset($our_hit['ProductsImage'][0]['image']) && !empty($our_hit['ProductsImage'][0]['image'])): ?>
                <?php echo $this->App->showProductsImage($our_hit['ProductsImage'][0]['image'], 'THUMB', array('class' => 'img-responsive', 'alt' => h($our_hit['Product']['name']))); ?>
              <?php endif; ?>
            </a>
            <?php if ($this->Product->isPromotionActive($our_hit) && !empty($our_hit['Product']['promotion_info'])): ?>
              <div class="promotion">
                <span class="discount"><?php echo h($our_hit['Product']['promotion_info']); ?></span>
              </div>
            <?php endif; ?>
          </div>
          <div class="description">
            <h3><a href="<?php echo $this->App->getProductViewUrl($our_hit); ?>"><?php echo h($our_hit['Product']['name']); ?></a></h3>
            <p><?php echo h(mb_substr(strip_tags($our_hit['Product']['short_description']), 0, 50)); ?></p>
          </div>
          <div class="price">
            <span><?php echo h($our_hit['Product']['price_tax']).' '.__('zł'); ?></span>
            <?php if ($our_hit['Product']['old_price'] > 0): ?>
              <span class="old-price"><?php echo h($our_hit['Product']['old_price']).' '.__('zł'); ?></span>
            <?php endif; ?>
          </div>
          <div class="action-control">
            <a class="btn btn-primary" onclick="App.Cart.addProductQuick(<?php echo h($our_hit['Product']['id']); ?>, this);">
              <span class="add2cart">
                <i class="fa fa-shopping-cart"> </i>
                <?php echo __('Do koszyka'); ?>
              </span>
            </a>
          </div>
        </div>
      </div>
      <!--/.item-->
    <?php endforeach; ?>

  </div>
  <!-- /.row -->
  
  <?php if (isset($pages) && count($pages) > 1): ?>
    <div class="container">
      <?php for ($i = 1; $i < count($pages); $i++): ?>
        <div class="col-lg-12 text-center">
            <h3 class="boxes-title-1 boxes-title-page-home boxes-title-no-bg"><span><?php echo h($pages[$i]['Page']['title']); ?></span></h3>
        </div>
        <div class="col-lg-8 col-lg-offset-2 text-center text-box">
            <?php echo $pages[$i]['Page']['content']; ?>
        </div>
        <div style="clear:both"></div>
        <div class="row">
          <div class="col-lg-12"><br/><br/></div>
        </div>
      <?php endfor; ?>
      
    </div>
  <?php endif; ?>
</div>