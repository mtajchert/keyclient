<div class="w100 sectionCategory morePost row featuredPostContainer style2 globalPaddingTop no-margin">
  <?php if (isset($pages) && !empty($pages)): ?>
    <div class="container">
      <div class="col-lg-12 text-center">
        <div class="boxes-title-1 boxes-title-page-home boxes-title-no-bg"><h2 style="font-size: 25px;"><span><?php echo h($pages[0]['Page']['title']); ?></span></h2></div>
      </div>
      <div class="col-lg-8 col-lg-offset-2 text-center text-box">
          <?php echo $pages[0]['Page']['content']; ?>
      </div>
      <div style="clear:both"></div>
      <div class="row">
        <div class="col-lg-12"><br/><br/></div>
      </div>
    </div>
  <?php endif; ?>
  
  <div class="container">
    <div class="row xsResponse">
      <?php foreach($promotions as $promotion): ?>
        <div class="item col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="product">
            <div class="image">
              <!--<div class="quickview">
                <a title="Quick View" class="btn btn-xs  btn-quickview" data-target="#product-details-modal" data-toggle="modal"> Quick View </a>
              </div>-->
              <a href="<?php echo $this->App->getProductViewUrl($promotion); ?>">
                <?php if (isset($promotion['ProductsImage'][0]['image']) && !empty($promotion['ProductsImage'][0]['image'])): ?>
                  <?php echo $this->App->showProductsImage($promotion['ProductsImage'][0]['image'], 'THUMB', array('class' => 'img-responsive', 'alt' => h($promotion['Product']['name']))); ?>
                <?php endif; ?>
              </a>
              <?php if (!empty($promotion['Product']['promotion_info'])): ?>
                <div class="promotion">
                  <span class="discount"><?php echo h($promotion['Product']['promotion_info']); ?></span>
                </div>
              <?php endif; ?>
            </div>
            <div class="description">
              <h3><a href="<?php echo $this->App->getProductViewUrl($promotion); ?>"><?php echo h($promotion['Product']['name']); ?></a></h3>
              <p><?php echo strip_tags($promotion['Product']['short_description']); ?></p>
            </div>
            <div class="price">
              <span><?php echo h($promotion['Product']['price_tax']).' '.__('zł'); ?></span>
              <?php if ($promotion['Product']['old_price'] > 0): ?>
                <span class="old-price"><?php echo h($promotion['Product']['old_price']).' '.__('zł'); ?></span>
              <?php endif; ?>
            </div>
            <div class="action-control">
              <a class="btn btn-primary" onclick="App.Cart.addProductQuick(<?php echo h($promotion['Product']['id']); ?>, this);">
                <span class="add2cart">
                  <i class="fa fa-shopping-cart"> </i>
                  <?php echo __('Do koszyka'); ?>
                </span>
              </a>
            </div>
          </div>
        </div>
        <!--/.item-->
      <?php endforeach; ?>

    </div>
    <!-- /.row -->
  </div>
  <!--/.container-->
  
  <?php if (isset($pages) && count($pages) > 1): ?>
    <div class="container">
      <?php for ($i = 1; $i < count($pages); $i++): ?>
        <div class="col-lg-12 text-center">
            <h3 class="boxes-title-1 boxes-title-page-home boxes-title-no-bg"><span><?php echo h($pages[$i]['Page']['title']); ?></span></h3>
        </div>
        <div class="col-lg-8 col-lg-offset-2 text-center text-box">
            <?php echo $pages[$i]['Page']['content']; ?>
        </div>
        <div style="clear:both"></div>
        <div class="row">
          <div class="col-lg-12"><br/><br/></div>
        </div>
      <?php endfor; ?>
      
    </div>
  <?php endif; ?>
</div>