<h2 class="boxes-title-1"><span>NAJPOPULARNIEJSZE PRODUKTY MOTORYZACYJNE</span></h2>

<div class="row featuredPostContainer">
  <div class="featuredImageLook3">
    <div class="col-md-4 col-sm-4 col-xs-4 col-xs-min-12">
      <div class="inner">
        <a href="<?php echo $this->App->getCategoryViewUrl($accessoriesCategory); ?>">
          <div class="box-content-overly box-content-overly-white">
            <div class="box-text-table">
              <div class="box-text-cell ">
                <div class="box-text-cell-inner dark">
                  <h3 class="uppercase"><?php echo h($accessoriesCategory['Category']['name']); ?></h3>
                  <p> <?php echo mb_substr(strip_tags($accessoriesCategory['Category']['description']), 0, 100); ?> </p>
                </div>
              </div>
            </div>
          </div>
        </a>
        <div class="img-title"> <?php echo h($accessoriesCategory['Category']['name']); ?> </div>
        <a class="img-block" href="<?php echo $this->App->getCategoryViewUrl($accessoriesCategory); ?>">
          <?php echo $this->Html->image(CLIENT.'.accesories.jpg', array( 'class' => 'img-responsive')); ?>
        </a>
      </div>
    </div>
    
    <div class="col-md-4 col-sm-4 col-xs-4 col-xs-min-12">
      <div class="inner disabled">
        <a href="">
          <div class="box-content-overly box-content-overly-white">
            <div class="box-text-table">
              <div class="box-text-cell ">              
                <div class="box-text-cell-inner dark">                
                  <h3 class="uppercase">Tuning</h3>
                  <p>Zachęcający opis</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        <div class="img-title"> Tuning</div>
        <a class="img-block" href="#">
          <?php echo $this->Html->image(CLIENT.'.tuning-disabled.jpg', array( 'class' => 'img-responsive')); ?>
        </a>
      </div>
    </div>
    
    <div class="col-md-4 col-sm-4 col-xs-4 col-xs-min-12">
      <div class="inner disabled">
        <a href="#">
          <div class="box-content-overly box-content-overly-white">
            <div class="box-text-table">
              <div class="box-text-cell ">
                <div class="box-text-cell-inner dark">
                  <h3 class="uppercase">Motocykle</h3>
                  <p>Zachęcający opis.</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        <div class="img-title"> Motocykle</div>
        <a class="img-block" href="#">
          <?php echo $this->Html->image(CLIENT.'.motocycles-disabled.jpg', array( 'class' => 'img-responsive')); ?>
        </a>
      </div>
    </div>
  </div>
  <!--/.featuredImageLook3-->
</div>
<!--/.featuredPostContainer-->