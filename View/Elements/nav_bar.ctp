<!-- Fixed navbar start -->
<div class="navbar navbar-tshop navbar-fixed-top megamenu" role="navigation">
  <div class="navbar-top">
    <div class="container">
      <div class="row">
      
        <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6">
          <div class="pull-left ">
            <ul class="userMenu ">
              <li>
                <span>
                  <span class="hidden-xs">SZYBKA POMOC</span>
                  <i class="glyphicon glyphicon-info-sign hide visible-xs "></i>
                </span>
              </li>
              <li class="phone-number">
                <a href="callto:+48684005040">
                  <span><i class="glyphicon glyphicon-phone-alt "></i></span>
                  <span class="hidden-xs" style="margin-left:5px"> 68 400 50 40, 693 932 812 </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        
        <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6 no-padding">
          <div class="pull-right">
            <ul class="userMenu">
              <?php if(isset($authUser) && $authUser): ?>
                <li>
                  <a href="<?php echo $this->Html->url(array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'account')); ?>">
                    <span class="hidden-xs"> <?php echo __('MOJE KONTO'); ?></span>
                    <i class="glyphicon glyphicon-user hide visible-xs "></i>
                  </a>
                </li>
                <li>
                  <a href="<?php echo $this->Html->url(array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'logout')); ?>">
                    <span class="hidden-xs"> <?php echo __('WYLOGUJ SIĘ'); ?></span>
                    <i class="glyphicon glyphicon-log-in hide visible-xs "></i>
                  </a>
                </li>
              <?php else: ?>
                <li>
                  <a href="#" data-toggle="modal" data-target="#ModalLogin" rel="nofollow">
                    <span class="hidden-xs"> <?php echo __('ZALOGUJ SIĘ'); ?></span>
                    <i class="glyphicon glyphicon-log-in hide visible-xs "></i>
                  </a>
                </li>
                <li class="hidden-xs">
                  <a href="#" data-toggle="modal" data-target="#ModalSignup" rel="nofollow"> <?php echo __('UTWÓRZ KONTO'); ?></a>
                </li>
              <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/.navbar-top-->

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"> Toggle navigation </span>
                <span class="icon-bar"> </span>
                <span class="icon-bar"> </span>
                <span class="icon-bar"> </span>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-cart" rel="nofollow">
              <i class="fa fa-shopping-cart colorWhite"> </i>
              <span class="cartRespons colorWhite"> <?php echo $this->element('cart_response'); ?> </span>
            </button>
            <a class="navbar-brand " href="/" style="padding: 2px 20px;">
              <?php echo $this->Html->image(CLIENT.'.logo.jpg', array( 'class' => 'hidden-sm hidden-xs img-responsive', 'style' => 'max-height:100%;')); ?>
              <?php echo $this->Html->image(CLIENT.'.logo-sm.png', array( 'class' => 'hidden-md hidden-lg img-responsive', 'style' => 'max-height:100%;')); ?>
            </a>
            <!-- this part for mobile -->
            <div class="search-box pull-right hidden-lg hidden-md hidden-sm">
                <div class="input-group">
                    <button class="btn btn-nobg getFullSearch" type="button" rel="nofollow"><i class="fa fa-search"> </i></button>
                </div>
            </div>
        </div>

        <!-- this part is duplicate from cartMenu  keep it for mobile -->
        <div class="navbar-cart  collapse">
            <div class="cartMenu  col-lg-4 col-xs-12 col-md-4 ">
                <?php echo $this->element('mini_cart_mobile'); ?>
            </div>
            <!--/.cartMenu-->
        </div>
        <!--/.navbar-cart-->

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown megamenu-fullwidth">
                <a data-toggle="dropdown" class="dropdown-toggle" href="<?php echo $this->App->getCategoryViewUrl($menuMainCategory); ?>"> <?php echo h($menuMainCategory['Category']['name']); ?> <b class="caret"> </b> 
                </a>
                <ul class="dropdown-menu">
                  <li class="megamenu-content">
                    <?php
                    $columnCnt = ceil(count($menuSubCategories) / 3);
                    $menuSubsCatsColumns = array(array(), array(), array());
                    
                    for ($i = 0; $i < count($menuSubCategories); $i++) {
                      $menuSubsCatsColumns[intval($i / $columnCnt)][] = $menuSubCategories[$i];
                    }
                    ?>
                    <?php foreach ($menuSubsCatsColumns as $column): ?>
                      <ul class="col-lg-3  col-sm-3 col-md-3 unstyled noMarginLeft newCollectionUl">
                        <?php foreach ($column as $menuCat): ?>
                          <li onmouseover="App.changeSubmenuImage($(this).parent().closest('li').find('.newProductMenuBlock img'), '<?php echo empty($menuCat['Category']['image']) ? '' : '/'.CLIENT.'/categories/thumb/'.$menuCat['Category']['image']; ?>');">
                            <a href="<?php echo $this->App->getCategoryViewUrl($menuCat); ?>">
                              <?php echo h($menuCat['Category']['name']); ?>
                            </a>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    <?php endforeach; ?>
                    <ul class="col-lg-3  col-sm-3 col-md-3  col-xs-3">
                      <li>
                        <a class="newProductMenuBlock" href="#">
                          <?php echo $this->App->showCategoryImage($menuSubCategories[0]['Category']['image'], 'THUMB', array( 'class' => 'img-responsive')); ?>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
                
              <li><a id="wiper-modal-btn" href="javascript:void(0);" rel="nofollow"> <?php echo __('Szukaj wycieraczki'); ?> </a></li>
              
              <li class="dropdown megamenu-s350width">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <?php echo h($menuHelpPage['Page']['title']); ?> <b class="caret"> </b> 
                </a>
                <ul class="dropdown-menu">
                  <li class="megamenu-content">
                    <ul class="col-lg-12  col-sm-12 col-md-12 unstyled noMarginLeft newCollectionUl">
                      <li>
                        <a href="<?php echo $this->App->getPageViewUrl($menuShippingAndPaymentsPage); ?>">
                          <?php echo h($menuShippingAndPaymentsPage['Page']['title']); ?>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'terms')); ?>">
                          <?php echo h($menuTermsPage['Page']['title']); ?>
                        </a>
                      </li>
                      <li>
                        <a href="<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'privacy')); ?>">
                          <?php echo h($menuPrivacyPage['Page']['title']); ?>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>

              <li>
                  <a href="<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'contact')); ?>">
                    <?php echo __('Kontakt'); ?>
                  </a>
              </li>
            </ul>

            <!--- this part will be hidden for mobile version -->
            <div class="nav navbar-nav navbar-right hidden-xs">
                <div class="dropdown  cartMenu ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" rel="nofollow">
                      <i class="fa fa-shopping-cart"> </i>
                      <span class="cartRespons"> <?php echo $this->element('cart_response'); ?> </span>
                      <b class="caret"> </b>
                    </a>
                    <div class="dropdown-menu col-lg-4 col-xs-12 col-md-4 ">
                        <?php echo $this->element('mini_cart'); ?>
                    </div>
                    <!--/.dropdown-menu-->
                </div>
                <!--/.cartMenu-->
                <div class="search-box">
                    <div class="input-group">
                        <button class="btn btn-nobg getFullSearch" type="button" rel="nofollow"><i class="fa fa-search"> </i></button>
                    </div>
                </div>
                <!--/.search-box -->
            </div>
            <!--/.navbar-nav hidden-xs-->
        </div>
        <!--/.nav-collapse -->

    </div>
    <!--/.container -->

    <div class="search-full text-right">
      <?php echo $this->Form->create('Search', array('url' => array('plugin' => CLIENT, 'controller' => 'Search', 'action' => 'search'), 'type' => 'get', 'id' => 'SearchForm')); ?>
        <a class="pull-right search-close"> <i class=" fa fa-times-circle"> </i> </a>

        <div class="searchInputBox pull-right">
            <input type="search" data-searchurl="<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Search', 'action' => 'index')); ?>" name="q" placeholder="<?php echo __('Wpisz fragment nazwy lub opisu szukanego produktu i wciśnij enter'); ?>"
                   class="search-input">
            <button class="btn-nobg search-btn" type="submit"><i class="fa fa-search"> </i></button>
        </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
    <!--/.search-full-->

</div>
<!-- /.Fixed navbar  -->