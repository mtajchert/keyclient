<?php if (!isset($this->firstHeader)) { $this->firstHeader = true; } ?>
<div class="col-lg-12 text-center">
    <?php if ($this->firstHeader): ?>
      <?php $this->firstHeader = false; ?>
      <h1 class="boxes-title-1 boxes-title-page-home boxes-title-no-bg" style="margin-bottom: 10px;"><span><?php echo h($page['Page']['title']); ?></span></h1>
    <?php else: ?>
      <h2 class="boxes-title-1 boxes-title-page-home boxes-title-no-bg" style="margin-bottom: 10px;"><span><?php echo h($page['Page']['title']); ?></span></h2>
    <?php endif; ?>
</div>
<div class="col-lg-8 col-lg-offset-2 text-center">
    <?php echo $page['Page']['content']; ?>
</div>