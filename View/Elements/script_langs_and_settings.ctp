<script>
  $(document).ready(function () {
    App.Categories.lang = {
      banner_redirect_product_message: '<?php echo __('Chcesz dodać produkt do koszyka czy wyświetlić stronę produktu?'); ?>',
      banner_redirect_product_title: '',
      btn_to_cart: '<?php echo __('Dodaj do koszyka'); ?>',
      btn_to_product_page: '<?php echo __('Wyświetl stronę produktu'); ?>',
    };
  
    App.Users.getZonesUrl = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'get_zones')); ?>';
    
    
    App.Cart.cartUrl = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'index')); ?>';
    App.Cart.addProductUrl = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'add_product')); ?>';
    App.Cart.deleteCartProductUrl = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'delete_cart_product')); ?>';
    App.Cart.saveCartProductAmountsUrl = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'save_cart_product_amounts')); ?>';
    App.Cart.getZonesUrl = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'get_zones')); ?>';
    App.Cart.getPayments = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Cart', 'action' => 'get_payments')); ?>';
    
    App.Search.getSearchResults = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Search', 'action' => 'get_search_results')); ?>';
    
    App.Wipers.searchUrl = '<?php echo Router::url(array('plugin' => CLIENT, 'controller' => 'Categories', 'action' => 'search_wipers_index')); ?>';
  });
</script>