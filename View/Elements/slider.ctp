<!-- Main component call to action -->
<div class="row">
  <div class="col-md-12">
    <div id="slideshow" style="height: 357px;">
    <?php foreach ($sliders as $slider): ?>
      <div>
        <?php if (!empty($slider['Slider']['url'])): ?>
          <a href="<?php echo $slider['Slider']['url']; ?>">
            <?php echo $this->App->showSliderImage($slider['Slider']['image'], 'ORIGIN', array('class' => 'img-responsive')); ?>
          </a>
        <?php else: ?>
          <?php echo $this->App->showSliderImage($slider['Slider']['image'], 'ORIGIN', array('class' => 'img-responsive')); ?>
        <?php endif; ?>
      </div>
    <?php endforeach; ?>        
    </div>
  </div>
</div>