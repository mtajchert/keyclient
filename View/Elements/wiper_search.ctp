<div class="modal signUpContent fade" id="wiper-search-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" style="width:98%;max-width:1170px;">
    <div class="modal-content">
      <div class="modal-header">
        <button style="opacity: 1;margin-top:10px;" type="button" class="close" data-dismiss="modal" aria-hidden="true"> <span style="color: white;" class="glyphicon glyphicon-remove"></span> </button>
        <h3 class="modal-title-site text-center"><?php echo __('Szukaj Wycieraczki'); ?></h3>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label for="exampleInputEmail1">Marka</label>
              <select class="form-control" placeholder="Wybierz markę" name="data[Wiper][brand]" id="wiper-brand"></select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="exampleInputEmail1">Model</label>
              <select class="form-control" placeholder="Wybierz model" name="data[Wiper][model]" id="wiper-model" disabled="disabled"></select>
            </div>
          </div>
          <div class="col-sm-4">  
            <buttton typ="button" style="margin-top: 27px;" id="search-wiper-btn" class="btn btn-block btn-md btn-primary"><?php echo __('SZUKAJ'); ?></buttton>
          </div>
        </div>
        <hr />    
        <div id="wipers-container"></div>    
      </div>

      <div class="modal-footer">
        <p class="text-center"></p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Login -->