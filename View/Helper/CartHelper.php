<?php

App::uses('AppHelper', 'View/Helper');

class CartHelper extends AppHelper {

  public function __construct(\View $View, $settings = array()) {
    parent::__construct($View, $settings);
    //$this->Product = ClassRegistry::init('KeyAdmin.Product');
  }

  public function getMiniCartView($cart, $mobile = false) {
    return $this->_View->element('../Cart/mini_cart', array('cart' => $cart, 'mobile' => $mobile));
  }
  
  public function getMiniCartLabel($cart) {
    if (empty($cart['CartProduct'])) {
      return __('Koszyk');
    }
    
    return __('Koszyk').' ('.$cart['Cart']['order_price_tax'].' '.__('zł').')';
  }

}
