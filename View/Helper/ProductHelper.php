<?php

App::uses('AppHelper', 'View/Helper');

class ProductHelper extends AppHelper {

  public function __construct(\View $View, $settings = array()) {
    parent::__construct($View, $settings);
    $this->Product = ClassRegistry::init('KeyAdmin.Product');
  }

  public function isPromotionActive($product) {
    return $this->Product->isPromotionActive($product);
  }

}
