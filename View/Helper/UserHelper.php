<?php

App::uses('AppHelper', 'View/Helper');

class UserHelper extends AppHelper {

  public function __construct(\View $View, $settings = array()) {
    parent::__construct($View, $settings);
    $this->UserAddress = ClassRegistry::init('KeyAdmin.UserAddress');
  }
  
  public function getFormattedAddress($userAddress, $lineSeparator = "<br>") {
    return $this->UserAddress->getFormattedAddress($userAddress, $lineSeparator);
  }

}
