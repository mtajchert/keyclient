<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?php echo isset($meta_keywords) ? h($meta_keywords) : ''; ?>" />
    <meta name="description" content="<?php echo (isset($home_description))? $home_description: ''; ?>" />
    <meta name="robots" content="index,follow" />  
    
    
    
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/<?php echo CLIENT; ?>/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/<?php echo CLIENT; ?>/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/<?php echo CLIENT; ?>/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/<?php echo CLIENT; ?>/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/<?php echo CLIENT; ?>/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/<?php echo CLIENT; ?>/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/<?php echo CLIENT; ?>/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/<?php echo CLIENT; ?>/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/<?php echo CLIENT; ?>/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/<?php echo CLIENT; ?>/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/<?php echo CLIENT; ?>/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/<?php echo CLIENT; ?>/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/<?php echo CLIENT; ?>/icon/favicon-16x16.png">
    <link rel="manifest" href="/<?php echo CLIENT; ?>/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">  
    
    <?php if (isset($canonical) && !empty($canonical)): ?>
    	<link rel="canonical" href="<?php echo $canonical; ?>" />
    <?php endif; ?>
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <?php $title = isset($meta_title) && !empty($meta_title) ? $meta_title : __('Sklep motoryzacyjny'); ?>
    <title><?php echo h($title); ?> - Alcatras.pl</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin-ext" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" type="text/css" rel="stylesheet">
    
    <?php $this->AssetCompress->config(AssetConfig::buildFromIniFile(APP.'Plugin'.DS.CLIENT.DS.'Config'.DS.'asset_compress.ini')); ?>
    
    <?php echo $this->AssetCompress->css('KeyClient-frontend', array('raw' => true)); ?>
    
    
    <?php echo $this->AssetCompress->script('KeyClient-frontend-pace', array('raw' => true)); ?>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->

    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <?php echo $this->AssetCompress->script('KeyClient-frontend-head', array('raw' => true)); ?>
    <?php echo $this->AssetCompress->script('KeyClient-frontend-pace', array('raw' => true)); ?>
    
    
    <?php echo $this->fetch('meta'); ?>
  </head>

  <body>
    
    <?php echo $this->element('nav_bar'); ?>
      
    <?php echo $this->element('account_modals'); ?>
	  
    <a name="start-content"></a>
    
    <?php echo $this->fetch('content'); ?>
  
    <?php echo $this->element('footer'); ?>
    
    <?php echo $this->AssetCompress->script('KeyClient-frontend', array('raw' => true)); ?>
      
    <?php echo $this->element('script_langs_and_settings'); ?>
  
    <?php echo $this->element('wiper_search'); ?>  
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-69449605-1', 'auto');
      ga('send', 'pageview');
    
    </script>
    <script type="text/javascript" src="/whcookies.js"></script>
  </body>
</html>
