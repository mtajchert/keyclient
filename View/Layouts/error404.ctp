<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fav and touch icons -->    
    <link rel="apple-touch-icon" sizes="57x57" href="/<?php echo CLIENT; ?>/icon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/<?php echo CLIENT; ?>/icon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/<?php echo CLIENT; ?>/icon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/<?php echo CLIENT; ?>/icon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/<?php echo CLIENT; ?>/icon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/<?php echo CLIENT; ?>/icon/apple-touch-icon-120x120.png">
    <link rel="icon" type="image/png" href="/<?php echo CLIENT; ?>/icon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/<?php echo CLIENT; ?>/icon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/<?php echo CLIENT; ?>/icon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/<?php echo CLIENT; ?>/icon/manifest.json">
    <link rel="shortcut icon" href="/<?php echo CLIENT; ?>/icon/favicon.ico">    
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>Alcatras</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin-ext" type="text/css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" type="text/css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/bootstrap.css?1437921136" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/footable-0.1.css?1437921138" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/footable.sortable-0.1.css?1437921139" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/footable.paginate.css?1437921139" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/jquery.minimalect.css?1439890970" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/bootstrap.css?1437921136" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/skin-11.css?1437921151" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/style.css?1442403084" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/colorbox.css?1442046901" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/skin-alcatras.css?1442073022" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/animate.min.css?1437921133" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/font-awesome.min.css?1437921138" />
    <link rel="stylesheet" type="text/css" href="/<?php echo CLIENT; ?>/css/bootstrap-rating.css?1442402953" />
    <script type="text/javascript" src="/<?php echo CLIENT; ?>/js/Pace/pace.min.js?1437921200"></script>
    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->

    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery/jquery-1.10.1.min.js?1437921189"></script>      </head>

  <body>
    
    <!-- Fixed navbar start -->
<div class="navbar navbar-tshop navbar-fixed-top megamenu" role="navigation">
  <div class="navbar-top">
    <div class="container">
      <div class="row">
      
        <div class="col-lg-6 col-sm-6 col-xs-6 col-md-6">
          <div class="pull-left ">
            <ul class="userMenu ">
              <li>
                <span>
                  <span class="hidden-xs">SZYBKA POMOC</span>
                  <i class="glyphicon glyphicon-info-sign hide visible-xs "></i>
                </span>
              </li>
              <li class="phone-number">
                <a href="callto:+48684005040">
                  <span><i class="glyphicon glyphicon-phone-alt "></i></span>
                  <span class="hidden-xs" style="margin-left:5px"> 68 400 50 40, 693 932 812 </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        
        
      </div>
    </div>
  </div>
  <!--/.navbar-top-->

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"> Toggle navigation </span>
                <span class="icon-bar"> </span>
                <span class="icon-bar"> </span>
                <span class="icon-bar"> </span>
            </button>
            <a class="navbar-brand " href="/" style="padding: 2px 20px;">
              <img src="/<?php echo CLIENT; ?>/img/logo.jpg?1441713587" class="hidden-sm hidden-xs img-responsive" style="max-height:100%;" alt="" />              <img src="/<?php echo CLIENT; ?>/img/logo-sm.png?1441965916" class="hidden-md hidden-lg img-responsive" style="max-height:100%;" alt="" />            </a>
            <!-- this part for mobile -->
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li>
                  <a href="<?php echo $homeUrl; ?>">
                    Strona główna                  </a>
              </li>
            </ul>
        </div>
        <!--/.nav-collapse -->

    </div>
    <!--/.container -->

</div>
<!-- /.Fixed navbar  -->      

    <a name="start-content"></a>
    
<div class="container main-container headerOffset">

    <div class="row innerPage">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row userInfo">

                <p class="lead text-center"> ... Niestety strona o podanym adresie nie istnieje .... </p>

                <h1 class="h1error"> BŁĄD <span class="err404"> 404</span></h1>

                <h1 class="h1error"><span class="err404"> <i class="fa fa-frown-o"></i></span></h1>


            </div>
            <!--/row end-->
        </div>
    </div>
    <!--/.innerPage-->
    <div style="clear:both"></div>
</div>


    <footer>
  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
          <h3> Pomoc </h3>
          <ul>
            <li class="supportLi">
            <h4>
              <a class="inline" href="callto:+48684005040"> <strong> <i class="fa fa-phone"> </i> 68 400 50 40, 693 932 812 </strong> </a>
            </h4>
            <h4>
              <a class="inline" href="mailto:sklep@alcatras.pl"><i class="fa fa-envelope-o"> </i> sklep@alcatras.pl </a>
            </h4>
            </li>
          </ul>
        </div>
        
                <div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
                    <h3> Informacje </h3>
                    <ul>
                      <li>
                        <a href="http://keyshop.rnowak.linuxpl.info/artykuly/certyfikat-prokonsumencki-p29">
                          Certyfikat prokonsumencki                        </a>
                      </li>
                      <li>
                        <a href="/polityka-prywatnosci">
                          Polityka prywatności                        </a>
                      </li>
                      <li>
                        <a href="/regulamin">
                          Regulamin                        </a>
                      </li>
                      <li>
                        <a href="/kontakt">
                          Dane firmy                        </a>
                      </li>
                      <li>
                        <a href="/kontakt">
                          Jak dojechać                        </a>
                      </li>
                    </ul>
                </div>

                <div style="clear:both" class="hide visible-xs"></div>

                <div class="col-lg-3  col-md-3 col-sm-4 col-xs-6">
                    <h3> Obsługa klienta </h3>
                    <ul>
                      <li>
                        <a href="http://keyshop.rnowak.linuxpl.info/artykuly/platnosc-i-dostawa-p24">
                          Płatność i dostawa                        </a>
                      </li>
                      <li>
                        <a href="http://keyshop.rnowak.linuxpl.info/artykuly/reklamacja-towaru-p30">
                          Reklamacja towaru                        </a>
                      </li>
                      <li>
                        <a href="http://keyshop.rnowak.linuxpl.info/artykuly/odstapienie-od-umowy-p31">
                          Odstąpienie od umowy                        </a>
                      </li>
                    </ul>
                </div>

                <div style="clear:both" class="hide visible-xs"></div>

                <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
                    <h3> Zostańmy w kontakcie </h3>
                    <ul class="social">
                        <li><a href="http://facebook.com"> <i class=" fa fa-facebook"> &nbsp; </i> </a></li>
                        <li><a href="http://twitter.com"> <i class="fa fa-twitter"> &nbsp; </i> </a></li>
                        <li><a href="https://plus.google.com"> <i class="fa fa-google-plus"> &nbsp; </i> </a></li>
                        <li><a href="http://pinterest.com"> <i class="fa fa-pinterest"> &nbsp; </i> </a></li>
                        <li><a href="http://youtube.com"> <i class="fa fa-youtube"> &nbsp; </i> </a></li>
                    </ul>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!--/.footer-->

    <div class="footer-bottom">
      <div class="container">
        <p class="pull-left"> Copyright &copy; 2015 M&M Solutions Wszelkie prawa zastrzeżone</p>

        <div class="pull-right paymentMethodImg">          
          <img src="/<?php echo CLIENT; ?>/img/logo.jpg?1441713587" class="hidden-sm hidden-xs img-responsive" style="max-height:40px;" alt="" />        </div>
      </div>
    </div>
    <!--/.footer-bottom-->
</footer>    
    <script type="text/javascript" src="/<?php echo CLIENT; ?>/js/bootstrap.min.js?1437921173"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/bootstrap.touchspin.js?1437921174"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/bootstrap-rating.min.js?1442402947"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/idangerous.swiper-2.1.min.js?1437921180"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.mCustomScrollbar.js?1437921193"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.cycle2.min.js?1437921191"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.easing.1.3.js?1437921192"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.parallax-1.1.js?1437921194"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.cookie.js?1442216375"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/helper-plugins/jquery.mousewheel.min.js?1437921177"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/rhinoslider/easing.js?1440072166"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/rhinoslider/rhinoslider-1.05.min.js?1440072166"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.colorbox-min.js?1442046904"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.scrollto.js?1437921195"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/grids.js?1437921177"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/home.js?1437921178"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/App.js?1441988969"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/AppForm.js?1439900969"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/AppUsers.js?1442071312"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/AppCart.js?1442071311"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/AppCategories.js?1442390097"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/AppWipers.js?1442071307"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/AppSearch.js?1442319345"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/AppProducts.js?1442409107"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/bootbox.min.js?1442071306"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/jquery.minimalect.js?1439890991"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/underscore-min.js?1439904525"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/footable.js?1437921175"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/footable.sortable.js?1437921176"></script><script type="text/javascript" src="/<?php echo CLIENT; ?>/js/script.js?1442318248"></script>      
    <script>
  $(document).ready(function () {
    App.Categories.lang = {
      banner_redirect_product_message: 'Chcesz dodać produkt do koszyka czy wyświetlić stronę produktu?',
      banner_redirect_product_title: '',
      btn_to_cart: 'Dodaj do koszyka',
      btn_to_product_page: 'Wyświetl stronę produktu',
    };
  
    App.Users.getZonesUrl = '/user/get_zones';
    
    
    App.Cart.cartUrl = '/koszyk';
    App.Cart.addProductUrl = '/cart/add_product';
    App.Cart.deleteCartProductUrl = '/cart/delete_cart_product';
    App.Cart.saveCartProductAmountsUrl = '/cart/save_cart_product_amounts';
    App.Cart.getZonesUrl = '/user/get_zones';
    App.Cart.getPayments = '/cart/get_payments';
    
    App.Search.getSearchResults = '/search/get_search_results';
  });
  
  
</script>  
     
  </body>
</html>
