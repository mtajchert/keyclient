<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Zamówienia'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'orders')),
    array(__('Płatność za zamówienie'), $this->here)
  ))); ?>

  <div class="row">
    <div class="col-xs-12">
      <h1 class="section-title-inner">
        <span><i class="fa fa-money"></i> <?php echo __('Płatność za zamówienie'); ?> </span>
      </h1>
    </div>
  </div>
  <!--/.row-->

  <div class="row">
    <?php echo $this->Session->flash('order_process'); ?>
  </div>
  
  <div class="row innerPage">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row userInfo">
          <div class="col-xs-12 col-sm-12">
            <h1 class=" text-left border-title"> <?php echo h($page['Page']['title']); ?> </h1>
            <?php echo $page['Page']['content']; ?>
          </div>
        </div>
        <div class="row">&nbsp;</div>
    </div>
  </div>
</div>
