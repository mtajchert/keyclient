<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Moje konto'), array('controller' => 'User', 'action' => 'account')),
    array(__('Historia zamówień'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'orders')),
    array(__('Szczegoły zamówienia'), $this->here)
  ))); ?>
  
  <div class="row">
    <?php echo $this->Session->flash('payment_process'); ?>
  </div>

  <div class="row">
    <div class="col-xs-12">
      <h1 class="section-title-inner">
        <span><i class="fa fa-list-alt"></i> <?php echo __('Zamówienie').' '.$order['Order']['number']; ?> </span>        
      </h1>
    </div>
  </div>
  <!--/.row-->

  <div class="row">
    <div class="col-xs-12 col-sm-4">
      <div class="row">
        <div class="col-xs-12">
          <h4><?php echo __('Adres do rozliczenia'); ?></h4>
          <p>
            <?php if (!empty($order['BillingOrderUserAddress'])): ?>
              <?php echo $this->User->getFormattedAddress($order['BillingOrderUserAddress'], '<br/>'); ?>
              <?php if (!empty($order['BillingOrderUserAddress']['phone'])): ?>
                <br/><?php echo __('tel.').' '.h($order['BillingOrderUserAddress']['phone']); ?>
              <?php endif; ?>
            <?php endif; ?>
          </p>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12">
          <h4><?php echo __('Adres do dostawy'); ?></h4>
          <?php if ($order['BillingOrderUserAddress']['user_address_id'] == $order['ShippingOrderUserAddress']['user_address_id']): ?>
            <p><?php echo __('Zgodny z adresem do rozliczenia'); ?></p>
          <?php else: ?>
            <p>
              <?php if (!empty($order['ShippingOrderUserAddress'])): ?>
                <?php echo $this->User->getFormattedAddress($order['ShippingOrderUserAddress'], '<br/>'); ?>
                <?php if (!empty($order['ShippingOrderUserAddress']['phone'])): ?>
                  <br/><?php echo __('tel.').' '.h($order['ShippingOrderUserAddress']['phone']); ?>
                <?php endif; ?>
              <?php endif; ?>
            </p>
          <?php endif; ?>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12 order-confirm-shipping">
          <h4><?php echo __('Forma dostawy'); ?></h4>
          <p>
          <?php if (!empty($order['Shipping']['image'])): ?>
            <?php echo $this->App->showShippingImage($order['Shipping']['image'], 'THUMB'); ?>
          <?php else: ?>
            <?php echo h($order['Shipping']['shop_name']); ?>
          <?php endif; ?>
          </p>
          </p>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12 order-confirm-payment">
          <h4><?php echo __('Forma płatności'); ?></h4>
          <p>
          <?php if (!empty($order['Payment']['image'])): ?>
            <?php echo $this->App->showPaymentImage($order['Payment']['image'], 'THUMB'); ?>
          <?php else: ?>
            <?php echo h($order['Payment']['name']); ?>
          <?php endif; ?>
          </p>
          </p>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-8">
      <div class="row">
        <div class="col-xs-12">
          <h4><?php echo __('Produkty'); ?></h4>
          <div class="cartContent w100">
            <table class="cartTable table-responsive" style="width:100%">
              <tbody>
                <tr class="CartProduct cartTableHeader">
                  <td style="width:15%"> <?php echo __('Produkt'); ?></td>
                  <td style="width:40%"><?php echo __('Szczegóły'); ?></td>
                  <td style="width:10%"> <?php echo __('Ilość'); ?></td>
                  <td style="width:10%"> <?php echo __('Rabat'); ?></td>
                  <td style="width:15%"> <?php echo __('Cena razem'); ?></td>
                </tr>
                <?php foreach ($order['OrderProduct'] as $orderProduct): ?>
                  <tr class="CartProduct">
                    <td class="CartProductThumb">
                      <div>
                        <a href="<?php echo $this->App->getProductViewUrl($orderProduct); ?>">
                          <?php if (isset($orderProduct['Product']['ProductsImage'][0]['image']) && !empty($orderProduct['Product']['ProductsImage'][0]['image'])): ?>
                            <?php echo $this->App->showProductsImage($orderProduct['Product']['ProductsImage'][0]['image'], 'LIST', array('class' => 'img-responsive', 'alt' => h($orderProduct['name']))); ?>
                          <?php endif; ?>
                        </a>
                      </div>
                    </td>
                    <td>
                      <div class="CartDescription">
                        <h4>
                          <a href="<?php echo $this->App->getProductViewUrl($orderProduct); ?>">
                            <?php echo h($orderProduct['name']); ?>
                          </a>
                        </h4>
                        <span class="size"> <?php echo h($orderProduct['add_info']); ?> </span>

                        <div class="price"><span> <?php echo h($orderProduct['price_tax']).' '.__('zł'); ?> </span></div>
                      </div>
                    </td>
                    <td><?php echo round($orderProduct['amount']); ?></td>
                    <td>0</td>
                    <td class="price"> <?php echo h($orderProduct['total_price_tax']).' '.__('zł'); ?> </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-8">
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-xs-12 col-sm-7">
          <h4><?php echo __('Informacje'); ?></h4>
          <div class=" statusTop">
            <p>
              <strong><?php echo __('Status'); ?>:</strong>
              <?php echo $order['OrderStatus']['name']; ?>
            </p>
            <p>
              <strong><?php echo __('Status płatności'); ?>:</strong>
              <?php if (!empty($order['Order']['paid'])): ?>
                <span class="label label-success"><?php echo __('ZAPŁACONE'); ?></span>
              <?php else: ?>
                <span class="label label-info"><?php echo __('OCZEKUJĄCE'); ?></span>
              <?php endif; ?>
            </p>
            <p>
              <strong><?php echo __('Data złożenia'); ?>:</strong>
              <?php echo h($this->Time->format($order['Order']['created'], '%Y-%m-%d %H:%M')); ?>
            </p>
            <p>
              <strong><?php echo __('Nr zamówienia'); ?>:</strong>
              <?php echo $order['Order']['number']; ?>
            </p>            
          </div>
        </div>
        <div class="col-xs-12 col-sm-5">
          <div class="w100 cartMiniTable">
            <table id="cart-summary" class="std table">
              <tbody>
                <tr>
                  <td><?php echo __('Produkty'); ?></td>
                  <td class="price"> <?php echo h($order['Order']['products_price_tax']).' '.__('zł'); ?> </td>
                </tr>
                <tr style="">
                  <td><?php echo __('Dostawa'); ?></td>
                  <td class="price">
                    <?php if ($order['Order']['shipping_id'] > 0 && empty($order['Order']['shipping_price_tax'])): ?>
                      <span class="success">GRATIS!</span>
                    <?php else: ?>
                      <?php echo h($order['Order']['shipping_price_tax']).' '.__('zł'); ?>
                    <?php endif; ?>
                  </td>
                </tr>
                <tr>
                  <td> <?php echo __('Razem'); ?></td>
                  <td class=" site-color" id="total-price"> <?php echo h($order['Order']['order_price_tax']).' '.__('zł'); ?> </td>
                </tr>
                <tr>
                  <td colspan="2" class="text-right">
                    <small><?php echo __('w tym VAT'); ?></small>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <br/>
    </div>
  </div>
  <!--/row-->
  
  <div class="row">
    <div class="col-lg-12 clearfix">
      <ul class="pager">
        <li class="previous pull-right">
          <?php echo $this->Html->link('<i class="fa fa-home"></i> '.__('Powrót do sklepu'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Powrót do sklepu'), 'escape' => false)); ?>
        </li>
        <li class="next pull-left">
          <?php echo $this->Html->link('&larr; '.__('Historia zamówień'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'orders'), array('title' => __('Historia zamówień'), 'escape' => false)); ?>
          <?php // echo $this->Html->link('<i class="fa fa-home"></i> '.__('Historia zamówień'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'orders'), array('title' => __('Historia zamówień'), 'escape' => false)); ?>
        </li>
      </ul>
    </div>
  </div>

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->