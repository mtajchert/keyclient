<div class="container main-container headerOffset">
    <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
      array(h($page['Page']['title']), $this->here),
    ))); ?>  
  
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-12">
            <div class="row userInfo">
                <div class="col-xs-12 col-sm-12">
                    <h1 class=" text-left border-title"> <?php echo h($page['Page']['title']); ?> </h1>
                    <?php echo $page['Page']['content']; ?>
                </div>
            </div>
            <!--/row end-->

        </div>
        
        <div class="col-lg-9 col-md-8 col-sm-12">
        	<div id="googleMap" style="width:100%;height:380px;"></div>
        </div>
    </div>
    <!--/row-->

    <div style="clear:both"></div>
</div>

<!-- /wrapper -->

<script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script>
function initialize() {

  var latlng = new google.maps.LatLng(51.922036, 15.508236);
  var options = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    navigationControl: true, navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
    mapTypeControl: false, mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR },
    scaleControl: true
  };
  var map = new google.maps.Map(document.getElementById("googleMap"), options);
    var marker = new google.maps.Marker({
    position: latlng, 
    map: map, 
    title: ""
  });
    
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>