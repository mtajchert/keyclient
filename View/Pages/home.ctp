<div class="container main-container headerOffset globalPaddingBottom">
	<div class="row">
    <?php echo $this->Session->flash('payment_process'); ?>
  </div>
  <?php echo $this->element('slider'); ?>
  
  
  <?php if (isset($page_home_under_slider) && !empty($page_home_under_slider)): ?>
    <div style="clear:both;height: 10px;"></div>
    <div class="row">
      <?php foreach ($page_home_under_slider as $item): ?>
        <?php echo $this->element('page_home', array('page' => $item)); ?>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
    
  <div style="clear:both;height: 20px;"></div>
  <?php echo $this->element('main_category', array('pages' => isset($page_home_main_categories) ? $page_home_main_categories : null)); ?>  
    
</div>
<!-- /main container -->

<?php if (isset($promotions) && !empty($promotions)): ?>
  <?php echo $this->element('home_promotions', array('pages' => isset($page_home_promotions) ? $page_home_promotions : null)); ?>
  <div class="container"><div class="row"><div class="col-lg-12"><br/><br/></div></div></div>
<?php endif; ?>

<?php if (isset($our_hits) && !empty($our_hits)): ?>
  <?php echo $this->element('home_our_hits', array('pages' => isset($page_home_our_hits) ? $page_home_our_hits : null)); ?>
  <div class="container"><div class="row"><div class="col-lg-12"><br/><br/></div></div></div>
<?php endif; ?>

<div class="w100 sectionCategory morePost row featuredPostContainer style2 globalPaddingTop no-margin">
  <div class="container">
    <div class="col-lg-12 text-center partners_logos">
      <?php echo $this->Html->link($this->Html->image(CLIENT.'.partner_logo_1.png', array('class' => 'img-responsive')), 'http://yanosik.pl', array('rel' => 'nofollow','escape' => false)); ?>
      <?php echo $this->Html->link($this->Html->image(CLIENT.'.alca-logo.png', array('style' => 'max-height: 50px;', 'class' => 'img-responsive')), 'http://alca.com.pl', array('rel' => 'nofollow','escape' => false)); ?>
      <?php echo $this->Html->link($this->Html->image(CLIENT.'.hayner.png', array('style' => 'max-height: 50px;', 'class' => 'img-responsive')), 'http://alca.com.pl', array('rel' => 'nofollow','escape' => false)); ?>
    </div>
    <div style="clear:both"></div>
  </div>
</div>

<div class="container"><div class="row"><div class="col-lg-12"><br/><br/></div></div></div>

<?php if (isset($page_home_before_footer) && !empty($page_home_before_footer)): ?>
<div class="container">
  <div class="row">
  <?php for ($i = 0; $i < count($page_home_before_footer); $i++): ?>
    <?php $page = $page_home_before_footer[$i]; ?>
    <?php if ($i % 3 == 0 && $i > 0): ?></div><div class="row">&nbsp;<br/></div><div class="row"><?php endif; ?>
    <div class="col-sm-4 col-xs-12">
      <div class="boxes-title-1 <?php echo $i == 0 ? 'boxes-title-page-home boxes-title-page-home-color' : ''; ?>"><span><?php echo h($page['Page']['title']); ?></span></div>
      <?php echo $page['Page']['content']; ?>
    </div>
  <?php endfor; ?>
  </div>
</div>
<?php endif; ?>


<div class="container"><div class="row"><div class="col-lg-12"><br/><br/></div></div></div>
