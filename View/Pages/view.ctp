<div class="container main-container headerOffset">
    <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
      array(h($page['Page']['title']), $this->here),
    ))); ?>  
  
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row userInfo">
                <div class="col-xs-12 col-sm-12">
                    <h1 class=" text-left border-title"> <?php echo h($page['Page']['title']); ?> </h1>
                    <?php echo $page['Page']['content']; ?>
                </div>
            </div>
            <!--/row end-->

        </div>
    </div>
    <!--/row-->

    <div style="clear:both"></div>
</div>

<!-- /wrapper -->