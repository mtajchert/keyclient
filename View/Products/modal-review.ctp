<?php $this->request->data = $newProductReview; ?>

<?php if ($this->Session->check('Message.product_review_error')): ?>
  <script>
    $(document).ready(function () {
      $('#modal-review').modal('show');
    });
  </script>
<?php endif; ?>

<!-- Modal review start -->
<div class="modal fade" id="modal-review" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button style="opacity: 1;margin-top:10px;" type="button" class="close" data-dismiss="modal" aria-hidden="true">
        	<span style="color: white;" class="glyphicon glyphicon-remove"></span>
        </button>
        <h3 class="modal-title-site text-center"> <?php echo __('Napisz opinię produktu'); ?> </h3>
      </div>
      <div class="modal-body">
        <div class="row">
          <?php echo $this->Session->flash('product_review_error'); ?>
        </div>
        <?php echo $this->Form->create('ProductReview'); ?>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('rate') ? 'has-error' : ''; ?>">
                <label> <?php echo __('Jak oceniasz ten produkt?'); ?> </label> <br>
                <div class="rating-here">
                  <input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" data-fractions="1" name="data[ProductReview][rate]" value="<?php echo isset($this->data['ProductReview']['rate']) ? $this->data['ProductReview']['rate'] : 0; ?>" />
                </div>
                <?php echo $this->Form->isFieldError('rate') ? $this->Form->error('rate', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('author_name') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('author_name', array('label' => 'Twoje imię:', 'placeholder' => __('Wpisz imię'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group <?php echo $this->Form->isFieldError('review') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('review', array('label' => 'Opinia:', 'placeholder' => __('Wpisz opinię'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'rows' => 3)); ?>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-dark"><?php echo __('Zapisz ocenę i opinię'); ?></button>
        <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal review -->

