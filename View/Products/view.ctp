<?php
$options = [];
$option_price = null;
foreach ($product['ProductsProductsOption'] as $option) {
  if (!isset($options[$option['product_option_id']])) {
    $options[$option['product_option_id']] = array(
      'id' => $option['product_option_id'],
      'name' => $option['ProductOption']['name'],
      'values' => []
    );
  }
  $options[$option['product_option_id']]['values'][] = array(
    'option_id' => $option['id'],
    'id' => $option['ProductOptionValue']['id'],
    'name' => $option['ProductOptionValue']['name'],
    'change_price_tax' => $option['change_price_tax']
  );
  
  if ($option['id'] == $optionId) {
    $option_price = $option['change_price_tax'];
  }
}
?>

<div class="container main-container headerOffset" itemscope itemtype="http://schema.org/Product">
  <?php $breadcrumbs = array(); ?>
  <?php for ($i = 0; $i < count($categoryPath); $i++): ?>
    <?php $breadcrumbs[] = array(h($categoryPath[$i]['Category']['name']), $this->App->getCategoryViewUrl($categoryPath[$i])); ?>
  <?php endfor; ?>
  <?php $breadcrumbs[] = array(h($product['Product']['name']), $this->here); ?>
  
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => $breadcrumbs)); ?>
  
  <div class="row transitionfx">  
    <?php echo $this->Form->input('Product.id', array('type' => 'hidden', 'value' => $product['Product']['id'])); ?>
     
		<!-- top column -->
		<div class="col-xs-12">
			<div class="row">
        <?php echo $this->Session->flash('product_review_success'); ?>
      </div>
      <h1 class="product-title" itemprop="name"> <?php echo h($product['Product']['name']); ?></h1>
      <h2 style="padding: 0px;"><?php echo h($product['Product']['seo_url']); ?></h2>
      <div class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <p>
          <?php $avg_rate = round($product['Product']['average_rate'], 0); ?>
          <span><i class="fa <?php echo $avg_rate < 1 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
          <span><i class="fa <?php echo $avg_rate < 2 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
          <span><i class="fa <?php echo $avg_rate < 3 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
          <span><i class="fa <?php echo $avg_rate < 4 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
          <span><i class="fa <?php echo $avg_rate < 5 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
          <span class="ratingInfo"> <span> / </span>
          <a data-target="#modal-review" data-toggle="modal"> <?php echo __('Napisz opinię'); ?> </a> </span>
        </p>
        <span class="hide">
          <span itemprop="ratingValue"><?php echo h($product['Product']['average_rate']); ?></span> na podstawie <span itemprop="reviewCount"> <?php echo h($product['Product']['review_count']); ?> </span> opinii
        </span>
      </div>
		</div>
		<div style="clear:both;"></div>
		<hr style="margin-top: 0px;" />
		
    <!-- left column -->
    <div class="col-lg-3 col-md-3 col-sm-12">      
      <div class="main-image-container">
        <?php if (count($product['ProductsImage']) > 0): ?>
          <?php $alt = empty($product['ProductsImage'][0]['description']) ? $product['Product']['name'] : $product['ProductsImage'][0]['description']; ?>
          <?php echo $this->Html->link($this->App->showProductsImage($product['ProductsImage'][0]['image'], 'ORIGIN', array('class' => 'center-block img-responsive img-maxheight', 'alt' => $alt, 'itemprop' => 'image')), '/'.CLIENT.'/products/origin/'.$product['ProductsImage'][0]['image'], array('escape' => false, 'class' => 'colorbox zoomThumbLink','title' => __($product['Product']['name']))); ?>
        <?php endif; ?>
      </div>
      <?php if (count($product['ProductsImage']) > 0): ?>
        <div class="zoomThumb">
          <?php unset($product['ProductsImage'][0]); ?>
          <?php foreach ($product['ProductsImage'] as $productImage): ?>  
            <?php $alt = empty($productImage['description']) ? $product['Product']['name'] : $productImage['description']; ?>
            <?php echo $this->Html->link($this->App->showProductsImage($productImage['image'], 'ORIGIN', array('alt' => $alt, 'itemprop' => 'image')), '/'.CLIENT.'/products/origin/'.$productImage['image'], array('escape' => false, 'class' => 'colorbox zoomThumbLink','title' => __($product['Product']['name']))); ?>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
    <!--/ left column end -->
    
    <!-- center column -->
    <div id="product-description" class="col-lg-6 col-md-6 col-sm-12">
    	<?php if (!empty($product['Product']['short_description'])): ?> 
        <div class="details-description" itemprop="description">
          <?php echo __($product['Product']['short_description']); ?>
        </div>
      <?php endif; ?>
      <?php echo __($product['Product']['description']); ?>
      
      <?php if (!empty($brands)): ?>
      <a class="btn btn-default col-xs-12" data-toggle="collapse" data-target="#table-wipers-rel">Pokaż pasujące modele aut &raquo;</a>
      <table id="table-wipers-rel" class="table table-striped table-wipers-rel collapse">
      	<tr>
      		<th>Producent</th>
      		<th>Modele</th>
      	</tr>
      	<?php $usedBrands = array(); ?> 
      	<?php foreach ($brands as $brand): ?>
      		<?php if (in_array($brand['ViewWiper']['id'], $usedBrands)) continue; ?>
      		<?php $usedBrands[] = $brand['ViewWiper']['id']; ?>
      		<tr>
      			<td><h2><?php echo "{$brand['ViewWiper']['brand_name']}"; ?></h2></td>
      			<?php $types = Hash::extract($brands, '{n}.ViewWiper[id='.$brand['ViewWiper']['id'].'].type_name');	?>
      			
      			<td><h2><?php echo implode($types, ', '); ?></h2></td>
      		</tr>
      	<?php endforeach;?>
      </table>
      <?php endif; ?>
    </div>
    <!-- end conter column -->
    
    <!-- right column -->
    <div class="col-lg-3 col-md-3 col-sm-12">
      <span class="hide" itemprop="brand"><?php echo isset($product['Product']['Manufacturer']['name']) ? h($product['Product']['Manufacturer']['name']) : ''; ?></span>
      
      <div class="product-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <meta itemprop="priceCurrency" content="PLN" />
        <input type="hidden" id="base_price" value="<?php echo __($product['Product']['price_tax']); ?>" />
        <span class="price-sales"> <span itemprop="price">
          <?php echo h(is_null($option_price) ? $product['Product']['price_tax'] : $option_price); ?>
        </span> <?php echo __('zł'); ?> </span>
        <?php /* <span class="price-standard">$95</span> */ ?>
      </div>

      <div class="productFilter productFilterLook2">
        <div class="row">
          <div class="form-group col-xs-12 col-sm-6">
            <label for="quantity">Podaj ilość</label>
            <input type="number" class="form-control" id="quantity-input" placeholder="Ilość" value="1" />
          </div>
        </div>
        <?php if (!empty($product['ProductsProductsOption'])): ?>
          
          <div class="row productOptions">
            <div class="form-group col-xs-12 col-sm-6">
              <label for="dataProductOption<?php echo h($option['id']); ?>">Wybierz opcje:</label>
              <?php foreach ($options as $option): ?>
                <div class="filterBox">
                  <select name="data[ProductOption][<?php echo h($option['id']); ?>]" placeholder="<?php echo h($option['name']); ?>" onchange="App.Products.changeOption();">
                    <option value="0" selected disabled><?php echo h($option['name']); ?></option>
                    <?php foreach ($option['values'] as $value): ?>
                      <option value="<?php echo h($value['id']); ?>" data-change_price_tax="<?php echo h($value['change_price_tax']); ?>" <?php echo $value['option_id'] == $optionId ? 'selected="selected"' : '' ?>><?php echo h($value['name']); ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        <?php endif; ?>
      </div>
      
      <div class="cart-actions">
        <div class="addto">
          <button type="button" title="Dodaj do koszyka" class="button btn-cart cart first" onclick="App.Cart.addProduct();">Dodaj do koszyka</button>
          <div style="clear:both"></div>
          <h3 class="incaps"><i class="fa fa fa-check-circle-o color-in"></i> Dostępny</h3>
          <?php echo $this->Html->image('/'.CLIENT.'/img/przelewy24_8.png', array('class' => 'img-responsive center-block', 'fullbase' => true)); ?>
        </div>
      </div>
      <!--/.cart-actions-->
      <div style="clear:both"></div>
      <?php /*<div class="product-share clearfix">
        <p> UDOSTĘPNIJ </p>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <div class="addthis_sharing_toolbox"></div>
        <div class="socialIcon">
          <a href="#"> <i class="fa fa-facebook"></i></a>
          <a href="#"> <i class="fa fa-twitter"></i></a>
          <a href="#"> <i class="fa fa-google-plus"></i></a>
          <a href="#"> <i class="fa fa-pinterest"></i></a>
        </div>
      </div> */ ?>
    <!--/.product-share-->
    </div>
    <!--/ right column end -->
  </div>
  <!--/.row-->
  <div style="clear:both"></div>
  
  <div class="row transitionfx"> 
  	<div class="col-xs-12">
      <div class="product-tab w100 clearfix">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#reviews"><?php echo __('Opinie i oceny'); ?></a></li>
        </ul>        
        <!-- Tab panes -->
        <div class="tab-content">
          <div id="reviews" class="tab-pane active">
            <?php if (empty($product['ProductReview'])): ?>
              <p><?php echo __('Brak opinii i ocen.'); ?>
            <?php else: ?>
              <?php foreach ($product['ProductReview'] as $productReview): ?>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="rating">
                      <p>
                        <?php echo strtotime($productReview['created']) > 0 ? h($this->Time->format($productReview['created'], '%Y-%m-%d %H:%M')) : '-'; ?>
                        &nbsp;
                        <?php $avg_rate = round($productReview['rate'], 0); ?>
                        <span><i class="fa <?php echo $avg_rate < 1 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
                        <span><i class="fa <?php echo $avg_rate < 2 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
                        <span><i class="fa <?php echo $avg_rate < 3 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
                        <span><i class="fa <?php echo $avg_rate < 4 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
                        <span><i class="fa <?php echo $avg_rate < 5 ? 'fa-star-o' : 'fa-star'; ?>"></i></span>
                        <span> &nbsp; <?php echo h($productReview['author_name']); ?> </span>
                      </p>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <p><?php echo h($productReview['review']); ?></p>
                  </div>
                </div>
                <hr/>
              <?php endforeach; ?>
            <?php endif; ?>
          </div>
        </div>
        <!-- /.tab content -->
      </div>
      <!--/.product-tab-->
  	</div>
  </div>
  
  <?php if (!empty($product['Product']['youtube_url'])): ?>
    <div class="gap"></div>
    
    <div class="row transitionfx"> 
    	<div class="col-xs-12">
        <div class="product-tab w100 clearfix">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#related-products"><?php echo __('Powiązany film'); ?></a></li>
          </ul>        
          <!-- Tab panes -->
          <div class="tab-content">
          	<?php $parts = parse_url($product['Product']['youtube_url'],PHP_URL_QUERY);?>
          	<iframe width="100%" height="515" src="https://www.youtube.com/embed/<?php echo substr($parts, 2); ?>" frameborder="0" allowfullscreen></iframe>
          	
        	</div>
        </div>
      </div>
    </div>
  <?php endif; ?>
  
  <div class="gap"></div>
  
  <div class="row transitionfx"> 
  	<div class="col-xs-12">
      <div class="product-tab w100 clearfix">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#related-products"><?php echo __('Inni klienci kupili również'); ?></a></li>
        </ul>        
        <!-- Tab panes -->
        <div class="tab-content">
          <div id="related-products" class="tab-pane active">
          	<!--/.productFilter-->
            <div class="row  categoryProduct xsResponse clearfix">
              <?php foreach ($products as $product): ?>
                <div class="item col-lg-4 col-md-4 col-sm-12 col-xs-12" style="height: 470px;" itemscope itemtype="http://schema.org/Product">
                  <div class="product">
                    <div class="image">
                      <!--<div class="quickview">
                        <a data-toggle="modal" data-target="#product-details-modal" class="btn btn-xs  btn-quickview" title="Quick View"> Quick View </a>
                      </div>-->
                      <a href="<?php echo $this->App->getProductViewUrl($product); ?>">
                        <?php if (isset($product['ProductsImage'][0]['image']) && !empty($product['ProductsImage'][0]['image'])): ?>
                          <?php echo $this->App->showProductsImage($product['ProductsImage'][0]['image'], 'THUMB', array('class' => 'img-responsive', 'alt' => h($product['Product']['name']), 'itemprop' => 'image')); ?>
                        <?php endif; ?>
                      </a>
                      <?php if ($this->Product->isPromotionActive($product) && !empty($product['Product']['promotion_info'])): ?>
                      <div class="promotion">
                        <span class="discount"><?php echo h($product['Product']['promotion_info']); ?></span>
                      </div>
                      <?php endif; ?>
                    </div>
                    <div class="description">
                      <h3 itemprop="name"><a href="<?php echo $this->App->getProductViewUrl($product); ?>"><?php echo h($product['Product']['name']); ?></a></h3>
                      <p><?php echo h(mb_substr(strip_tags($product['Product']['short_description']), 0, 50)); ?></p>
                    </div>
                    <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                      <meta itemprop="priceCurrency" content="PLN" />
                      <span><span itemprop="price"><?php echo h($product['Product']['price_tax']); ?></span><?php echo ' '.__('zł'); ?></span>
                      <?php if ($product['Product']['old_price'] > 0): ?>
                        <span class="old-price"><?php echo h($product['Product']['old_price']).' '.__('zł'); ?></span>
                      <?php endif; ?>
                    </div>
                    <div class="action-control">
                      <a class="btn btn-primary" href="javascript:void(0);" onclick="App.Cart.addProductQuick(<?php echo h($product['Product']['id']); ?>, this);">
                        <span class="add2cart">
                          <i class="glyphicon glyphicon-shopping-cart"> </i> <?php echo __('Do koszyka'); ?>
                        </span>
                      </a>
                    </div>
                  </div>
                </div>
                <!--/.item-->
              <?php endforeach; ?>
                <p>&nbsp</p>
            </div>
            <!--/.categoryProduct || product content end-->
          </div>
        </div>
        <!-- /.tab content -->
      </div>
      <!--/.product-tab-->
  	</div>
  </div>
  
</div>
<div class="gap"></div>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5280d8387b019a37" async="async"></script>


<?php echo $this->element('../Products/modal-review', array('product' => $product, 'newProductReview' => $newProductReview)); ?>