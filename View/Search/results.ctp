<div class="container search_result headerOffset">
  <div class="row clearfix">&nbsp;</div>
  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-xs-12">
      <h3><?php echo __('Wyniki wyszukiwania "%s"', $query); ?></h3>
    </div>
  </div>
  
  <div class="productFilter clearfix">
    <div class="pull-right ">
      <div class="change-order pull-right">
        <div style="display:none;">
          <?php echo $this->Paginator->sort('al_default', null, array('id' => 'sortLink-al_default', 'direction' => 'asc', 'lock' => true)); ?>
          <?php echo $this->Paginator->sort('al_price', null, array('id' => 'sortLink-al_price', 'direction' => 'asc', 'lock' => true)); ?>
          <?php echo $this->Paginator->sort('al_price-desc', null, array('id' => 'sortLink-al_price-desc', 'direction' => 'desc', 'lock' => true)); ?>
          <span><?php echo $this->Paginator->sortKey('ProductsCategory'); ?></span>
        </div>
        <select name="orderby" class="form-control" style="width:200px;" onchange="App.Categories.changeOrder(this);">

          <option value="al_default" <?php echo $sort == 'al_default' ? 'selected="selected"' : ''; ?>><?php echo __('Domyślne sortowanie'); ?></option>
          <option value="al_price" <?php echo $sort == 'al_price' ? 'selected="selected"' : ''; ?>><?php echo __('Najpierw najtańsze'); ?></option>
          <option value="al_price-desc" <?php echo $sort == 'al_price-desc' ? 'selected="selected"' : ''; ?>><?php echo __('Najpierw najdroższe'); ?></option>
        </select>
      </div>
      <div class="change-view pull-right">
        <a class="grid-view" title="<?php echo __('Kafelki'); ?>" href="#"> <i class="fa fa-th-large"></i> </a>
        <a class="list-view " title="<?php echo __('Lista'); ?>" href="#"><i class="fa fa-th-list"></i></a>
      </div>
    </div>
  </div>
  <!--/.productFilter-->
  <div class="row  categoryProduct xsResponse clearfix">
    <?php if (empty($products)): ?>
      <p class="text-center"><?php echo __('Brak wyników wyszukiwania'); ?></p>
      <p>&nbsp;</p>
    <?php else: ?>
      <?php foreach ($products as $product): ?>
        <div class="item col-lg-3 col-md-3 col-sm-4 col-xs-6" style="height: 470px;">
          <div class="product">
            <div class="image">
              <!--<div class="quickview">
                <a data-toggle="modal" data-target="#product-details-modal" class="btn btn-xs  btn-quickview" title="Quick View"> Quick View </a>
              </div>-->
              <a href="<?php echo $this->App->getProductViewUrl($product); ?>">
                <?php if (isset($product['ProductsImage'][0]['image']) && !empty($product['ProductsImage'][0]['image'])): ?>
                  <?php echo $this->App->showProductsImage($product['ProductsImage'][0]['image'], 'THUMB', array('class' => 'img-responsive', 'alt' => h($product['Product']['name']))); ?>
                <?php endif; ?>
              </a>
              <?php if ($this->Product->isPromotionActive($product) && !empty($product['Product']['promotion_info'])): ?>
                <div class="promotion">
                  <span class="discount"><?php echo h($product['Product']['promotion_info']); ?></span>
                </div>
              <?php endif; ?>
            </div>
            <div class="description">
              <h4>
                <a href="<?php echo $this->App->getProductViewUrl($product); ?>">
                  <?php echo h($product['Product']['name']); ?>
                </a>
              </h4>

              <div class="grid-description">
                <p><?php echo h(mb_substr(strip_tags($product['Product']['short_description']), 0, 50)); ?></p>
              </div>
              <div class="list-description">
                <p><?php echo h(strip_tags($product['Product']['short_description'])); ?></p>
              </div>
              <!--<span class="size">XL / XXL / S </span>-->
            </div>
            <div class="price">
              <span><?php echo h($product['Product']['price_tax']).' '.__('zł'); ?></span>
              <?php if ($product['Product']['old_price'] > 0): ?>
                <span class="old-price"><?php echo h($product['Product']['old_price']).' '.__('zł'); ?></span>
              <?php endif; ?>
            </div>
            <div class="action-control">
              <a class="btn btn-primary" href="javascript:void(0);" onclick="App.Cart.addProductQuick(<?php echo h($product['Product']['id']); ?>, this);">
                <span class="add2cart">
                  <i class="glyphicon glyphicon-shopping-cart"> </i> <?php echo __('Do koszyka'); ?>
                </span>
              </a>
            </div>
          </div>
        </div>
        <!--/.item-->
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
  <!--/.categoryProduct || product content end-->

  <div class="clearFix">&nbsp;</div>
  
  <div class="w100 categoryFooter">
    <div class="pagination pull-left no-margin-top">
      <ul class="pagination no-margin-top">
        <?php
        $this->Paginator->options['url'] = array('plugin' => CLIENT, 'controller' => 'Search', 'action' => 'index', 'q' => $query);
        echo $this->Paginator->prev('«', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
        echo $this->Paginator->next('»', array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
      </ul>
    </div>
    <div class="pull-right pull-right col-sm-4 col-xs-12 no-padding text-right text-left-xs">
      <p><?php echo $this->Paginator->counter(array('format' => __('Produkty {:start} - {:end} z {:count}'))); ?></p>
    </div>
  </div>
  <!--/.categoryFooter-->
</div>