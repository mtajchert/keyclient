<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Moje konto'), $this->here)
  ))); ?>
  
  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner"><span><i class="fa fa-unlock-alt"></i>
        <?php if (isset($pages[0])): ?>
          <?php echo h($pages[0]['Page']['title']); ?>
        <?php else: ?>
          <?php echo __('Moje konto'); ?>
        <?php endif; ?>
      </span></h1>

      <div class="row userInfo">
        <div class="col-xs-12 col-sm-12">
          <?php if (isset($pages[0])): ?>
            <?php foreach ($pages as $key => $page): ?>
              <?php if ($key > 0): ?>
                <h2 class="block-title-2"><span><?php echo h($page['Page']['title']); ?></span></h2>
              <?php endif; ?>
              <?php echo $page['Page']['content']; ?>
            <?php endforeach; ?>
          <?php endif; ?>
          <br/>
          
          <ul class="myAccountList row">
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
              <div class="thumbnail equalheight">
                  <?php echo $this->Html->link('<i class="fa fa-calendar"></i>'.__('Historia zamówień'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'orders'), array('title' => __('Historia zamówień'), 'escape' => false)); ?>
              </div>
            </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
              <div class="thumbnail equalheight">
                <?php echo $this->Html->link('<i class="fa fa-map-marker"></i>'.__('Adresy'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'addresses'), array('title' => __('Adresy'), 'escape' => false)); ?>
              </div>
            </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
              <div class="thumbnail equalheight">
                <?php echo $this->Html->link('<i class="fa fa-edit"></i>'.__('Dodaj adres'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'create_address'), array('title' => __('Dodaj adres'), 'escape' => false)); ?>
              </div>
            </li>
            <?php /*
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
              <div class="thumbnail equalheight">
                  <?php echo $this->Html->link('<i class="fa fa-cog"></i>'.__('Ustawienia konta'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'settings'), array('title' => __('Ustawienia konta'), 'escape' => false)); ?>
              </div>
            </li>
            */ ?>
            <!--<li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
              <div class="thumbnail equalheight"><a title="My wishlists" href="wishlist.html"><i
                    class="fa fa-heart"></i> <?php echo __('Ulubione'); ?> </a></div>
            </li>-->
          </ul>
          <div class="clear clearfix"></div>
        </div>
      </div>
      <!--/row end-->

    </div>
  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /wrapper -->