<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Moje konto'), array('controller' => 'User', 'action' => 'account')),
    array(__('Dodaj adres'), $this->here),
  ))); ?>

  <?php echo $this->Session->flash(); ?>

  <div class="row">

    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner"><span><i class="fa fa-map-marker"></i>
          <?php if (isset($pages[0])): ?>
            <?php echo h($pages[0]['Page']['title']); ?>
          <?php else: ?>
            <?php echo __('Dodaj adres'); ?>
          <?php endif; ?>
        </span></h1>

      <div class="row userInfo">
        <div class="col-lg-12">
          <?php if (isset($pages[0])): ?>
            <?php foreach ($pages as $key => $page): ?>
              <?php if ($key > 0): ?>
                <h2 class="block-title-2"><span><?php echo h($page['Page']['title']); ?></span></h2>
              <?php endif; ?>
              <?php echo $page['Page']['content']; ?>
            <?php endforeach; ?>
          <?php endif; ?>
          <br/>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-7">
          <?php echo $this->Form->create('UserAddress'); ?>
            <div class="row">
              <div class="col-xs-6">
                <div class="form-group required <?php echo $this->Form->isFieldError('first_name') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('first_name', array('label' => __('Imię').': <sup>*</sup>', 'placeholder' => __('Wpisz imię'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group required <?php echo $this->Form->isFieldError('last_name') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('last_name', array('label' => __('Nazwisko').': <sup>*</sup>', 'placeholder' => __('Wpisz nazwisko'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group required <?php echo $this->Form->isFieldError('company') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('company', array('label' => __('Nazwa firmy').':', 'placeholder' => __('Wpisz nazwę firmy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group required <?php echo $this->Form->isFieldError('nip') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('nip', array('label' => __('Numer NIP firmy').':', 'placeholder' => __('Wpisz numer NIP firmy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group required <?php echo $this->Form->isFieldError('street_address') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('street_address', array('label' => __('Ulica i nr domu').': <sup>*</sup>', 'placeholder' => __('Wpisz ulicę i nr domu'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-5">
                <div class="form-group required <?php echo $this->Form->isFieldError('post_code') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('post_code', array('label' => __('Kod pocztowy').': <sup>*</sup>', 'placeholder' => __('Wpisz kod pocztowy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
              <div class="col-xs-7">
                <div class="form-group required <?php echo $this->Form->isFieldError('city') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('city', array('label' => __('Miejscowość').': <sup>*</sup>', 'placeholder' => __('Wpisz miejscowość'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6">
                <div class="form-group required <?php echo $this->Form->isFieldError('country_id') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('country_id', array('label' => __('Kraj').': <sup>*</sup>', 'empty' => __('Wybierz kraj'), 'default' => $defaultCountryId, 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'onchange' => 'App.Users.changeCountry($(this).val());')); ?>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group <?php echo $this->Form->isFieldError('zone_id') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('zone_id', array('label' => __('Województwo').':', 'empty' => __('Wybierz województwo'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group required <?php echo $this->Form->isFieldError('phone') ? 'has-error' : ''; ?>">
                  <div>
                    <?php echo $this->Form->input('phone', array('label' => __('Telefon').': <sup>*</sup>', 'placeholder' => __('Wpisz nr telefonu'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group required <?php echo $this->Form->isFieldError('default_billing') ? 'has-error' : ''; ?>">
                  <label class="checkbox-inline c-checkbox">
                    <?php echo $this->Form->input('default_billing', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                    <?php echo __('domyślny adres rozliczeniowy'); ?>
                  </label>
                </div>
              </div>
              <div class="col-xs-12">
                <div class="form-group required <?php echo $this->Form->isFieldError('default_shipping') ? 'has-error' : ''; ?>">
                  <label class="checkbox-inline c-checkbox">
                    <?php echo $this->Form->input('default_shipping', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                    <?php echo __('domyślny adres dostawy'); ?>
                  </label>
                </div>
              </div>
            </div>
          
            <div class="col-lg-12 col-xs-12 clearfix">
              <button type="submit" class="btn   btn-primary"><i class="fa fa-map-marker"></i> <?php echo __('Zapisz adres'); ?></button>
            </div>
          <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
          <!--/row end-->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-5">
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 clearfix">
          <ul class="pager">
            <li class="previous pull-right">
              <?php echo $this->Html->link('<i class="fa fa-home"></i> '.__('Powrót do sklepu'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Powrót do sklepu'), 'escape' => false)); ?>
            </li>
            <li class="next pull-left">
              <?php echo $this->Html->link('&larr; '.__('Adresy'), array('controller' => 'User', 'action' => 'addresses'), array('title' => __('Adresy'), 'escape' => false)); ?>
            </li>
          </ul>
        </div>
      </div>
      <!--/row end-->
    </div>

    <div class="col-lg-3 col-md-3 col-sm-5"></div>

  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->