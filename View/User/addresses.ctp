<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Moje konto'), array('controller' => 'User', 'action' => 'account')),
    array(__('Adresy'), $this->here),
  ))); ?>
  
  <?php echo $this->Session->flash(); ?>


  <div class="row">

    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner"><span><i class="fa fa-map-marker"></i>
          <?php if (isset($pages[0])): ?>
            <?php echo h($pages[0]['Page']['title']); ?>
          <?php else: ?>
            <?php echo __('Adresy'); ?>
          <?php endif; ?>
        </span></h1>

      <div class="row userInfo">
        <div class="col-lg-12">
          <?php if (isset($pages[0])): ?>
            <?php foreach ($pages as $key => $page): ?>
              <?php if ($key > 0): ?>
                <h2 class="block-title-2"><span><?php echo h($page['Page']['title']); ?></span></h2>
              <?php endif; ?>
              <?php echo $page['Page']['content']; ?>
            <?php endforeach; ?>
          <?php endif; ?>
          <br/>
        </div>
      </div>

      <div class="row">
        <div class="w100 clearfix">
          <?php if (isset($addresses) && !empty($addresses)): ?>
            <?php foreach ($addresses as $address): ?>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                      <!--<strong><?php echo h($address['UserAddress']['alias']); ?></strong>-->
                      <?php if ($address['UserAddress']['default_billing']): ?>
                        <small> <?php echo __('domyślny adres rozliczeniowy'); ?> </small>
                      <?php endif; ?>
                      <?php if ($address['UserAddress']['default_shipping']): ?>
                        <small> <?php echo ($address['UserAddress']['default_billing'] ? ', ' : '').__('domyślny adres dostawy'); ?> </small>
                      <?php endif; ?>
                    </h3>
                  </div>
                  <div class="panel-body">
                    <ul>
                      <li><span class="address-name">
                          <strong><?php echo h($address['UserAddress']['first_name'].' '.$address['UserAddress']['last_name']); ?></strong></span>
                      </li>
                      <?php if ($address['UserAddress']['is_company']): ?>
                        <li><span class="address-company"> <?php echo h($address['UserAddress']['company']); ?> </span></li>
                      <?php endif; ?>
                      <li><span class="address-line1"> <?php echo h($address['UserAddress']['street_address']); ?> </span></li>
                      <li><span class="address-line1"> <?php echo h($address['UserAddress']['post_code'].' '.$address['UserAddress']['city']); ?> </span></li>
                      <li><span class="address-line2"> <?php echo h($address['Country']['name'].(isset($address['Zone']['name']) && !empty($address['Zone']['name']) ? ', '.$address['Zone']['name'] : '')); ?> </span></li>
                      <?php if (!empty($address['UserAddress']['phone'])): ?>
                      <li><span> Tel.: <?php echo h($address['UserAddress']['phone']); ?> </span></li>
                      <?php endif; ?>
                    </ul>
                  </div>
                  <div class="panel-footer panel-footer-address">
                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> '.__('Edytuj'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'edit_address', $address['UserAddress']['id']), array('title' => __('Edytuj'), 'escape' => false, 'class' => 'btn btn-sm btn-success')); ?>
                    &nbsp;
                    <?php if (!$address['UserAddress']['default_billing'] && !$address['UserAddress']['default_shipping']): ?>
                      <?php echo $this->Html->link('<i class="fa fa-minus-circle"></i> '.__('Usuń'), 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Users.deleteAddress(\''.h($address['UserAddress']['alias']).'\', \''.$this->Html->url(array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'delete_address', $address['UserAddress']['id'])).'\')', 'class' => 'btn btn-sm btn-danger')); ?>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          <?php else: ?>
            <p><?php echo __('Brak adresów do wyświetlenia'); ?></p>
          <?php endif; ?>
        </div>
        <!--/.w100-->

        <div class="col-lg-12 clearfix">
          <?php echo $this->Html->link('<i class="fa fa-plus-circle"></i> '.__('Dodaj nowy adres'), array('plugin' => CLIENT, 'controller' => 'User', 'action' => 'create_address'), array('title' => __('Dodaj nowy adres'), 'escape' => false, 'class' => 'btn btn-primary')); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 clearfix">
          <ul class="pager">
            <li class="previous pull-right">
              <?php echo $this->Html->link('<i class="fa fa-home"></i> '.__('Powrót do sklepu'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Powrót do sklepu'), 'escape' => false)); ?>
            </li>
            <li class="next pull-left">
              <?php echo $this->Html->link('&larr; '.__('Moje konto'), array('controller' => 'User', 'action' => 'account'), array('title' => __('Moje konto'), 'escape' => false)); ?>
            </li>
          </ul>
        </div>
      </div>
      <!--/row end-->
    </div>

  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->