<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Moje konto'), array('controller' => 'User', 'action' => 'account')),
    array(__('Historia zamówień'), $this->here),
  ))); ?>


  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner"><span><i class="fa fa-list-alt"></i>
        <?php if (isset($pages[0])): ?>
          <?php echo h($pages[0]['Page']['title']); ?>
        <?php else: ?>
          <?php echo __('Historia zamówień'); ?>
        <?php endif; ?>
      </span></h1>

      <div class="row userInfo">
        <div class="col-lg-12">
          <?php if (isset($pages[0])): ?>
            <?php foreach ($pages as $key => $page): ?>
              <?php if ($key > 0): ?>
                <h2 class="block-title-2"><span><?php echo h($page['Page']['title']); ?></span></h2>
              <?php endif; ?>
              <?php echo $page['Page']['content']; ?>
            <?php endforeach; ?>
          <?php endif; ?>
          <br/>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <table class="footable">
            <thead>
              <tr>
                <th data-class="expand"><span> <?php echo __('Nr zamówienia'); ?></span></th>
                <th data-hide="phone,tablet" data-sort-ignore="true"> <?php echo __('Ilość pozycji'); ?></th>
                <th data-hide="phone,tablet"><strong> <?php echo __('Forma płatności'); ?></strong></th>
                <th data-hide="phone,tablet"><strong> <?php echo __('Forma dostawy'); ?></strong></th>
                <th data-hide="phone,tablet"><strong></strong></th>
                <th data-hide="default"> <?php echo __('Wartość'); ?></th>
                <th data-hide="default" data-type="numeric" data-sort-initial="descending"> <?php echo __('Data złożenia'); ?></th>
                <th data-hide="phone" data-type="numeric"> <?php echo __('Status'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php if (isset($orders) && !empty($orders)): ?>
                <?php foreach ($orders as $order): ?>
                  <tr>
                    <td><?php echo h($order['Order']['number']); ?></td>
                    <td>
                      <?php echo isset($order['OrderProduct']) ? count($order['OrderProduct']) : 0; ?> 
                      <small><?php echo __('pozycja/ie'); ?></small>
                    </td>
                    <td><?php echo h($order['Payment']['name']); ?></td>
                    <td><?php echo h($order['Shipping']['name']); ?></td>
                    <td>
                      <!--<a href="order-status.html" class="btn btn-primary btn-sm"><?php echo __('szczegóły zamówienia'); ?></a>-->
                      <?php echo $this->Html->link(__('szczegóły zamówienia'), array('controller' => 'Order', 'action' => 'view', $order['Order']['id']), array('title' => __('szczegóły zamówienia'), 'class' => 'btn btn-primary btn-sm')); ?>
                    </td>
                    <td><?php echo h($order['Order']['order_price_tax']).' '.__('zł'); ?></td>
                    <td data-value="<?php echo strtotime($order['Order']['created']); ?>"><?php echo h($this->Time->format($order['Order']['created'], '%Y-%m-%d %H:%M')); ?>&nbsp;</td>
                    <td data-value="3"><span class="label <?php echo h($order['OrderStatus']['css_label_class']); ?>"><?php echo h($order['OrderStatus']['name']); ?></span>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                  <tr>
                    <td colspan="8"><?php echo __('Brak zamówień do wyświetlenia'); ?></td>
                  </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 clearfix">
          <ul class="pager">
            <li class="previous pull-right">
              <?php echo $this->Html->link('<i class="fa fa-home"></i> '.__('Powrót do sklepu'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Powrót do sklepu'), 'escape' => false)); ?>
            </li>
            <li class="next pull-left">
              <?php echo $this->Html->link('&larr; '.__('Moje konto'), array('controller' => 'User', 'action' => 'account'), array('title' => __('Moje konto'), 'escape' => false)); ?>
            </li>
          </ul>
        </div>
      </div>
      <!--/row end-->

    </div>
    <div class="col-lg-3 col-md-3 col-sm-5"></div>
  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /main-container -->