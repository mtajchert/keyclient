<div class="container main-container headerOffset">
  <?php echo $this->element('breadcrumb', array('breadcrumbs' => array(
    array(__('Moje konto'), array('controller' => 'User', 'action' => 'account')),
    array(__('Ustawienia konta'), $this->here),
  ))); ?>

  <?php echo $this->Session->flash(); ?>


  <div class="row">

    <div class="col-lg-9 col-md-9 col-sm-7">
      <h1 class="section-title-inner"><span><i class="fa fa-map-marker"></i>
          <?php if (isset($pages[0])): ?>
            <?php echo h($pages[0]['Page']['title']); ?>
          <?php else: ?>
            <?php echo __('Ustawienia konta'); ?>
          <?php endif; ?>
        </span></h1>

      <div class="row userInfo">
        <div class="col-lg-12">
          <?php if (isset($pages[0])): ?>
            <?php foreach ($pages as $key => $page): ?>
              <?php if ($key > 0): ?>
                <h2 class="block-title-2"><span><?php echo h($page['Page']['title']); ?></span></h2>
              <?php endif; ?>
              <?php echo $page['Page']['content']; ?>
            <?php endforeach; ?>
          <?php endif; ?>
          <br/>
        </div>
      </div>

      <div class="row">
        <div class="w100 clearfix">
          <?php echo $this->Form->create('Customer'); ?>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group required <?php echo $this->Form->isFieldError('first_name') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('first_name', array('label' => __('Imię').': <sup>*</sup>', 'placeholder' => __('Wpisz imię'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>

              <div class="form-group required <?php echo $this->Form->isFieldError('last_name') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('last_name', array('label' => __('Nazwisko').': <sup>*</sup>', 'placeholder' => __('Wpisz nazwisko'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>

              <div class="form-group <?php echo $this->Form->isFieldError('company') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('company', array('label' => __('Firma').':', 'placeholder' => __('Wpisz nazwę firmy'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
              
              <div class="form-group required <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('email', array('label' => __('E-mail').': <sup>*</sup>', 'placeholder' => __('Wpisz adres e-mail'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
              
              <div class="form-group <?php echo $this->Form->isFieldError('contact_phone') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('contact_phone', array('label' => __('Telefon').':', 'placeholder' => __('Wpisz nr telefonu'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password', array('label' => __('Nowe hasło').':', 'placeholder' => __('Wpisz nowe hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
              <div class="form-group <?php echo $this->Form->isFieldError('password2') ? 'has-error' : ''; ?>">
                <div>
                  <?php echo $this->Form->input('password2', array('type' => 'password', 'label' => __('Powtórz nowe hasło').':', 'placeholder' => __('Wpisz powtórnie nowe hasło'), 'div' => false, 'class' => 'form-control input', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </div>
            </div>
            <div class="col-lg-12">
              <button type="submit" class="btn   btn-primary"><i class="fa fa-save"></i> &nbsp; <?php echo __('Zapisz'); ?></button>
            </div>
          <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 clearfix">
          <ul class="pager">
            <li class="previous pull-right">
              <?php echo $this->Html->link('<i class="fa fa-home"></i> '.__('Powrót do sklepu'), array('plugin' => CLIENT, 'controller' => 'Pages', 'action' => 'home'), array('title' => __('Powrót do sklepu'), 'escape' => false)); ?>
            </li>
            <li class="next pull-left">
              <?php echo $this->Html->link('&larr; '.__('Moje konto'), array('controller' => 'User', 'action' => 'account'), array('title' => __('Moje konto'), 'escape' => false)); ?>
            </li>
          </ul>
        </div>
      </div>
      <!--/row end-->
    </div>

    <div class="col-lg-3 col-md-3 col-sm-5"></div>

  </div>
  <!--/row-->

  <div style="clear:both"></div>
</div>
<!-- /.main-container -->