(function ($) {
  App = {
    changeSubmenuImage: function(image, imageUrl) {
      if (imageUrl.length > 0) {
        image.attr('src', imageUrl);
        image.show();
      } else {
        image.hide();
      }
    }
  };
})(jQuery);

$(document).ready(function () {
  //--
});