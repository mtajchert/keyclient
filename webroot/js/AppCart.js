var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Cart = {
    
    lang: {
      warning: 'Uwaga!',
      btn_show_product_title: 'Przejdź do strony produktu',
      btn_ok_title: 'Ok',
      add_success: 'Dodano do koszyka',
      btn_back_to_shopping_title: 'Wróć do zakupów',
      btn_to_cart_title: 'Przejdź do koszyka',
      chooseCountry: 'Wybierz kraj',
      noCountryFound: 'Nie znaleziono kraju',
      chooseZone: 'Wybierz województwo',
      noZoneFound: 'Nie znaleziono województwa',
    },
    
    cartUrl: null,
    addProductUrl: null,
    deleteCartProductUrl: null,
    saveCartProductAmountsUrl: null,
    getZonesUrl: null,
    getPayments: null,
    
    onDocReady: function () {
      $(".productFilter select").each(function(){
        $(this).minimalect({placeholder: $(this).attr('placeholder')});
      });
      
      $('#UserAddress0CountryId').minimalect({
        placeholder: this.lang.chooseCountry,
        empty: this.lang.noCountryFound
      });
      $('#UserAddress0ZoneId').minimalect({
        placeholder: this.lang.chooseZone,
        empty: this.lang.noZoneFound
      });
      
      this.registrationChangeCreateAccount();
    },
    
    addProductQuick: function (productId, btn, productOptions) {
      if (typeof btn != 'undefinded') {
        this.flyToCart(btn);
      }
      
      productOptions = productOptions || null;

      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.addProductUrl,
        type: 'post',
        data: ({mode: 'quick', productId: productId, amount: 'auto', productOptions: productOptions}),
        context: this,
        success: function (data, textStatus) {
          if (data.result.success) {
            $('.cartRespons').text(data.result.cartResponse);
            $('.navbar-nav .cartMenu .dropdown-menu').html(data.result.miniCart);
            $('.navbar-cart .cartMenu').html(data.result.miniCartMobile);
            
            window.box = bootbox.dialog({
              message: data.result.message,
              title: this.lang.add_success,
              buttons: {
                back_to_shopping: {
                  label: this.lang.btn_back_to_shopping_title,
                  className: 'btn-default pull-left',
                  callback: function() {
                    bootbox.hideAll();
                  }
                },
                to_cart: {
                  label: this.lang.btn_to_cart_title,
                  className: "btn-primary",
                  callback: function () {
                    window.location.href = App.Cart.cartUrl;
                  }
                }
              },
              onEscape: true,
              backdrop: true
            });
          } else {
            window.box = bootbox.dialog({
              message: data.result.message,
              title: this.lang.warning,
              buttons: {
                product: {
                  label: this.lang.btn_show_product_title,
                  className: 'btn-default',
                  callback: function() {
                    var anchor = $(btn).closest('.product').find('.description a').first();
                    window.location.href = anchor.attr('href');
                  }
                },
                success: {
                  label: this.lang.btn_ok_title,
                  className: "btn-primary",
                  callback: function () {
                    bootbox.hideAll();
                  }
                }
              },
              onEscape: true,
              backdrop: true
            });
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during adding item to cart. Please try again.');
        }
      });
      
    },
    
    addMultiProductQuick: function (btn) {
      var data = [];
      $this = this;
      $(btn).closest('.categoryProduct').find('input[name="multi-add-data[]"]').each(function(){
        data.push(JSON.parse($(this).val()));
        $this.flyToCart(this);
      });

      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.addProductUrl,
        type: 'post',
        data: ({mode: 'quick_multi', productId: 0, amount: 'auto', multiData: data}),
        context: this,
        success: function (data, textStatus) {
          if (data.result.success) {
            $('.cartRespons').text(data.result.cartResponse);
            $('.navbar-nav .cartMenu .dropdown-menu').html(data.result.miniCart);
            $('.navbar-cart .cartMenu').html(data.result.miniCartMobile);
            
            window.box = bootbox.dialog({
              message: data.result.message,
              title: this.lang.add_success,
              buttons: {
                back_to_shopping: {
                  label: this.lang.btn_back_to_shopping_title,
                  className: 'btn-default pull-left',
                  callback: function() {
                    bootbox.hideAll();
                  }
                },
                to_cart: {
                  label: this.lang.btn_to_cart_title,
                  className: "btn-primary",
                  callback: function () {
                    window.location.href = App.Cart.cartUrl;
                  }
                }
              },
              onEscape: true,
              backdrop: true
            });
          } else {
            window.box = bootbox.dialog({
              message: data.result.message,
              title: this.lang.warning,
              buttons: {
                product: {
                  label: this.lang.btn_show_product_title,
                  className: 'btn-default',
                  callback: function() {
                    var anchor = $(btn).closest('.product').find('.description a').first();
                    window.location.href = anchor.attr('href');
                  }
                },
                success: {
                  label: this.lang.btn_ok_title,
                  className: "btn-primary",
                  callback: function () {
                    bootbox.hideAll();
                  }
                }
              },
              onEscape: true,
              backdrop: true
            });
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during adding item to cart. Please try again.');
        }
      });
      
    },
    
    addProduct: function () {
      var productId = $('#ProductId').val();
      var productOptions = {};
      var error = false;
      
      var amount = $('#quantity-input').val();
      if (isNaN(parseInt(amount)) || parseInt(amount) <= 0) {
        error = true;
        $('#quantity-input').parent().addClass('has-error');
      } else {
        $('#quantity-input').parent().removeClass('has-error');
      }
      
      $('.productOptions select').each(function(){
        if (isNaN(parseInt($(this).val())) || parseInt($(this).val()) <= 0) {
          error = true;
          $(this).parent().addClass('has-error');
        } else {
          productOptions[$(this).attr('name').substring("data[ProductOption][".length).slice(0, -1)] = $(this).val();
          $(this).parent().removeClass('has-error');
        }
      });
      
      if (!error) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.addProductUrl,
          type: 'post',
          data: ({mode: 'normal', productId: productId, amount: amount, productOptions: productOptions}),
          context: this,
          success: function (data, textStatus) {
            if (data.result.success) {
              $('.cartRespons').text(data.result.cartResponse);
              $('.navbar-nav .cartMenu .dropdown-menu').html(data.result.miniCart);
              $('.navbar-cart .cartMenu').html(data.result.miniCartMobile);
              
              window.box = bootbox.dialog({
                message: data.result.message,
                title: this.lang.add_success,
                buttons: {
                  back_to_shopping: {
                    label: this.lang.btn_back_to_shopping_title,
                    className: 'btn-default pull-left',
                    callback: function() {
                      bootbox.hideAll();
                    }
                  },
                  to_cart: {
                    label: this.lang.btn_to_cart_title,
                    className: "btn-primary",
                    callback: function () {
                      window.location.href = App.Cart.cartUrl;
                    }
                  }
                },
                onEscape: true,
                backdrop: true
              });
            } else {
              window.box = bootbox.dialog({
                message: data.result.message,
                title: this.lang.warning,
                buttons: {
                  success: {
                    label: this.lang.btn_ok_title,
                    className: "btn-primary",
                    callback: function () {
                      bootbox.hideAll();
                    }
                  }
                },
                onEscape: true,
                backdrop: true
              });
            }
          },
          error: function (data, textStatus) {
            alert('Error occured during adding item to cart. Please try again.');
          }
        });
      }
    },
    
    deleteCartProduct: function (cartProductId, from) {
      from = from || '';
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.deleteCartProductUrl,
        data: ({cartProductId: cartProductId}),
        context: this,
        success: function (data, textStatus) {
          if (data.result.success) {
            $('.cartRespons').text(data.result.cartResponse);
            $('.navbar-nav .cartMenu .dropdown-menu').html(data.result.miniCart);
            $('.navbar-cart .cartMenu').html(data.result.miniCartMobile);
            
            if (from == 'cart') {
              window.location.reload();
            } 
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during deleting item to cart. Please try again.');
        }
      });
      
    },
    
    saveAmountsAndRefresh: function() {
      var data = {};
      $('.cartTable input[name^="data[CartProduct]"][name$="[amount]"]').each(function(){
        var id = $(this).attr('name').substring("data[CartProduct][".length).slice(0, "][amount]".length * -1);
        data[id] = $(this).val();
      });
      
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.saveCartProductAmountsUrl,
        type: 'post',
        data: {amounts: data},
        context: this,
        success: function (data, textStatus) {
          if (data.result.success) {
            window.location.reload();
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during saving cart. Please try again.');
        }
      });
    },
    
    flyToCart: function (btn) {
      var cart = $('.cartRespons:visible');
      var imgtodrag = $(btn).closest('.product').find('img').eq(0);
      if (imgtodrag && imgtodrag.length > 0) {
        var imgclone = imgtodrag.clone()
                .offset({
                  top: imgtodrag.offset().top,
                  left: imgtodrag.offset().left
                })
                .css({
                  'opacity': '0.5',
                  'position': 'absolute',
                  'height': '150px',
                  'width': '150px',
                  'z-index': '1040'
                })
                .appendTo($('body'))
                .animate({
                  'top': cart.offset().top + 10,
                  'left': cart.offset().left + 10,
                  'width': 75,
                  'height': 75
                }, 1000, 'easeInOutExpo');

        imgclone.animate({
          'width': 0,
          'height': 0
        }, function () {
          $(this).detach()
        });
      }
    },
    
    registrationChangeCountry: function(country_id) {
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.getZonesUrl,
        data: ({country_id: country_id}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert('Error occured during getting zones for selected country. Please try again.');
          } else {
            var options = [$('#UserAddress0ZoneId option').first()];
            $(data.result.zones).each(function(){
              options.push($('<option>').attr({value:this.id}).text(this.name));
            });
            $('#UserAddress0ZoneId option').remove();
            $('#UserAddress0ZoneId').append(options);
            
            $('#UserAddress0ZoneId').minimalect('update');
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during getting zones for selected country. Please try again.');
        }
      });
    },
    
    selectShippingRow: function(row) {
      $(row).find('input[type=radio]').prop('checked', true).change();
    },
    
    selectPaymentRow: function(row) {
      $(row).find('input[type=radio]').prop('checked', true).change();
    },
    
    changeShipping: function() {
      var products_price_tax = parseFloat($('#products_price_tax').val());
      var shipping_price_tax = parseFloat($('input[name="data[Cart][shipping_id]"]:checked').data('price'));
      
      var all_price = products_price_tax + shipping_price_tax;
      $('#shipping-price').text(shipping_price_tax.toFixed(2));
      $('#all-price').text(all_price.toFixed(2));
      
      console.log(products_price_tax, shipping_price_tax);
      
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.getPayments,
        data: ({
          shipping_id: $('input[name="data[Cart][shipping_id]"]:checked').val(),
          payment_id: $('input[name="data[Cart][payment_id]"]:checked').val()
        }),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert('Error occured during getting payments. Please try again.');
          } else {
            $('#payments-forms').html(data.result.html);
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during getting payments. Please try again.');
        }
      });
    },
    
    setDiscountCode: function(input) {
      setTimeout(function(){
        if ($(input).parent().find('p.text-danger').length <= 0) {
          $(input).parent().append('<p class="text-danger">Nieprawidłowy kod rabatowy</p>');
        }
      }, 400);
    },
    
    registrationChangeCreateAccount: function() {
      if ($('#CustomerNonGuestAccount').prop('checked')) {
        $('#CustomerOrderAccountForm .register-password').slideDown();
        $('#CustomerOrderAccountForm .register-password input[type=password]').prop('required', true);
      } else {
        $('#CustomerOrderAccountForm .register-password').slideUp();
        $('#CustomerOrderAccountForm .register-password input[type=password]').prop('required', false);
      }
    }
  };
})(App, jQuery);

$(document).ready(function () {
  App.Cart.onDocReady();
});