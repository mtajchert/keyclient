var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Categories = {
    
    lang: {
      banner_redirect_product_message: 'Chcesz dodać produkt do koszyka czy wyświetlić stronę produktu?',
      banner_redirect_product_title: '',
      btn_to_cart: 'Dodaj do koszyka',
      btn_to_product_page: 'Wyświetl stronę produktu'
    },
    
    onDocReady: function () {
      //--
    },
    
    changeOrder: function(select) {
      var url = "" + $('a#sortLink-' + $(select).val()).attr('href');
      if (url.length > 0) {
        window.location.href = url;
      }
    },
    
    redirectToProduct: function(productId, productViewUrl) {
      window.box = bootbox.dialog({
        message: this.lang.banner_redirect_product_message,
        title: this.lang.banner_redirect_product_title,
        buttons: {
          to_page: {
            label: this.lang.btn_to_product_page,
            className: 'btn-default pull-left',
            callback: function() {
              window.location.href = productViewUrl;
            }
          },
          to_cart: {
            label: this.lang.btn_to_cart,
            className: "btn-primary",
            callback: function () {
              App.Cart.addProductQuick(productId);
            }
          }
        },
        onEscape: true,
        backdrop: true
      });
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  App.Categories.onDocReady();
});