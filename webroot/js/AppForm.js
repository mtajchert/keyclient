var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Form = {
      
    onDocReady: function(){
      // --
    },
    
    toggleShow: function(check, selector_show, selector_hide, inside) {
      inside = inside || 'html';
      if (check) {
        $(inside).find(selector_hide).slideUp();
        //$(inside).find(selector_hide + ':not(' + selector_show + ')').slideUp();
        $(inside).find(selector_show).slideDown();
      }
    }
  };
})(App, jQuery);

$(document).ready(function () {
  App.Form.onDocReady();
});