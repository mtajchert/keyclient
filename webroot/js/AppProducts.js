var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Products = {
    onDocReady: function () {
      $('.rating-tooltip-manual').rating({
        extendSymbol: function () {
          var title;
          $(this).tooltip({
            container: 'body',
            placement: 'bottom',
            trigger: 'manual',
            title: function () {
              return title;
            }
          });
          $(this).on('rating.rateenter', function (e, rate) {
            title = rate;
            $(this).tooltip('show');
          }).on('rating.rateleave', function () {
            $(this).tooltip('hide');
          });
        }
      });
    },
    
    changeOption: function() {
      var price = parseFloat($('#base_price').val());
      $('select[name^="data[ProductOption]"] option:selected').each(function(){
        price = parseFloat($(this).data('change_price_tax'));
      });
      $('.product-price .price-sales span').text(Number(price).toFixed(2));
    }
  };
})(App, jQuery);

$(document).ready(function () {
  App.Products.onDocReady();
});