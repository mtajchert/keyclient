var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Search = {
    /*lang: {
      chooseCountry: 'Wybierz kraj',
      noCountryFound: 'Nie znaleziono kraju',
      chooseZone: 'Wybierz województwo',
      noZoneFound: 'Nie znaleziono województwa',
      
      confirmDeleteTitle: 'Potwierdź usunięcie',
      confirmDeleteAddressMsg: 'Czy na pewno chcesz usunąć adres?',
      delete: 'Usuń',
      cancel: 'Anuluj'
    },
    
    getZonesUrl: null,*/
    
    getSearchResults: null,
    
    search: function(search) {
      if ($(search).val().length <= 0) {
        return false;
      }
      
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.getSearchResults,
        data: ({q: $(search).val()}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert('Error occured during getting zones for selected country. Please try again.');
          } else {
            $('body > .w100, body > .container').hide();
            $('body .search_result_old').remove();
            $('body > .search_result').addClass('search_result_old')
            $('body a[name="start-content"]').after(data.result.results);
            $('body > .search_result:not(.search_result_old)').fadeIn();
            $('html, body').scrollTo(0,0);
            
            handleProductView();
            
            var url = $(search).data('searchurl') + '/q:' + $(search).val();
            window.history.pushState({},"", url);
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during getting search results. Please try again.');
        }
      });
    },
    
    /**
     * 
     */
    onDocReady: function () {
      /*$('#UserAddressCountryId').minimalect({
        placeholder: this.lang.chooseCountry,
        empty: this.lang.noCountryFound
      });
      $('#UserAddressZoneId').minimalect({
        placeholder: this.lang.chooseZone,
        empty: this.lang.noZoneFound
      });*/
    },

    /*deleteAddress: function(alias, deleteUrl) {
      bootbox.confirm({
        title: this.lang.confirmDeleteTitle,
        message: this.lang.confirmDeleteAddressMsg,
        buttons: {
          cancel: {
            label: this.lang.cancel,
            className: 'btn-default'
          },
          confirm: {
            label: this.lang.delete,
            className: 'btn-primary'
          }
        },
        callback: function(result){
          if (result) {
            window.location.href = deleteUrl;
          }
        },
        onEscape: true,
        backdrop: true
      });
    },
    
    changeCountry: function(country_id) {
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.getZonesUrl,
        data: ({country_id: country_id}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert('Error occured during getting zones for selected country. Please try again.');
          } else {
            var options = [$('#UserAddressZoneId option').first()];
            $(data.result.zones).each(function(){
              options.push($('<option>').attr({value:this.id}).text(this.name));
            });
            $('#UserAddressZoneId option').remove();
            $('#UserAddressZoneId').append(options);
            
            $('#UserAddressZoneId').minimalect('update');
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during getting zones for selected country. Please try again.');
        }
      });
    }*/
    
  };
})(App, jQuery);

$(document).ready(function () {
  App.Users.onDocReady();
});