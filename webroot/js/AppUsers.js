var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Users = {
    lang: {
      chooseCountry: 'Wybierz kraj',
      noCountryFound: 'Nie znaleziono kraju',
      chooseZone: 'Wybierz województwo',
      noZoneFound: 'Nie znaleziono województwa',
      
      confirmDeleteTitle: 'Potwierdź usunięcie',
      confirmDeleteAddressMsg: 'Czy na pewno chcesz usunąć adres?',
      delete: 'Usuń',
      cancel: 'Anuluj'
    },
    
    getZonesUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      $('#UserAddressCountryId').minimalect({
        placeholder: this.lang.chooseCountry,
        empty: this.lang.noCountryFound
      });
      $('#UserAddressZoneId').minimalect({
        placeholder: this.lang.chooseZone,
        empty: this.lang.noZoneFound
      });
    },

    deleteAddress: function(alias, deleteUrl) {
      bootbox.confirm({
        title: this.lang.confirmDeleteTitle,
        message: this.lang.confirmDeleteAddressMsg,
        buttons: {
          cancel: {
            label: this.lang.cancel,
            className: 'btn-default'
          },
          confirm: {
            label: this.lang.delete,
            className: 'btn-primary'
          }
        },
        callback: function(result){
          if (result) {
            window.location.href = deleteUrl;
          }
        },
        onEscape: true,
        backdrop: true
      });
    },
    
    changeCountry: function(country_id) {
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.getZonesUrl,
        data: ({country_id: country_id}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert('Error occured during getting zones for selected country. Please try again.');
          } else {
            var options = [$('#UserAddressZoneId option').first()];
            $(data.result.zones).each(function(){
              options.push($('<option>').attr({value:this.id}).text(this.name));
            });
            $('#UserAddressZoneId option').remove();
            $('#UserAddressZoneId').append(options);
            
            $('#UserAddressZoneId').minimalect('update');
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during getting zones for selected country. Please try again.');
        }
      });
    },
    
    /*saveSortOrder: function(unit_id, value) {
      if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.saveSortOrderUrl,
          data: ({unit_id: unit_id, value: parseInt(value)}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert(this.lang.saveSortOrderErrorMsg);
            }
          },
          error: function (data, textStatus) {
            alert(this.lang.saveSortOrderErrorMsg);
          }
        });
      } else {
        alert(this.lang.sortOrderInvalidValue);
      }
    },*/
    
  };
})(App, jQuery);

$(document).ready(function () {
  App.Users.onDocReady();
});