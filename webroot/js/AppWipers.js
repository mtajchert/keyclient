var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Wipers = {
    searchUrl: null,
    
    onDocReady: function () {
      $('#wiper-search-modal').on('shown.bs.modal', function () {
        App.Wipers.setIniState();
      });
      $('#wiper-modal-btn').on('click', function(e){
        App.Wipers.searchModalOpen();
      });
      
      $('#wiper-brand, #wiper-brand-s').on('change', function(e){
        App.Wipers.loadModels(this);
      });
      
      $('#search-wiper-btn, #search-wiper-btn-s').on('click', function(e){
        App.Wipers.search(this);
      });
    },
    
    searchModalOpen: function() {      
      $('#wiper-search-modal').modal('show');      
    },
    
    setIniState: function(){
      $('#wiper-search-modal').find('select').prop('disabled', true).html('');
      $.ajax({
        dataType: "html",
        url: '/categories/get_brands',
        success: function (data) {
          $('#wiper-brand').html(data);
          $('#wiper-brand').prop('disabled', false);
        },
        error: function (data, textStatus) {
          alert('Błąd. Prosimy spróbować pózniej.');
        }
      });
    },
    
    
    loadModels: function(elem){
      var postfix = $(elem).attr('id').substr(-2) == '-s' ? '-s' : '';
      if ($('#wiper-brand'+postfix).val() == ''){
        $('#wiper-model'+postfix).html('');
        $('#wiper-model'+postfix).prop('disabled', true);
        
        $('#wiper-wiper'+postfix).html('');
        $('#wiper-wiper'+postfix).prop('disabled', true);
        return false;
      }
      $.ajax({
        dataType: "html",
        url: '/categories/get_models/'+$('#wiper-brand'+postfix).val(),
        success: function (data) {
          $('#wiper-model'+postfix).html(data);
          $('#wiper-model'+postfix).prop('disabled', false);
        },
        error: function (data, textStatus) {
          alert('Błąd. Prosimy spróbować pózniej.');
        }
      });
    },
    
    search: function(elem){
      var postfix = $(elem).attr('id').substr(-2) == '-s' ? '-s' : '';
      if ($('#wiper-brand'+postfix).val() == '' || $('#wiper-model'+postfix).val() == ''){
        alert('Wybierz wszystkie parametry.')
        return false;
      }
      
      window.location.href = this.searchUrl + '/category_id:' + $('#wiper-model'+postfix).val();
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  App.Wipers.onDocReady();
});